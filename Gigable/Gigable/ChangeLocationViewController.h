//
//  ChangeLocationViewController.h
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChangeLocationViewController;


@protocol ChangeLocationViewControllerDelegate <NSObject>
@optional
- (void) getLocation: (ChangeLocationViewController *) sender;

@end


@interface ChangeLocationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tblLocationList;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tblLocationHeight;
@property (nonatomic, weak) id <ChangeLocationViewControllerDelegate> delegate;
@end
