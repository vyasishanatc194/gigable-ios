//
//  ConcertPlayListViewController.m
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import "ConcertPlayListViewController.h"
#import "HomeDashboardViewController.h"
#import "HomeDashboardViewController.h"
#import "AppDelegate.h"
#import "Static.h"
#import "Utility.h"
#import "ArtistCellCollectionViewCell.h"
#import "FeaturedArtistCell.h"
#import "PlaylistCell.h"
#import "UIImageView+WebCache.h"
#import "ConcertPlalyListCell.h"
#import "ChangeLocationViewController.h"
#import "PlayListDelegate.h"
#import "TrackInfoVC.h"
#import "ArtistDetailViewController.h"


@interface ConcertPlayListViewController ()<ChangeLocationViewControllerDelegate,AVAudioPlayerDelegate>
{
    NSMutableArray *PlayalistAllData;
    NSMutableArray *PlayListTracks;
    NSMutableArray *PlayListShows;
    
    IBOutlet NSLayoutConstraint *tblHeight;
    NSMutableArray *arrTracks,*arrShow;
    AppDelegate *delegate;
    BOOL isPlaying;
    BOOL isPlayALL;
    BOOL isPushed;
}
@end

@implementation ConcertPlayListViewController

-(void)viewWillAppear:(BOOL)animated
{
    
    if(delegate.audioPlayer.isPlaying){
        [_btnPlayAll setTitle:@"PAUSE" forState:UIControlStateNormal];
        [AppDelegate initAppdelegate].playerView.hidden = false;
        isPlayALL = true;
    }else{
        [_btnPlayAll setTitle:@"PLAY ALL" forState:UIControlStateNormal];
        [AppDelegate initAppdelegate].playerView.hidden = true;
        isPlayALL = false;
    }
//    _tblconcertPlaylist.estimatedRowHeight = 78;
//    _tblconcertPlaylist.rowHeight = UITableViewAutomaticDimension;
    NSString * locationName = [[NSUserDefaults standardUserDefaults]valueForKey:LocationNameDefault];
    _lblLocation.text = locationName;
    NSLog(@"%f",_lblLocation.frame.size.height);
//    NSLog(@"%ld",(long)_lblLocation.numberOfLines);
    isPlayALL = false;
    isPushed = false;
    if (_isFavSideMenu1 == true)
    {
        _lblTitleConcertPlaylist.text = @"Favorites";
        [self getMyFavourites];
        
    }
    else
    {
        _lblTitleConcertPlaylist.text = @"Concerts Near Me Playlist";
        [self FetchPlaylistByIndex];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    PlayListTracks = [NSMutableArray array];
     delegate=[AppDelegate initAppdelegate];
    
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"] == NO)
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playPlayer)name:@"PauseNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pausePlayer)name:@"PlayNotification"
                                               object:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isFirst"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(void)FetchPlaylistByIndex{
  
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    
    //    NSString *post = [NSString stringWithFormat: @"Lgn_id=%@&Latitude=%f&Longitude=%f&playlist_distance=%f&distance_type=%@&month_range=%ld",[AppDelegate initAppdelegate].objUser.user_id,self.locationManager.location.coordinate.latitude,self.locationManager.location.coordinate.longitude,[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],@"",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    NSString * latitude = [[NSUserDefaults standardUserDefaults]valueForKey:Latitude];
    NSString * longitude = [[NSUserDefaults standardUserDefaults]valueForKey:Logitude];
    
    NSString *post = [NSString stringWithFormat: @"lgn_id=%@&latitude=%@&longitude=%@&utp_play_id=%@",[AppDelegate initAppdelegate].objUser.user_id,latitude,longitude,self.index];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,FETCH_PLAYLIST_BY_INDEX]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               STOPLOADING()
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   NSMutableDictionary * dictRes = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];

                                   if (PlayListTracks.count > 0)
                                   {
                                       PlayListTracks = [[NSMutableArray alloc]init];
                                   }
                                   

                                   if([[dictRes valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       PlayalistAllData = [dictRes valueForKey:@"DATA"];
                                       PlayListTracks = [PlayalistAllData valueForKey:@"Track"];
                                       PlayListShows = [PlayalistAllData valueForKey:@"Show"];
                                       
                                       
                                       if(PlayListTracks.count >0){
                                           arrTracks=[[NSMutableArray alloc]init];
                                           for(NSMutableDictionary *dict in PlayListTracks)
                                           {
                                               [arrTracks addObject:[Track initWithResponceDict:dict]];
                                           }
                                           delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
                                           delegate.index_current_song=0;
                                           
                                       }
                                       
                                       
//                                        delegate.arrTracks=[[NSMutableArray alloc]initWithArray:PlayListTracks];
//                                       delegate.index_current_song=0;
                                       
                                       
                                       
                                       _lblConcertCount.text = [NSString stringWithFormat:@"%lu Concerts",(unsigned long)PlayListShows.count];
                                       _lblTracksCount.text = [NSString stringWithFormat:@"%lu Tracks",(unsigned long)PlayListTracks.count];
                                       [_tblconcertPlaylist reloadData];
                                   }else{
                                       
                                       
                                       
                                   }
                                   
                               }
                           }];
    
}


-(void)getMyFavourites
{
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    
    //    NSString *post = [NSString stringWithFormat: @"Lgn_id=%@&Latitude=%f&Longitude=%f&playlist_distance=%f&distance_type=%@&month_range=%ld",[AppDelegate initAppdelegate].objUser.user_id,self.locationManager.location.coordinate.latitude,self.locationManager.location.coordinate.longitude,[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],@"",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    NSString * latitude = [[NSUserDefaults standardUserDefaults]valueForKey:Latitude];
    NSString * longitude = [[NSUserDefaults standardUserDefaults]valueForKey:Logitude];
    
    NSString *post = [NSString stringWithFormat: @"lgn_id=%@&latitude=%@&longitude=%@&utp_play_id=fav_playlist",[AppDelegate initAppdelegate].objUser.user_id,latitude,longitude];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,FETCH_PLAYLIST_BY_INDEX]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               STOPLOADING()
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   NSMutableDictionary * dictRes = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   NSLog(@"%@",dictRes);
                                   //                                   [PlayalistAllData removeAllObjects];
                                   if (PlayListTracks.count > 0)
                                   {
                                       PlayListTracks = [[NSMutableArray alloc]init];
                                   }

                                   //                                   [PlayListShows removeAllObjects];
                                   if([[dictRes valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       PlayalistAllData = [dictRes valueForKey:@"DATA"];
                                       PlayListTracks = [PlayalistAllData valueForKey:@"Track"];
                                       PlayListShows = [PlayalistAllData valueForKey:@"Show"];
                                       
                                       
                                       if(PlayListTracks.count >0){
                                           arrTracks=[[NSMutableArray alloc]init];
                                           for(NSMutableDictionary *dict in PlayListTracks)
                                           {
                                               [arrTracks addObject:[Track initWithResponceDict:dict]];
                                           }
                                           delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
                                           delegate.index_current_song=0;
                                           
                                       }
                                       
                                       
                                       //                                        delegate.arrTracks=[[NSMutableArray alloc]initWithArray:PlayListTracks];
                                       //                                       delegate.index_current_song=0;
                                       
                                       
                                       
                                       _lblConcertCount.text = [NSString stringWithFormat:@"%lu Concerts",(unsigned long)PlayListShows.count];
                                       _lblTracksCount.text = [NSString stringWithFormat:@"%lu Tracks",(unsigned long)PlayListTracks.count];
                                       [_tblconcertPlaylist reloadData];
                                   }else{
                                       
                                       
                                       
                                   }
                                   
                               }
                           }];

//    [PlayListTracks removeAllObjects];
//    if(![Utility connectedToNetwork]){
//        [Utility showNetworkError];
//        return;
//    }
//    
//    
//    SHOWLOADING(@"Loading")
//    NSString *postStr=[NSString stringWithFormat:@"usrplay_lgn_id=%@",[AppDelegate initAppdelegate].objUser.user_id];
//    [Utility executePOSTRequestwithServicetype:GET_MY_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
//        STOPLOADING()
//        //NSLog(@"%@",dictresponce);
//        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
//            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
//            if(arr.count >0){
//                PlayListTracks=[[NSMutableArray alloc]init];
//                for(NSMutableDictionary *dict in arr)
//                {
//                    [PlayListTracks addObject:[Track initWithResponceDict:dict]];
//                }
//                NSLog(@"%@",PlayListTracks);
//                delegate.arrTracks=[[NSMutableArray alloc]initWithArray:PlayListTracks];
//                delegate.index_current_song=0;
//                [delegate playAudioWithIndex:delegate.index_current_song];
//                [delegate pausePlayer];
//                delegate.isMyFav=true;
////                lblPlayTitle.text=@"Favorites Playlist";
//            }
//            [_tblconcertPlaylist reloadData];
////            lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
//            
//            
//        }else{
//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
//        }
//    }];
}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView registerNib:[UINib nibWithNibName:@"ConcertPlalyListCell" bundle:nil] forCellReuseIdentifier:@"ConcertPlalyListCell"];
    
    ConcertPlalyListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConcertPlalyListCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (_isFavSideMenu1 == false)
    {
        [cell.btninfo setImage:[UIImage imageNamed:@"i_icon"] forState:UIControlStateNormal];
        [cell.btninfo addTarget:self action:@selector(btnInfoPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [cell.btninfo setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        [cell.btninfo addTarget:self action:@selector(favTrackClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
        cell.btninfo.tag = indexPath.row;
    
        NSMutableDictionary *track = [PlayListTracks objectAtIndex:indexPath.row];
        cell.lblArtistName.text = [track valueForKey:@"art_name"];
        cell.lblConcertName.text = [track valueForKey:@"trk_title"];
        cell.lblPlace.text = [track valueForKey:@"show_venue"];
        cell.lblPrice.text = [track valueForKey:@"show_price"];
        
        
        //    cell.lblPlace.text = [track valueForKey:@""];
        //    cell.lblDate.text = [track valueForKey:@"play_createddate"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [formatter dateFromString:[track valueForKey:@"play_createddate"]];
        
        [formatter setDateFormat:@"dd MMM yy"];
        NSString *output = [formatter stringFromDate:date];
        
        cell.lblDate.text = output;
        
        
        
        NSString *imageURl=[track valueForKey:@"artist_image"];
        
        
        [cell.imgArtist sd_setImageWithURL:[NSURL URLWithString:imageURl]
                          placeholderImage:[UIImage imageNamed:@""]];
//    }
    
//    else
//    {
//        cell.btninfo.tag = indexPath.row;
//        [cell.btninfo addTarget:self action:@selector(btnInfoPressed:) forControlEvents:UIControlEventTouchUpInside];
//        NSMutableDictionary *track = [PlayListTracks objectAtIndex:indexPath.row];
//        cell.lblArtistName.text = [track valueForKey:@"art_name"];
//        cell.lblConcertName.text = [track valueForKey:@"trk_title"];
////        cell.lblPlace.text = [track valueForKey:@"show_venue"];
////        cell.lblPrice.text = [track valueForKey:@"show_price"];
//        
//        
//        //    cell.lblPlace.text = [track valueForKey:@""];
//        //    cell.lblDate.text = [track valueForKey:@"play_createddate"];
//        
////        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
////        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
////        NSDate *date = [formatter dateFromString:[track valueForKey:@"play_createddate"]];
////        
////        [formatter setDateFormat:@"dd MMM yy"];
////        NSString *output = [formatter stringFromDate:date];
////        
////        cell.lblDate.text = output;
//        
//        
//        
////        NSString *imageURl=[track valueForKey:@"artist_image"];
////        
////        
////        [cell.imgArtist sd_setImageWithURL:[NSURL URLWithString:imageURl]
////                          placeholderImage:[UIImage imageNamed:@""]];
//    }

    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return PlayListTracks.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    isPlayALL = false;
//    NSLog(@"%@",delegate.arrTracks);
//    [delegate playAudioWithIndex:indexPath.row];
}
- (IBAction)btnInfoPressed:(UIButton *)sender {
    ArtistDetailViewController *controller = [[ArtistDetailViewController alloc]initWithNibName:@"ArtistDetailViewController" bundle:nil];
    NSDictionary *Artist = [PlayListTracks objectAtIndex:sender.tag];
    controller.ArtistId = [Artist valueForKey:@"art_id"];
    [self.navigationController pushViewController:controller animated:YES];
    
}


//-(void)showInfo
//{
//    //    [info.layer setCornerRadius:info.frame.size.width/2];
//    //    info.layer.masksToBounds=true;
//    if(isPushed==true){
//        [UIView transitionWithView:self.navigationController.view
//                          duration:0.75
//                           options:UIViewAnimationOptionTransitionFlipFromLeft
//                        animations:^{
////                            [info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];
//                            
//                            [self.navigationController popViewControllerAnimated:true];
//                        }
//                        completion:nil];
//        
//        isPushed=false;
//        return;
//    }
//    
//    id <PlayListDelegate> playListDelegate;
//    
////    for(UIViewController *vc in [self.navigationController viewControllers]){
////        if([vc isKindOfClass:[DashboardVC class]]){
////            
////            DashboardVC *objd=(DashboardVC *)vc;
////            playListDelegate=objd.playListDelegate;
////            break;
////        }
////    }
//    
//    
//    
//    
//    Track *objTr=[PlayListTracks objectAtIndex:0];
//    
//    TrackInfoVC *objTrinf=[[TrackInfoVC alloc]initWithNibName:@"TrackInfoVC" bundle:nil];
//    objTrinf.objtr=objTr;
//    objTrinf.playListDelegate=playListDelegate;
//    
//    [UIView transitionWithView:self.navigationController.view
//                      duration:0.75
//                       options:UIViewAnimationOptionTransitionFlipFromRight
//                    animations:^{
////                        [info setImage:[UIImage imageNamed:@"infobuttonActive.png"] forState:UIControlStateNormal];
//                        
//                        isPushed=true;
//                        [self.navigationController pushViewController:objTrinf animated:true];
//                    }
//                    completion:nil];
//}



- (IBAction)btnChangeLocationpressed:(id)sender {
    ChangeLocationViewController *Controller = [[ChangeLocationViewController alloc]initWithNibName:@"ChangeLocationViewController" bundle:nil];
    Controller.delegate = self;
    Controller.modalPresentationStyle = UIModalPresentationCustom;
    
    
    [self presentViewController:Controller animated:true completion:nil];

}

- (void) getLocation: (ChangeLocationViewController *) sender {
    NSString * locationName = [[NSUserDefaults standardUserDefaults]valueForKey:LocationNameDefault];
    _lblLocation.text = locationName;
    NSLog(@"%f",_lblLocation.frame.size.height);
    [self FetchPlaylistByIndex];
}

- (IBAction)btnBackPressed:(id)sender {
    
    HomeDashboardViewController *controller = [[HomeDashboardViewController alloc]initWithNibName:@"HomeDashboard" bundle:nil];
    
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)btnPlayAllpressed:(id)sender {
    
    if (delegate.arrTracks.count > 0)
    {
        if (isPlayALL == false)
        {
            if(delegate.audioPlayer.isPlaying)
            {
                [[AppDelegate initAppdelegate] pausePlayer];
                //                [self pausePlayer];
                
            }
            else
            {
                tblHeight.constant = _tblconcertPlaylist.frame.size.height - 140;
                [_btnPlayAll setTitle:@"PAUSE" forState:UIControlStateNormal];
                [delegate playAudioWithIndex:0];
                isPlayALL = true;
                [_btnPlayAll addTarget:self action:@selector(playPlayer) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
        }
        else
        {
            if(delegate.audioPlayer.isPlaying)
            {
                [[AppDelegate initAppdelegate] pausePlayer];
//                [self pausePlayer];
                
            }
            else
            {
                 [[AppDelegate initAppdelegate] playPlayer];
//                [self playPlayer];
            }
//            [_btnPlayAll addTarget:self action:@selector(playPlayer) forControlEvents:UIControlEventTouchUpInside];
        }
        
       
        
    }
   
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [[AppDelegate initAppdelegate] pausePlayer];
    [AppDelegate initAppdelegate].playerView.hidden = true;
}
-(void)pausePlayer
{
    [delegate.audioPlayer pause];
    
    [_btnPlayAll setTitle:@"PLAY ALL" forState:UIControlStateNormal];
    [_btnPlayAll addTarget:self action:@selector(playPlayer) forControlEvents:UIControlEventTouchUpInside];
    if(delegate.audioPlayer.isPlaying){
        isPlaying=true;
    }else{
        isPlaying=false;
    }
    
}
-(void)playPlayer
{
    [delegate.audioPlayer resume];
    [_btnPlayAll setTitle:@"PAUSE" forState:UIControlStateNormal];
    [_btnPlayAll addTarget:self action:@selector(pausePlayer) forControlEvents:UIControlEventTouchUpInside];
    if(delegate.audioPlayer.isPlaying){
        isPlaying=true;
    }else{
        isPlaying=false;
    }
    
    
}



-(void)favTrackClicked:(UIButton *)btn{
    
    Track *objTr=[PlayListTracks objectAtIndex:btn.tag];
    NSString *trackId = [[PlayListTracks objectAtIndex:btn.tag]valueForKey:@"trk_id"];
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"usrplay_lgn_id=%@&utp_trk_id=%@",[AppDelegate initAppdelegate].objUser.login_id,trackId];
    
    [Utility executePOSTRequestwithServicetype:MAKE_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [self getMyFavourites];
//            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
//            if(objTr.isFav==true){
//                objTr.isFav=false;
//                //                [arrTracks removeObject:objTr];
//                //                [tblView reloadData];
//                //                lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
//                [[NSNotificationCenter defaultCenter]
//                 postNotificationName:@"FavClick"
//                 object:self];
//                [[AppDelegate initAppdelegate]checkFavTrack:objTr];
//            }else{
//                objTr.isFav=true;
//            }
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    
    
    
    
    
    
    
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
