//
//  LoginVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "DashboardVC.h"
#import "AppDelegate.h"
#import "Static.h"
#import "Utility.h"
#import "SignUpVC.h"
@interface ForgotPasswordVC ()<UITextFieldDelegate>

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [newPwd setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [confirmPwd setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];

    [txtCode setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [email setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [btnPwd addTarget:self action:@selector(btnForgotPasswordClicked:) forControlEvents:UIControlEventTouchUpInside];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    [textField resignFirstResponder];
    return true;
}

- (IBAction)btnForgotPasswordClicked:(id)sender
{
    if([Utility isStringEmpty:email.text]){
        CGRect frLb=speForgot.frame;
        frLb.size.height=3.0;
        speForgot.frame=frLb;
        errorForgotPas.text=@"Please enter email";
        return;
    }
    
    if(![Utility NSStringIsValidEmail:email.text]){
        CGRect frLb=speForgot.frame;
        frLb.size.height=3.0;
        speForgot.frame=frLb;
        errorForgotPas.text=@"Please enter valid email";
        return;
    }
    
    [email resignFirstResponder];
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *post = [NSString stringWithFormat: @"lgn_email=%@",email.text ];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,FORGOT_PASSWORD]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               STOPLOADING()
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   NSMutableDictionary * dictRes = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   //NSLog(@"%@",dictRes);
                                   
                                   if([[dictRes valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       [Utility showAlrtWithMessage:[[dictRes valueForKey:@"MESSAGES"]objectAtIndex:0]];
                                       login_ID=[[dictRes valueForKey:@"DATA"] valueForKey:@"lgn_id"];
                                       viewForgotPwd.hidden=true;
                                       viewverificationCode.hidden=false;
                                       btnPwd.hidden=true;
                                       btnCode.hidden=false;
                                       btnresetPass.hidden=true;
//                                       [self.navigationController popViewControllerAnimated:true];
                                   

                                   }else{
                                       CGRect frLb=speForgot.frame;
                                       frLb.size.height=3.0;
                                       speForgot.frame=frLb;
                                       errorForgotPas.text=[[dictRes valueForKey:@"MESSAGES"]objectAtIndex:0];

                                   }
                               }
                           }];

    
    
}

- (IBAction)btnVerifyCodeClicked:(id)sender
{
    
    if([Utility isStringEmpty:txtCode.text]){
        
        CGRect frLb=sepCode.frame;
        frLb.size.height=3.0;
        sepCode.frame=frLb;
        errCode.text=@"Please enter verification code";
        return;
    }
    
        [txtCode resignFirstResponder];
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *post = [NSString stringWithFormat:@"lgn_id=%@&lgn_fgtpass_verifycode=%@",login_ID,txtCode.text ];
    
    
    [Utility executePOSTRequestwithServicetype:VERIFY_CODE withPostString:post withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        //NSLog(@"%@",dictresponce);
        STOPLOADING()
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            
            viewForgotPwd.hidden=true;
            viewverificationCode.hidden=true;
            resetPwd.hidden=false;
            btnPwd.hidden=true;
            btnCode.hidden=true;
            btnresetPass.hidden=false;
            //                                       [self.navigationController popViewControllerAnimated:true];
            
            
        }else{
            CGRect frLb=sepCode.frame;
            frLb.size.height=3.0;
            sepCode.frame=frLb;
            errCode.text=[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0];

        }

    }];
    
    


}


- (IBAction)btnResetPasswordClicked:(id)sender

{
    
    if([Utility isStringEmpty:newPwd.text]){
        
        CGRect sepNew1=sepNew.frame;
        sepNew1.size.height=3.0;
        sepNew.frame=sepNew1;
        errnewpwd.text=@"Please enter new Password";
        return;
    }
    
    if([Utility isStringEmpty:confirmPwd.text]){
        
        CGRect frLb11=sepConfirm.frame;
        frLb11.size.height=3.0;
        sepConfirm.frame=frLb11;
        errConfPwd.text=@"Please enter confirm Password";

        return;
    }
    
    
    if(![newPwd.text isEqualToString:confirmPwd.text])
    {
        
        CGRect sepNew1=sepNew.frame;
        sepNew1.size.height=3.0;
        sepNew.frame=sepNew1;
        errnewpwd.text=@"Password mismatch";
        CGRect frLb11=sepConfirm.frame;
        frLb11.size.height=3.0;
        sepConfirm.frame=frLb11;
        errConfPwd.text=@"Password mismatch";

        return;
    }
    [newPwd resignFirstResponder];
    [confirmPwd resignFirstResponder];
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *post = [NSString stringWithFormat:@"lgn_id=%@&new_password=%@&cnfm_password=%@",login_ID,newPwd.text,confirmPwd.text ];
    
    
    [Utility executePOSTRequestwithServicetype:RESET_PWD withPostString:post withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        //NSLog(@"%@",dictresponce);
        STOPLOADING()
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            
            
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            
            [self.navigationController popViewControllerAnimated:true];
            
        }else{
            
            CGRect sepNew1=sepNew.frame;
            sepNew1.size.height=3.0;
            sepNew.frame=sepNew1;
            errnewpwd.text=[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0];
//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
        
    }];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    [self resetFr];
    return  true;
}
-(void)resetFr
{
    CGRect frLb=speForgot.frame;
    frLb.size.height=1.0;
    speForgot.frame=frLb;
    errorForgotPas.text=@"";
    
    CGRect frLb1=sepCode.frame;
    frLb1.size.height=1.0;
    sepCode.frame=frLb1;
    errCode.text=@"";
    
    CGRect frLb11=sepConfirm.frame;
    frLb11.size.height=1.0;
    sepConfirm.frame=frLb11;
    errConfPwd.text=@"";
    
    CGRect sepNew1=sepNew.frame;
    sepNew1.size.height=1.0;
    sepNew.frame=sepNew1;
    errnewpwd.text=@"";
    
    
    
    
//    CGRect frLb1=sepPwd.frame;
//    frLb1.size.height=1.0;
//    sepPwd.frame=frLb1;
//    errro2.text=@"";
    
}
@end
