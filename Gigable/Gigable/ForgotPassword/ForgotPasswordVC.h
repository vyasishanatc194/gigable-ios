//
//  LoginVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController
{
     IBOutlet UIView *viewverificationCode;
    
     IBOutlet UIView *resetPwd;
     IBOutlet UIButton *btnPwd;
     IBOutlet UITextField *txtCode;
     IBOutlet UIView *viewForgotPwd;
     IBOutlet UITextField *email;
    NSString *login_ID;
     IBOutlet UITextField *newPwd;
     IBOutlet UITextField *confirmPwd;
     IBOutlet UIButton *btnCode;
     IBOutlet UIButton *btnresetPass;
    
    IBOutlet UILabel *errorForgotPas;
    IBOutlet UILabel *speForgot;
    
    IBOutlet UILabel *sepConfirm;
    IBOutlet UILabel *sepNew;
    IBOutlet UILabel *errConfPwd;
    IBOutlet UILabel *errnewpwd;
    IBOutlet UILabel *errCode;
    IBOutlet UILabel *sepCode;
}

- (IBAction)btnForgotPasswordClicked:(id)sender;
- (IBAction)btnVerifyCodeClicked:(id)sender;
- (IBAction)btnResetPasswordClicked:(id)sender;
@end
