//
//  UpcomingConcertViewController.m
//  Gigable
//
//  Created by AppDev03 on 2/3/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import "UpcomingConcertViewController.h"
#import "DashboardVC.h"
#import "Static.h"
#import "Utility.h"
#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import "MFSideMenu.h"
#import "ASIFormDataRequest.h"
#import "PlayListVC.h"
#import "AppDelegate.h"
#import "HDNotificationView.h"
#import <Social/Social.h>
#import "TicketInfoVC.h"
#import "PaymentPageVC.h"
#import "PurchaseTicket.h"
#import "ShowDetailViewController.h"

@interface UpcomingConcertViewController ()
{
//    NSString *imageURl;
//    NSString *ArtistName;
    NSMutableArray *arrShows;
}

@end

@implementation UpcomingConcertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (_isGetTicket == true)
    {
        [_tblView reloadData];
    }
    else
    {
        [self getUpcomingConcerts];
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrShowForUpcoming.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                  reuseIdentifier:MyIdentifier] ;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    
    Show *objSh=[_arrShowForUpcoming objectAtIndex:indexPath.row];
    cell.textLabel.text=objSh.show_title;
    cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
    cell.textLabel.textColor=[UIColor whiteColor];
    
    UIImageView *imgB=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"play_indicator.png"]];
    cell.accessoryView=imgB;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor =CELL_SELECTTION_COLOR;
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    cell.backgroundColor=[UIColor clearColor];
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([_tblView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tblView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_tblView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tblView setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
      Show *objS=[_arrShowForUpcoming objectAtIndex:indexPath.row];
    
    ShowDetailViewController *controller = [[ShowDetailViewController alloc]initWithNibName:@"ShowDetailViewController" bundle:nil];
    controller.showId =  [objS valueForKey:@"show_id"];
    
    controller.artistImage = [objS valueForKey:@"art_image"];
    controller.artistName = [objS valueForKey:@"art_name"];

    

    [self.navigationController pushViewController:controller animated:YES];
}

-(void)getUpcomingConcerts
{
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    [Utility executeRequestwithServicetype:FETCH_FUTURE_CONCERTS withDictionary:[[NSMutableDictionary alloc]init] withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
             arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"show"];
            NSMutableArray *OtherDetails=[[dictresponce valueForKey:@"DATA"] valueForKey:@"otherdetails"];
//            imageURl = [OtherDetails valueForKey:@"image"];
//            ArtistName = [OtherDetails valueForKey:@"artistname"];
            if(arrShows.count >0){
                _arrShowForUpcoming=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows)
                {
                    [_arrShowForUpcoming addObject:[Show initWithResponceDict:dict]];
                }
                
                [_tblView reloadData];
            }

            
            
        }
        
        
        
        
    }];
}
- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
