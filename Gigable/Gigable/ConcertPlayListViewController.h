//
//  ConcertPlayListViewController.h
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConcertPlayListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tblconcertPlaylist;
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblConcertCount;
@property (strong, nonatomic) IBOutlet UILabel *lblTracksCount;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayAll;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleConcertPlaylist;

@property (nonatomic,strong) NSString *index;
@property (nonatomic,strong) NSString *Title;
@property (assign,nonatomic)BOOL isFavSideMenu1;
@end

