//
//  TicketInfoVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 17/02/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "TicketInfoVC.h"
#import "Utility.h"
#import "Static.h"
#import "PurchaseTicket.h"
#import "AppDelegate.h"
@interface TicketInfoVC ()
{
    AppDelegate *delegate;
    BOOL playerHidden;
}
@end

@implementation TicketInfoVC
@synthesize playListDelegate;
@synthesize objShow;
@synthesize isOverlay;
- (void)viewDidLoad {
    [super viewDidLoad];
    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
    delegate=[AppDelegate initAppdelegate];
    playerHidden=[AppDelegate initAppdelegate].playerView.hidden;

    if(self.objShow){
        
        NSString *dateString = self.objShow.show_Full_Date;
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *yourDate = [dateFormatter dateFromString:dateString];
        dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";
//        dateFormatter.dateFormat = @"MMMM dd, yyyy";

        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
        
        lblLocation.text=self.objShow.show_title;
        lblShowDesc.text=self.objShow.show_desc;
        lblShowName.text=self.objShow.show_title;
//        [lblShowDesc sizeToFit];
        lblShowDate.text=[dateFormatter stringFromDate:yourDate];
        
        [btnFav setImage:[UIImage imageNamed:self.objShow.isFav?@"Star-Icon-Active.png":@"fav_mark.png"] forState:UIControlStateNormal];
        
        [btnClose.layer setCornerRadius:btnClose.frame.size.width/2];
        btnClose.layer.masksToBounds=true;
        [btnClose.layer setBorderWidth:1.0];
        [btnClose.layer setBorderColor:NAVNBAR_COLOR.CGColor];
        [self GteShowDetails];
    }
    
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
}
- (void)viewWillDisappear:(BOOL)animated; {
    [AppDelegate initAppdelegate].playerView.hidden=playerHidden;
}

-(IBAction)backButtonPressed:(id)sender
{
    [AppDelegate initAppdelegate].playerView.hidden=false;

    self.navigationController.navigationBarHidden=true;
    [self.navigationController popViewControllerAnimated:true];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buyTicketPressed:(id)sender {
    PurchaseTicket *objP=[[PurchaseTicket alloc]initWithNibName:@"PurchaseTicket" bundle:nil];
    objP.objShow=self.objShow;
    objP.arrTic=[dictRes valueForKey:@"Tickets"];
    objP.playListDelegate=self.playListDelegate;
    [self.navigationController pushViewController:objP animated:true];
    
    return;
        if([dictRes objectForKey:@"Tickets"]){
            NSMutableArray *arrT=[dictRes valueForKey:@"Tickets"];
            if(arrT.count >0){
                alertTicSel=[[UIAlertView alloc] initWithTitle:@"Select Ticket Type" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                for(int i=0;i<arrT.count;i++){
                    [alertTicSel addButtonWithTitle:[[arrT objectAtIndex:i] valueForKey:@"ticket_type_label"]];
                }
                [alertTicSel show];
                
            }else{
                [Utility showAlrtWithMessage:@"No Tickets found"];
            }
    }
    
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    
    if(buttonIndex== alertView.cancelButtonIndex){
        return;
    }
    PurchaseTicket *objP=[[PurchaseTicket alloc]initWithNibName:@"PurchaseTicket" bundle:nil];
    objP.objShow=self.objShow;
    NSString *booked=[[[dictRes valueForKey:@"Tickets"] objectAtIndex:buttonIndex-1] valueForKey:@"ticket_booked"];
    NSString *qty=[[[dictRes valueForKey:@"Tickets"] objectAtIndex:buttonIndex-1] valueForKey:@"ticket_qty"];

    if([qty integerValue] >[booked integerValue])
    
//objP.totalqty=[NSString stringWithFormat:@"%d",[qty integerValue] >[booked integerValue]];
    objP.totalqty=[[[dictRes valueForKey:@"Tickets"] objectAtIndex:buttonIndex-1] valueForKey:@"ticket_qty"];

    objP.ticketId=[[[dictRes valueForKey:@"Tickets"] objectAtIndex:buttonIndex-1] valueForKey:@"ticket_id"];
    objP.ticketTypeId=[[[dictRes valueForKey:@"Tickets"] objectAtIndex:buttonIndex-1] valueForKey:@"ticket_type_id"];

    objP.price=[[[dictRes valueForKey:@"Tickets"] objectAtIndex:buttonIndex-1] valueForKey:@"ticket_price"];

    [self.navigationController pushViewController:objP animated:true];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)makeMyFav:(id)sender {
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"msf_show_id=%@&msf_lgn_id=%@",self.objShow.show_id,[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:MARK_SHOW_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            self.objShow.isFav=!self.objShow.isFav;
            [btnFav setImage:[UIImage imageNamed:self.objShow.isFav?@"Star-Icon-Active.png":@"fav_mark.png"] forState:UIControlStateNormal];

            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNotification"
             object:self];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNotifi"
             object:self];
        }else{
            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
        }
    }];

}

-(void)GteShowDetails{
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"show_id=%@",self.objShow.show_id];
    [Utility executePOSTRequestwithServicetype:DETAIL_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            dictRes=[dictresponce valueForKey:@"DATA"];
            lblShowDesc.text=[dictRes valueForKey:@"show_desc"];
            txtDesc.text=[dictRes valueForKey:@"show_desc"];
            lblShowName.text=[dictRes valueForKey:@"ven_name"];
        locationName.text=[NSString stringWithFormat:@"%@, %@",[dictRes valueForKey:@"ven_city"],[dictRes valueForKey:@"ven_state"]];
            lblArtist.text=[dictRes valueForKey:@"artist"];
            

        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];

}

@end
