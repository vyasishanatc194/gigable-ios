//
//  TicketInfoVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 17/02/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "User.h"
#import "PlayListDelegate.h"
@interface TicketInfoVC : UIViewController <UIAlertViewDelegate>
{
    IBOutlet UIButton *btnClose;
    
    IBOutlet UILabel *lblShowDesc;
    
     IBOutlet UITextView *txtDesc;
    IBOutlet UILabel *lblShowDate;
    IBOutlet UILabel *lblLocation;
     IBOutlet UIView *topView;
    IBOutlet UILabel *lblShowName;
    IBOutlet UIButton *btnFav;
    NSMutableArray *arrTickType;
    NSMutableDictionary *dictRes;
    UIAlertView *alertTicSel;
    
     IBOutlet UILabel *lblArtist;
     IBOutlet UILabel *locationName;
}
@property (nonatomic, weak) id <PlayListDelegate> playListDelegate;
@property (strong,nonatomic)Show *objShow;
@property (assign,nonatomic)BOOL *isOverlay;
- (IBAction)makeMyFav:(id)sender;

@end
