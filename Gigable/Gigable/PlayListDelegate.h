//
//  PlayListDelegate.h
//  Gigable
//
//  Created by Kalpit Gajera on 13/06/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlayListDelegate <NSObject>
- (void)didSelectItemAtIndex:(NSUInteger)index;
- (void)didSelectPlayAtIndex:(NSUInteger)index;
- (void)didSelectPlayItemListItemAtIndex:(NSUInteger)index;
@end
