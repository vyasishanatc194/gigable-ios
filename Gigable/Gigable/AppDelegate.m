//
//  AppDelegate.m
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "AppDelegate.h"
#import "Static.h"
#import "SVProgressHUD.h"
#import "SideMenuViewController.h"
#import "SplashVC.h"
#import "MFSideMenuContainerViewController.h"
#import "DashboardVC.h"
#import "Utility.h"
#import "User.h"
#import "TicketInfoVC.h"
#import "Payment1.h"
#import "TrackInfoVC.h"
#import "GoogleSignIn/GoogleSignIn.h"
#import <Stripe/Stripe.h>
#import "LoadingVc.h"
#import "MFSideMenu.h"
#import "ShowsListVC.h"
#import "PlayListVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "PlayListDelegate.h"
#import "HomeDashboardViewController.h"
#import "ConcertPlayListViewController.h"
#import "UpcomingConcertViewController.h"
@interface AppDelegate ()<AVAudioPlayerDelegate, GIDSignInDelegate >
{
    MFSideMenuContainerViewController *container;
}
@end


@implementation AppDelegate
@synthesize audioPlayer;
@synthesize arrLoaction;
@synthesize objUser;
@synthesize playerView;
@synthesize arrTracks;
@synthesize index_current_song;
@synthesize isRef,isMyFav;
@synthesize locationManager;
@synthesize objPlaylist;
@synthesize info;
@synthesize arrPlayList;
@synthesize isPushed,isPushedShow;
+(AppDelegate*)initAppdelegate
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self performSelector:@selector(getLocationFromWebservice) withObject:nil];

    //NSLog(@"%@",[UIFont fontNamesForFamilyName:@"Gotham Bold"]);

    
    [GIDSignIn sharedInstance].clientID = kClientID;
   
   // [Stripe setDefaultPublishableKey:@"pk_test_GAGUG3sxvYMq6lovOuMP3O38"];

 [Stripe setDefaultPublishableKey:@"pk_live_14b3owRRGPFiSYfoukx8vO3U"];

    [[AVAudioSession sharedInstance] setDelegate:self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];

    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:true];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:FONTNAME size:18.0]
                                                           }];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters;
    //    self.locationManager.distanceFilter=0;
    self.locationManager.delegate = self;
    NSUInteger code = [CLLocationManager authorizationStatus];
    if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
        // choose one request according to your business.
        if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
            [self.locationManager requestAlwaysAuthorization];
        } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
            [self.locationManager  requestWhenInUseAuthorization];
        } else {
            //NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
        }
    }
    
    [self.locationManager startUpdatingLocation];

    if([[NSUserDefaults standardUserDefaults]objectForKey:KEY_LOGINDICT]){
        
        [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_LOGINDICT]];
        [[AppDelegate initAppdelegate]showDashboardMenu];

    }else{
    
//    [self showLoginMenu];
        
        UINavigationController *navDash=[[UINavigationController alloc]initWithRootViewController:[[LoadingVc alloc] initWithNibName:@"LoadingVc" bundle:nil]];
        
        //       UINavigationController *navDash=[[UINavigationController alloc]initWithRootViewController:[[Payment1 alloc] initWithNibName:@"Payment1" bundle:nil]];
        
        navDash.navigationBarHidden=true;
        self.window.rootViewController=navDash;
        [self.window makeKeyAndVisible];

    }
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    // Override point for customization after application launch.

    return YES;
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation] || [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                                                               openURL:url
                                                                                                     sourceApplication:sourceApplication
                                                                                                            annotation:annotation];
    
}
-(void)showLoginMenu
{
    
        [[NSUserDefaults standardUserDefaults]removeObjectForKey: KEY_LOGINDICT];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    self.playerView=nil;
    [self.audioPlayer stop];

    self.audioPlayer=nil;

    UINavigationController *navDash=[[UINavigationController alloc]initWithRootViewController:[[SplashVC alloc] initWithNibName:@"SplashVC" bundle:nil]];
    
//       UINavigationController *navDash=[[UINavigationController alloc]initWithRootViewController:[[Payment1 alloc] initWithNibName:@"Payment1" bundle:nil]];
    
    navDash.navigationBarHidden=true;
    self.window.rootViewController=navDash;
    [self.window makeKeyAndVisible];

}

-(void)showDashboardMenu
{
//    [self setPlayer];
//    DashboardVC *objDsah=[[DashboardVC alloc]initWithNibName:@"DashboardVC" bundle:nil];
//   nav=[[UINavigationController alloc]
//                                initWithRootViewController:objDsah];
//    
//    
//    SideMenuViewController *leftMenuViewController = [[SideMenuViewController alloc] init];
//    leftMenuViewController.demoController=objDsah;
//   container = [MFSideMenuContainerViewController containerWithCenterViewController:nav leftMenuViewController:leftMenuViewController rightMenuViewController:nil];
//    self.window.rootViewController = container;
//    [self.window makeKeyAndVisible];
    
    [self setPlayer];
    HomeDashboardViewController *objDsah=[[HomeDashboardViewController alloc]initWithNibName:@"HomeDashboard" bundle:nil];
    nav=[[UINavigationController alloc]
         initWithRootViewController:objDsah];
    
    
    SideMenuViewController *leftMenuViewController = [[SideMenuViewController alloc] init];
    leftMenuViewController.demoController=objDsah;
    container = [MFSideMenuContainerViewController containerWithCenterViewController:nav leftMenuViewController:leftMenuViewController rightMenuViewController:nil];
    self.window.rootViewController = container;
    [self.window makeKeyAndVisible];
}


- (DashboardVC *)demoController {
    return [[DashboardVC alloc] initWithNibName:@"DashboardVC" bundle:nil];
}

- (UINavigationController *)navigationController {
    return [[UINavigationController alloc]
            initWithRootViewController:[self demoController]];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];

}



- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark Custom Process View

-(void)showCustomProgressViewWithText:(NSString *)strText{
    
    [SVProgressHUD showWithStatus:strText maskType:SVProgressHUDMaskTypeGradient];
    //  [GMDCircleLoader setOnView:self.window withTitle:strText animated:YES];
    
}
-(void)hideCustomProgressView
{
    [SVProgressHUD dismiss];
    //    [GMDCircleLoader hideFromView:self.window animated:YES];
    
    
}
-(void)getLocationFromWebservice
{
    return;
    [Utility executeRequestwithServicetype:GET_LOCATION withDictionary:[[NSMutableDictionary alloc]init] withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            if([[dictresponce valueForKey:@"COUNT"] intValue]>0){
                self.arrLoaction=[[NSMutableArray alloc]init];
                NSMutableArray *arrTemp=[dictresponce valueForKey:@"DATA"];
                for(NSMutableDictionary *dict in arrTemp){
                    Location *objl=[Location initWithResponceDict:dict];
                    
                    if([objl.loc_id isEqualToString:self.objUser.locId]){
                        self.objUser.objLocation=[Location initWithResponceDict:dict];
                    }
                    
                    [self.arrLoaction addObject:objl];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCUPDATE" object:self];

            }
        }
    }];
}

-(void)setPlayer{
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    audioPlayer = [[STKAudioPlayer alloc] initWithOptions:(STKAudioPlayerOptions){ .flushQueueOnSeek = YES, .enableVolumeMixer = NO, .equalizerBandFrequencies = {50, 100, 200, 400, 800, 1600, 2600, 16000} }];
    audioPlayer.meteringEnabled = YES;
    audioPlayer.volume = 1;
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    //    STKDataSource* dataSource = [STKAudioPlayer dataSourceFromURL:urlq];
    playerView=[[UIView alloc]initWithFrame:CGRectMake(0, HEIGHT-130, WIDTH, 130)];
    [playerView setBackgroundColor:[UIColor blackColor]];
    
    CGRect viewFr=playerView.frame;

    UIButton *back=[[UIButton alloc]initWithFrame:CGRectMake(0,viewFr.size.height-60, 60,60)];
    [back setImage:[UIImage imageNamed:@"backward.png"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(backWordPlayer) forControlEvents:UIControlEventTouchUpInside];
    [back.layer setBorderWidth:0.5];
    [back.layer setBorderColor:[UIColor grayColor].CGColor];
    
//    [playerView addSubview:back];;
    
//    play=[[UIButton alloc]initWithFrame:CGRectMake(back.frame.origin.x+back.frame.size.width,viewFr.size.height-60, 60,60)];
    play=[[UIButton alloc]initWithFrame:CGRectMake(back.frame.origin.x,viewFr.size.height-60, 60,60)];
    [play setImage:[UIImage imageNamed:@"paly.png"] forState:UIControlStateNormal];
    [play addTarget:self action:@selector(playPlayer) forControlEvents:UIControlEventTouchUpInside];
    [play.layer setBorderWidth:0.5];
    [play.layer setBorderColor:[UIColor grayColor].CGColor];
    
    [playerView addSubview:play];;
    
    UIButton *forward=[[UIButton alloc]initWithFrame:CGRectMake(play.frame.origin.x+play.frame.size.width,viewFr.size.height-60, 60,60)];
    [forward setImage:[UIImage imageNamed:@"forward.png"] forState:UIControlStateNormal];
    [forward addTarget:self action:@selector(forwardPlayer) forControlEvents:UIControlEventTouchUpInside];
    [playerView addSubview:forward];;
    
    [forward.layer setBorderWidth:0.5];
    [forward.layer setBorderColor:[UIColor grayColor].CGColor];
    
    float width=(WIDTH-180)/3;
    
    UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, viewFr.size.height-60,WIDTH,0.5)];
    [lblSep setBackgroundColor:[UIColor grayColor]];
    [playerView addSubview:lblSep];

    btnfav=[[UIButton alloc]initWithFrame:CGRectMake(forward.frame.origin.x+forward.frame.size.width,viewFr.size.height-60, width,60)];
    [btnfav setImage:[UIImage imageNamed:@"Star-Icon.png"] forState:UIControlStateNormal];
    [btnfav addTarget:self action:@selector(makeFav) forControlEvents:UIControlEventTouchUpInside];
    [playerView addSubview:btnfav];;
    
    info=[[UIButton alloc]initWithFrame:CGRectMake(btnfav.frame.origin.x+btnfav.frame.size.width,viewFr.size.height-60, width,60)];
    [info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];
    [info addTarget:self action:@selector(showInfo) forControlEvents:UIControlEventTouchUpInside];

//    [playerView addSubview:info];;
    
//    ticket=[[UIButton alloc]initWithFrame:CGRectMake(info.frame.origin.x+info.frame.size.width,viewFr.size.height-60, width,60)];
    ticket=[[UIButton alloc]initWithFrame:CGRectMake(info.frame.origin.x,viewFr.size.height-60, width+100,60)];
    [ticket setTitle:@"GET TICKETS" forState:UIControlStateNormal];
    [ticket setTitleColor:[UIColor colorWithRed:10.0/255.0 green:216.0/255.0 blue:248.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [ticket setImage:[UIImage imageNamed:@"Ticket.png"] forState:UIControlStateNormal];
    [ticket addTarget:self action:@selector(bookShowticket) forControlEvents:UIControlEventTouchUpInside];
    [playerView addSubview:ticket];;
    
    CGRect frame = CGRectMake(0.0, -10.0, viewFr.size.width, 30);
    sliderAudio = [[UISlider alloc] initWithFrame:frame];
    [sliderAudio addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [sliderAudio setBackgroundColor:[UIColor clearColor]];
    sliderAudio.tintColor=NAVNBAR_COLOR;
    sliderAudio.maximumTrackTintColor=[UIColor blackColor];
    sliderAudio.minimumValue = 0.0;
    sliderAudio.continuous = YES;
    sliderAudio.value = 0.0;
    [sliderAudio setThumbImage:[UIImage imageNamed:@"Thumb.png"] forState:UIControlStateNormal];
    [playerView addSubview:sliderAudio];;
    sliderAudio.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);

    
    totalTime=[[UILabel alloc]initWithFrame:CGRectMake(WIDTH-50, 22, 40, 12)];
    totalTime.textColor=[UIColor whiteColor];
    totalTime.textAlignment=NSTextAlignmentRight;
    totalTime.text=@"00:00";
    totalTime.font=[UIFont fontWithName:FONTNAME size:10.0];
    [playerView addSubview:totalTime];;
    
    remainingTime=[[UILabel alloc]initWithFrame:CGRectMake(10, 22, 40, 12)];
    remainingTime.textColor=[UIColor whiteColor];
    remainingTime.text=@"00:00";
    remainingTime.font=[UIFont fontWithName:FONTNAME size:10.0];
    [playerView addSubview:remainingTime];;
    
    UILabel *lblNowPlaying=[[UILabel alloc]initWithFrame:CGRectMake(20, 37.0, 200, 12)];
    lblNowPlaying.textColor=[UIColor whiteColor];
    lblNowPlaying.text=@"Now Playing:";
    lblNowPlaying.font=[UIFont fontWithName:FONTNAME size:10.0];
    [playerView addSubview:lblNowPlaying];;
    
   lblTrack_playing=[[UILabel alloc]initWithFrame:CGRectMake(20, 49.0, WIDTH-40, 20.0)];
    lblTrack_playing.textColor=[UIColor whiteColor];
    lblTrack_playing.font=[UIFont fontWithName:FONTNAME size:13.0];
    [playerView addSubview:lblTrack_playing];;
}

#pragma mark-Player Delegate


-(void)playAudioWithIndex:(NSInteger )index;
{
//    isPlaying=true;

    if(index <0){
        index=self.arrTracks.count;
    }
    if(index >= self.arrTracks.count){
        index=0;
        
    }
    self.index_current_song=index;
    
    Track *objTr=[self.arrTracks objectAtIndex:index];
    if(objTr.ticket_status ==true){
        ticket.enabled=true;
    }else{
        ticket.enabled=false;
    }
    if(objTr.isFav==true){
        [btnfav setImage:[UIImage imageNamed:@"Star-Icon-Active.png"] forState:UIControlStateNormal];
    }else{
        [btnfav setImage:[UIImage imageNamed:@"Star-Icon.png"] forState:UIControlStateNormal];
    }
    [[UIApplication sharedApplication].keyWindow addSubview:playerView];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TRACKUPDATE" object:objTr];
    //    http://mastimaza.in/download-43-0886096a0c7bc48e2b62/01.+Sanam+Re.mp3
    playerView.hidden=false;
    NSString *urlstr=objTr.trk_audio;
    
    NSURL *urlq=[NSURL URLWithString:urlstr];

      [audioPlayer playURL:urlq];
    [play setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [play addTarget:self action:@selector(pausePlayer) forControlEvents:UIControlEventTouchUpInside];
    if (objTr.art_name == nil)
    {
        lblTrack_playing.text=[NSString stringWithFormat:@"%@",objTr.trk_title];
    }
    else
    {
        lblTrack_playing.text=[NSString stringWithFormat:@"%@, %@",objTr.art_name ,objTr.trk_title];
    }
    
    
    if(!updateTimer){
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:.01 target:self selector:@selector(updateCurrentTime) userInfo:nil repeats:YES];
    }
  
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"PLAYCHANGE"
     object:self];
    

}


-(IBAction)sliderAction:(id)sender
{
    [audioPlayer seekToTime:sliderAudio.value];
}
-(void)updateCurrentTime
{
    
    if(audioPlayer.duration >1.0 && audioPlayer.progress >1.0){
        
        
        sliderAudio.maximumValue=audioPlayer.duration;
//    //NSLog(@"Duration %f  Progress %f",audioPlayer.duration,audioPlayer.progress);
    sliderAudio.value=audioPlayer.progress;


    if (audioPlayer.state ==STKAudioPlayerStatePlaying)
    {
        
        NSString *dur = [NSString stringWithFormat:@"%02d:%02d", (int)((int)(audioPlayer.progress)) / 60, (int)((int)(audioPlayer.progress)) % 60];
        
        NSString *maxDur = [NSString stringWithFormat:@"%02d:%02d", (int)((int)(audioPlayer.duration)) / 60, (int)((int)(audioPlayer.duration)) % 60];
        [self playerTimeDelegate:dur :maxDur];
    }
    }
}

-(void)playerTimeDelegate:(NSString *)currTime :(NSString *)maxTime
{
    remainingTime.text = currTime;
    totalTime.text = maxTime;
    [sliderAudio setValue:audioPlayer.progress];
    if([currTime isEqualToString:maxTime])
    {
 remainingTime.text = @"00:00";
        [self playNext];
    }
}

-(void)playNext
{
    
[self playAudioWithIndex:index_current_song+1];
    
}

ShowsListVC *objList;
PlayListVC *objPlayList;

-(void)bookShowticket
{
    
    
    if(self.isPushedShow==true){
//        [UIView transitionWithView:nav.view
//                          duration:0.75
//                           options:UIViewAnimationOptionTransitionFlipFromLeft
//                        animations:^{
        
                            [nav popViewControllerAnimated:false ];
//                        }
//                        completion:nil];
        
        isPushedShow=false;
//        return;
    }

    
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
id <PlayListDelegate> playListDelegate;
    
    for(UIViewController *vc in [nav viewControllers]){
    if([vc isKindOfClass:[DashboardVC class]]){
        
        DashboardVC *objd=(DashboardVC *)vc;
        playListDelegate=objd.playListDelegate;
        break;
    }
    }
    
    
    
    SHOWLOADING(@"Loading")
    Track *objTr=[self.arrTracks objectAtIndex:index_current_song];
    NSString *postStr;
    if([self.objPlaylist.play_id isEqualToString:@"local_playlist"]){
        postStr=[NSString stringWithFormat:@"lgn_id=%@&trk_id=%@&lat=%f&lon=%f",[AppDelegate initAppdelegate].objUser.user_id,objTr.id_track,self.locationManager.location.coordinate.latitude,self.locationManager.location.coordinate.longitude ];

    }else{
        postStr=[NSString stringWithFormat:@"lgn_id=%@&trk_id=%@&lat=&lon=",[AppDelegate initAppdelegate].objUser.user_id,objTr.id_track ];
    }
    [Utility executePOSTRequestwithServicetype:GET_TICKET_FROM_TRACK withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
            if(arr.count > 0){
                NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
            for(NSMutableDictionary *dict in arr)
            {
                
                [arrTemp addObject:[Show initWithResponceDict:dict]];
            }
               

                
                if([self.objPlaylist.play_id isEqualToString:@"local_playlist"]){

                
                objList=[[ShowsListVC alloc]initWithNibName:@"ShowsListVC" bundle:nil];
                objList.arrShows=arrTemp;
                objList.isTick=true;
                    
                    objList.playListDelegate=playListDelegate;
                    
                    
                   
                [UIView transitionWithView:nav.view
                                  duration:0.75
                                   options:UIViewAnimationOptionTransitionFlipFromRight
                                animations:^{
                                    self.isPushedShow=true;
                                    [nav pushViewController:objList animated:true];
                                }
                                completion:nil];
                }else{
//                    PlayListVC *objPlayList=[[PlayListVC alloc]initWithNibName:@"PlayListVC" bundle:nil];
//                    objPlayList.isTracks=false;
//                    objPlayList.arrData=arrTemp;
//                    NSLog(@"%@",arrTemp);
//                    objPlayList.playListDelegate=playListDelegate;
//                    objPlayList.isFromTic=true;
                    UpcomingConcertViewController *upcomingVC = [[UpcomingConcertViewController alloc]initWithNibName:@"UpcomingConcertViewController" bundle:nil];
                   
                    upcomingVC.arrShowForUpcoming=arrTemp;
                    upcomingVC.isGetTicket = true;
                    NSLog(@"%@",arrTemp);
//                    upcomingVC.playListDelegate=playListDelegate;
//                    upcomingVC.isFromTic=true;
                   
                    [UIView transitionWithView:nav.view
                                      duration:0.75
                                       options:UIViewAnimationOptionTransitionFlipFromRight
                                    animations:^{
                                        self.isPushedShow=true;

                                        [nav pushViewController:upcomingVC animated:true];
                                    }
                                    completion:nil];
                }
            }else{
                [Utility showAlrtWithMessage:@"No Show's found"];
            }
           
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];

}
-(void)setPushTag
{
    isPushed=false;
}

-(void)showInfo
{
//    [info.layer setCornerRadius:info.frame.size.width/2];
//    info.layer.masksToBounds=true;
    if(isPushed==true){
        [UIView transitionWithView:nav.view
                          duration:0.75
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            [info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];

                            [nav popViewControllerAnimated:true];
                        }
                        completion:nil];
        
        isPushed=false;
        return;
    }
    
    id <PlayListDelegate> playListDelegate;
    
    for(UIViewController *vc in [nav viewControllers]){
        if([vc isKindOfClass:[DashboardVC class]]){
            
            DashboardVC *objd=(DashboardVC *)vc;
            playListDelegate=objd.playListDelegate;
            break;
        }
    }
    

    
    
    Track *objTr=[self.arrTracks objectAtIndex:self.index_current_song];

    TrackInfoVC *objTrinf=[[TrackInfoVC alloc]initWithNibName:@"TrackInfoVC" bundle:nil];
    objTrinf.objtr=objTr;
    objTrinf.playListDelegate=playListDelegate;
    
    [UIView transitionWithView:nav.view
                      duration:0.75
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        [info setImage:[UIImage imageNamed:@"infobuttonActive.png"] forState:UIControlStateNormal];

                        isPushed=true;
                        [nav pushViewController:objTrinf animated:true];
                    }
                    completion:nil];
}


-(void)showInfo:(NSInteger *)ind
{
    //    [info.layer setCornerRadius:info.frame.size.width/2];
    //    info.layer.masksToBounds=true;
//    if(isPushed==true){
        [UIView transitionWithView:nav.view
                          duration:0.75
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            [info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];
                            
                            [nav popViewControllerAnimated:true];
                        }
                        completion:nil];
        
//        isPushed=false;
//        return;
//    }
    
    id <PlayListDelegate> playListDelegate;
    
    for(UIViewController *vc in [nav viewControllers]){
        if([vc isKindOfClass:[DashboardVC class]]){
            
            DashboardVC *objd=(DashboardVC *)vc;
            playListDelegate=objd.playListDelegate;
            break;
        }
    }
    
    
    
    
    Track *objTr=[self.arrTracks objectAtIndex:ind];
    
    TrackInfoVC *objTrinf=[[TrackInfoVC alloc]initWithNibName:@"TrackInfoVC" bundle:nil];
    objTrinf.objtr=objTr;
    objTrinf.playListDelegate=playListDelegate;
    
    [UIView transitionWithView:nav.view
                      duration:0.75
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        [info setImage:[UIImage imageNamed:@"infobuttonActive.png"] forState:UIControlStateNormal];
                        
                        isPushed=true;
                        [nav pushViewController:objTrinf animated:true];
                    }
                    completion:nil];
}




-(void)makeFav
{
    
    Track *objTr=[self.arrTracks objectAtIndex:self.index_current_song];

 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"usrplay_lgn_id=%@&utp_trk_id=%@",[AppDelegate initAppdelegate].objUser.login_id,objTr.id_track];
    
    [Utility executePOSTRequestwithServicetype:MAKE_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
            if(objTr.isFav==true){
                objTr.isFav=false;
                [btnfav setImage:[UIImage imageNamed:@"Star-Icon.png"] forState:UIControlStateNormal];
            }else{
                objTr.isFav=true;
                [btnfav setImage:[UIImage imageNamed:@"Star-Icon-Active.png"] forState:UIControlStateNormal];
            }
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavClick"
             object:self];
            
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    
}
-(void)pausePlayer
{
    ConcertPlayListViewController *concert = [[ConcertPlayListViewController alloc]init];
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"] == YES)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:concert];
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"PlayNotification" object:self];
//    concert.btnPlayAll = [[UIButton alloc]init];
    [concert.btnPlayAll setTitle:@"PLAY ALL" forState:UIControlStateNormal];
    [concert.btnPlayAll setBackgroundColor:[UIColor redColor]];
    [audioPlayer pause];
    
    [play setImage:[UIImage imageNamed:@"paly.png"] forState:UIControlStateNormal];
    [play addTarget:self action:@selector(playPlayer) forControlEvents:UIControlEventTouchUpInside];
    if(audioPlayer.isPlaying){
        isPlaying=true;
    }else{
        isPlaying=false;
    }
   
}
-(void)playPlayer
{
    ConcertPlayListViewController *concert = [[ConcertPlayListViewController alloc]init];
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"] == YES)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:concert];
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"PauseNotification" object:self];
//    concert.btnPlayAll = [[UIButton alloc]init];
    [concert.btnPlayAll setTitle:@"PAUSE" forState:UIControlStateNormal];
    
    [audioPlayer resume];
    [play setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [play addTarget:self action:@selector(pausePlayer) forControlEvents:UIControlEventTouchUpInside];
    if(audioPlayer.isPlaying){
        isPlaying=true;
    }else{
        isPlaying=false;
    }

    }
-(void)backWordPlayer
{
       if(audioPlayer.isPlaying){
        isPlaying=true;
    }else{
        isPlaying=false;
    }
   
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SHOWDETAIL" object:self];
    [self playAudioWithIndex:index_current_song-1];
    if(isPlaying==false){
        [self pausePlayer];
    }
}

-(void)forwardPlayer
{
    if(audioPlayer.isPlaying){
        isPlaying=true;
    }else{
        isPlaying=false;
    }
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"SHOWDETAIL"
     object:self];
    [self playAudioWithIndex:index_current_song+1];
    if(isPlaying==false){
        [self pausePlayer];
    }
}


- (void)remoteControlReceivedWithEvent: (UIEvent *) receivedEvent {
    
    if (receivedEvent.type == UIEventTypeRemoteControl) {
        
        switch (receivedEvent.subtype) {
            case UIEventSubtypeRemoteControlNextTrack:
                [self forwardPlayer];
                break;
            case UIEventSubtypeRemoteControlPreviousTrack:
                [self backWordPlayer];
                break;
            case UIEventSubtypeRemoteControlPlay:
                [self playPlayer];
                break;
            case UIEventSubtypeRemoteControlPause:
                [self pausePlayer];
                break;
                 case UIEventSubtypeRemoteControlStop:
                [self pausePlayer];
                break;
                
            case UIEventSubtypeRemoteControlTogglePlayPause:
                if (audioPlayer.state == STKAudioPlayerStatePlaying) {
                    [self pausePlayer];
                }else {
                    [self playPlayer];
                }
                break;

                
                        default: break;
        }
    }
}

-(void)checkFavTrack:(Track *)objTr
{
    Track *objTrPlay=[self.arrTracks objectAtIndex:self.index_current_song];
    if([objTr.id_track isEqualToString:objTrPlay.id_track]){
        if(objTr.isFav==true){
            [btnfav setImage:[UIImage imageNamed:@"Star-Icon-Active.png"] forState:UIControlStateNormal];
        }else{
            [btnfav setImage:[UIImage imageNamed:@"Star-Icon.png"] forState:UIControlStateNormal];
        }

    }
    
    
    for(Track *objTrack in self.arrTracks){
            if([objTrack.id_track isEqualToString:objTr.id_track]){
                objTrack.isFav=objTr.isFav;
                break;
            }
    }

}
@end
