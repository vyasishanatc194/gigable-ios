//
//  PaymentPageVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 12/05/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentPageVC : UIViewController
{
    
    IBOutlet UILabel *userName;
    IBOutlet UILabel *showName;
    
    IBOutlet UILabel *showDate;
    IBOutlet UILabel *qty;
    IBOutlet UILabel *tickertType;

}
@property (strong,nonatomic)NSString *ticketId;
- (IBAction)backButtonPressed:(id)sender;
@end
