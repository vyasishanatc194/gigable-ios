//
//  PaymentPageVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 12/05/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "PaymentPageVC.h"
#import "Static.h"
#import "Utility.h"
#import "Appdelegate.h"
@interface PaymentPageVC ()
{
    BOOL playerHidden;
    AppDelegate *delegate;


}
@end

@implementation PaymentPageVC
@synthesize ticketId;
- (void)viewDidLoad {
    [super viewDidLoad];
    delegate=[AppDelegate initAppdelegate];
    playerHidden=[AppDelegate initAppdelegate].playerView.hidden;
    

    [self getTicketOrderDetail];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [AppDelegate initAppdelegate].playerView.hidden=true;

}
- (void)viewWillDisappear:(BOOL)animated; {
    [AppDelegate initAppdelegate].playerView.hidden=playerHidden;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getTicketOrderDetail{
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    
    NSString *postStr=[NSString stringWithFormat:@"lgn_id=%@&utkt_id=%@",[AppDelegate initAppdelegate].objUser.user_id,self.ticketId];
    NSData *postData = [postStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,GET_ORDER_DETAIL]]];
    //NSLog(@"%@",[NSString stringWithFormat:@"%@%@",MAIN_URL,GET_ORDER_DETAIL]);
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //            [request setValue:@"application/form-data" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   STOPLOADING()
                                   //NSLog(@"Responce : %@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                   NSMutableDictionary * innerJson = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   
                                   NSMutableDictionary *dict = [innerJson mutableCopy];
                                   NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                                   [dict removeObjectsForKeys:keysForNullValues];
                                   
                                   if([[dict valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       userName.text=[AppDelegate initAppdelegate].objUser.userName;
                                       tickertType.text=[NSString stringWithFormat:@"%@ Tickets, %@",[[dict valueForKey:@"DATA"] valueForKey:@"utkt_qty"],[[dict valueForKey:@"DATA"] valueForKey:@"ticket_type_label"]];
                                       
                                       qty.text=[NSString stringWithFormat:@"%ld",[[[dict valueForKey:@"DATA"] valueForKey:@"utkt_price"] integerValue]];
                                       
                                       NSString *dateString =[NSString stringWithFormat:@"%@ %@",[[dict valueForKey:@"DATA"] valueForKey:@"show_date"],[[dict valueForKey:@"DATA"] valueForKey:@"show_time"]];
                                       NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                       dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                                       NSDate *yourDate = [dateFormatter dateFromString:dateString];
                                       dateFormatter.dateFormat = @"MMM dd, yyyy";
                                       showDate.text=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:yourDate]];
                                       showName.text=[[dict valueForKey:@"DATA"] valueForKey:@"show_title"];
                                       
                                   }else{
                                       [Utility showAlrtWithMessage:[dict valueForKey:@"MESSAGES"]];
                                   }
                                   
                                   
                               }
                           }];
    

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:true];
}
@end
