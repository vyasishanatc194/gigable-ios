//
//  HomeDashboardViewController.m
//  Gigable
//
//  Created by AppDev03 on 1/16/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import "HomeDashboardViewController.h"
#import "HomeDashboardViewController.h"
#import "AppDelegate.h"
#import "Static.h"
#import "Utility.h"
#import "ArtistCellCollectionViewCell.h"
#import "FeaturedArtistCell.h"
#import "PlaylistCell.h"
#import "UIImageView+WebCache.h"

#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import "MFSideMenu.h"

#import "ConcertPlayListViewController.h"
#import "ArtistDetailViewController.h"

#import "ChangeLocationViewController.h"
#import "UpcomingConcertViewController.h"

@interface HomeDashboardViewController ()<ChangeLocationViewControllerDelegate>

{
    NSMutableArray *PlayalistAllData;
    NSMutableDictionary *NearMeData;
    NSMutableArray *NearMeArtist;
    
    NSMutableArray *FeaturedArtistData;
    NSMutableArray *UpComingConcertData;
    
    NSMutableArray *LocationArray;
    NSString *ArtistName;
    
    NSMutableArray *playlistArray;
}
@end

@implementation HomeDashboardViewController

@synthesize locationManager;



- (void)viewDidLoad {
    [super viewDidLoad];
     [self getAllLocation];
    PlayalistAllData = [[NSMutableArray alloc]init];
    NearMeData = [[NSMutableDictionary alloc]init];
    ArtistName = @"";
    self.navigationController.navigationBarHidden=true;
    LocationArray = [[NSMutableArray alloc]init];
    NSLog(@"%@",NSStringFromCGSize(CollectionFeaturedArtist.contentSize) );
    // Do any additional setup after loading the view.
}


- (void) getLocation: (ChangeLocationViewController *) sender {
    [self FetchPlaylistByLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    
   
    self.navigationController.navigationBarHidden = YES;
    
    //    [self getUpcomingConcerts];
    CollectionArtist.layer.borderWidth = 2.0;
    CollectionArtist.layer.borderColor = [UIColor colorWithRed:10.0/255.0 green:216.0/255.0 blue:248.0/255.0 alpha:1.0].CGColor;
    CollectionFeaturedArtist.scrollEnabled = false;
    
//    [[AppDelegate initAppdelegate] pausePlayer];
    [AppDelegate initAppdelegate].playerView.hidden = true;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == CollectionArtist)
    {
        return NearMeArtist.count;
    }
    else if (collectionView == CollectionFeaturedArtist)
    {
        return FeaturedArtistData.count;
        
    }
    else
    {
//        return  PlayalistAllData.count - 1;
        return playlistArray.count;
    }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionArtist.backgroundColor = [UIColor colorWithRed:39.0/255.0 green:39.0/255.0 blue:39.0/255.0 alpha:1.0];
    collectionPlaylist.backgroundColor = [UIColor colorWithRed:39.0/255.0 green:39.0/255.0 blue:39.0/255.0 alpha:1.0];
    CollectionFeaturedArtist.backgroundColor = [UIColor colorWithRed:39.0/255.0 green:39.0/255.0 blue:39.0/255.0 alpha:1.0];
    
    
    if (collectionView == CollectionArtist)
    {
        [collectionView registerNib:[UINib nibWithNibName:@"ArtistCellCollectionViewCell"
                                                   bundle:nil]forCellWithReuseIdentifier:@"ArtistCellCollectionViewCell"];
        
        ArtistCellCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ArtistCellCollectionViewCell" forIndexPath:indexPath];
        NSString *imageURl=[[NearMeArtist objectAtIndex:indexPath.row] objectForKey:@"image"];;
        
        
        [cell.imgArtist sd_setImageWithURL:[NSURL URLWithString:imageURl]
                          placeholderImage:[UIImage imageNamed:@""]];
        
        CGFloat FeaturedHeight = CollectionFeaturedArtist.contentSize.height;
        CGFloat PlaylistHeight = collectionPlaylist.contentSize.height;
        mainViewScrollHeight.constant = FeaturedHeight + PlaylistHeight + 260.0 + 110.0 +173.0 + 60.0;
        
        return cell;
    }
    else if (collectionView == CollectionFeaturedArtist)
    {
        [collectionView registerNib:[UINib nibWithNibName:@"FeaturedArtistCell"
                                                   bundle:nil]forCellWithReuseIdentifier:@"FeaturedArtistCell"];
        
        
        FeaturedArtistCell *cellF = [collectionView dequeueReusableCellWithReuseIdentifier:@"FeaturedArtistCell" forIndexPath:indexPath];
        cellF.imgFituredArtist.layer.borderWidth = 2.0;
        cellF.imgFituredArtist.layer.borderColor = [UIColor colorWithRed:10.0/255.0 green:216.0/255.0 blue:248.0/255.0 alpha:1.0].CGColor;
        NSString *imageURl=[[FeaturedArtistData objectAtIndex:indexPath.row] objectForKey:@"art_image"];;
        
        [cellF.imgFituredArtist sd_setImageWithURL:[NSURL URLWithString:imageURl]
                                  placeholderImage:[UIImage imageNamed:@""]];
        cellF.lblfeaturedArtistName.text = [[FeaturedArtistData objectAtIndex:indexPath.row] objectForKey:@"art_name"];;
        
        NSLog(@"%@",NSStringFromCGSize(CollectionFeaturedArtist.contentSize) );
        CGFloat FeaturedHeight = CollectionFeaturedArtist.contentSize.height;
        FeaturedCollectionViewHeight.constant = FeaturedHeight;
        
        
        CGFloat PlaylistHeight = collectionPlaylist.contentSize.height;
        mainViewScrollHeight.constant = FeaturedHeight + PlaylistHeight + 260.0 + 110.0 +173.0 + 60.0;
        
        
        
        return cellF;
    }
    else
    {
        [collectionView registerNib:[UINib nibWithNibName:@"PlaylistCell"
                                                   bundle:nil]forCellWithReuseIdentifier:@"PlaylistCell"];
        
        
        PlaylistCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlaylistCell" forIndexPath:indexPath];
        cell.imgPlayList.layer.borderWidth = 2.0;
        cell.imgPlayList.layer.borderColor = [UIColor colorWithRed:10.0/255.0 green:216.0/255.0 blue:248.0/255.0 alpha:1.0].CGColor;
        
        //        if ([PlayalistAllData objectAtIndex:0])
        //        {
        //
        //        }
        //        else
        //        {
//        if (indexPath.row != 0)
//        {
            NSMutableDictionary * playlistData;
            playlistData = [playlistArray objectAtIndex:(indexPath.row)];
            
            NSString *imageURl=[playlistData valueForKey:@"image"];
            
            
            [cell.imgPlayList sd_setImageWithURL:[NSURL URLWithString:imageURl]
                                placeholderImage:[UIImage imageNamed:@""]];
            NSString * TeackCount = [playlistData valueForKey:@"totaltrack"];
            cell.lblTrackCount.text = [NSString stringWithFormat:(@"%@ Tracks"),TeackCount];
            cell.lblPlayListName.text = [playlistData valueForKey:@"play_name"];
//        }
        
        
        //        }
        CGFloat PlaylistHeight = collectionPlaylist.contentSize.height;
        PlaylistCollectionViewHeight.constant = PlaylistHeight;
        
        CGFloat FeaturedHeight = CollectionFeaturedArtist.contentSize.height;
        
        mainViewScrollHeight.constant = FeaturedHeight + PlaylistHeight + 260.0 + 110.0 +173.0 +60.0;
        
        return cell;
    }
    
}


- (CGSize)collectionView:(UICollectionView* )collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath* )indexPath
{
    CGFloat screenWidth = [[UIScreen mainScreen]bounds].size.width;
    CGFloat screenHeight = [[UIScreen mainScreen]bounds].size.height;
    if (collectionView == CollectionArtist)
    {
        return CGSizeMake(55, 50);
    }
    else if (collectionView == CollectionFeaturedArtist)
    {
        return CGSizeMake(screenWidth/3-20, 120);
    }
    else
    {
        return CGSizeMake(screenWidth/2-15, 185);
    }
    
}

- (IBAction)showSideMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //        [self setupMenuBarButtonItems];
    }];
    
}



-(void)FetchPlaylistByLocation{
    //    if([Utility isStringEmpty:email.text]){
    //        //        [Utility showAlrtWithMessage:@"Please enter email"];
    //        CGRect frLb=sepLogin.frame;
    //        frLb.size.height=3.0;
    //        sepLogin.frame=frLb;
    //        errro1.text=@"Please enter email";
    //
    //        return;
    //    }
    //
    //    if(![Utility NSStringIsValidEmail:email.text]){
    //        CGRect frLb=sepLogin.frame;
    //        frLb.size.height=3.0;
    //        sepLogin.frame=frLb;
    //        errro1.text=@"Please enter valid email";
    //        return;
    //    }
    //
    //    if([Utility isStringEmpty:password.text]){
    //        CGRect frLb1=sepPwd.frame;
    //        frLb1.size.height=3.0;
    //        sepPwd.frame=frLb1;
    //        errro2.text=@"Please enter password";
    //
    //
    //        return;
    //    }
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    
    //    NSString *post = [NSString stringWithFormat: @"Lgn_id=%@&Latitude=%f&Longitude=%f&playlist_distance=%f&distance_type=%@&month_range=%ld",[AppDelegate initAppdelegate].objUser.user_id,self.locationManager.location.coordinate.latitude,self.locationManager.location.coordinate.longitude,[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],@"",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    NSString * latitude = [[NSUserDefaults standardUserDefaults]valueForKey:Latitude];
    NSString * longitude = [[NSUserDefaults standardUserDefaults]valueForKey:Logitude];
    NSString * locationName = [[NSUserDefaults standardUserDefaults]valueForKey:LocationNameDefault];
    lblLocation.text = locationName;
    
    NSString *post = [NSString stringWithFormat: @"lgn_id=%@&latitude=%@&longitude=%@&playlist_distance=%@&distance_type=%@&month_range=%@",[AppDelegate initAppdelegate].objUser.user_id,latitude,longitude,@"1000",@"2",@"12"];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,FETCH_PLAYLIST_BY_LOCATION]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               STOPLOADING()
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   NSMutableDictionary * dictRes = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
//                                   NSLog(@"%@",dictRes);
                                   
                                   if([[dictRes valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       PlayalistAllData = [dictRes valueForKey:@"DATA"];
                                       FeaturedArtistData = [dictRes valueForKey:@"FEATURE"];
                                       UpComingConcertData = [dictRes valueForKey:@"UPCOMING_CONCERTS"];
                                       //                                       FutureContestShow = [UpComingConcertData valueForKey:@"otherdetails"];
                                       
                                       NearMeData = [PlayalistAllData objectAtIndex:0];
                                       playlistArray = [[NSMutableArray alloc]init];
                                       int i;
                                       
                                       for (i = 0; i < PlayalistAllData.count; i++) {
                                           if (i != 0)
                                           {
                                               NSString *new = [PlayalistAllData objectAtIndex:i];
                                               [playlistArray addObject:new];
                                               
                                           }
                                       }
                                       NearMeArtist = [NearMeData valueForKey:@"artist"];
                                       
                                       
                                       NSString *imageURl=[UpComingConcertData valueForKey:@"image"];
                                       
                                       lblUpcomingArtistName.text = [UpComingConcertData valueForKey:@"artistname"];
                                       
                                       [imgUpcomingConcert sd_setImageWithURL:[NSURL URLWithString:imageURl]
                                                             placeholderImage:[UIImage imageNamed:@""]];
                                       
                                       imgUpcomingConcert.layer.borderWidth = 2.0;
                                       imgUpcomingConcert.layer.borderColor = [UIColor colorWithRed:10.0/255.0 green:216.0/255.0 blue:248.0/255.0 alpha:1.0].CGColor;
                                       
                                       NSString * ConcertCountUpcomin = [UpComingConcertData valueForKey:@"totalshow"];
                                       lblUpcomingConcertCount.text = [NSString stringWithFormat:(@"%@ Concert"),ConcertCountUpcomin];
                                       
                                       NSString * TeackCountUpcoming = [UpComingConcertData valueForKey:@"totaltrack"];
                                       lblUpcomingTracksCount.text = [NSString stringWithFormat:(@"%@ Tracks"),TeackCountUpcoming];
                                       
                                       
                                       for(NSDictionary *Artist in NearMeArtist)
                                       {
                                           if ([ArtistName  isEqual: @""])
                                           {
                                               if ([Artist valueForKey:@"name"] != nil)
                                               {
                                                   ArtistName = [Artist valueForKey:@"name"];
                                               }
                                               
                                           }
                                           else
                                           {
                                               if ([Artist valueForKey:@"name"] != nil)
                                               {
                                                   ArtistName =  [NSString stringWithFormat:(@"%@,%@"),ArtistName,[Artist valueForKey:@"name"]];
                                               }
                                               
                                           }
                                           NSLog(@"%@",[Artist valueForKey:@"name"]);
                                           lblArtistName.text = ArtistName;
                                       }
                                       
                                       [CollectionArtist reloadData];
                                       [CollectionFeaturedArtist reloadData];
                                       [collectionPlaylist reloadData];
                                       NSString * ConcertCount = [NearMeData valueForKey:@"totalshow"];
                                       lblConcertCount.text = [NSString stringWithFormat:(@"%@ Concert"),ConcertCount];
                                       
                                       NSString * TeackCount = [NearMeData valueForKey:@"totaltrack"];
                                       lblTrackCount.text = [NSString stringWithFormat:(@"%@ Tracks"),TeackCount];
                                       
                                   }else{
                                       
                                       
                                       
                                   }
                                   
                               }
                           }];
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == CollectionArtist)
    {
        ConcertPlayListViewController *controller = [[ConcertPlayListViewController alloc]initWithNibName:@"ConcertPlayList" bundle:nil];
        controller.index = [NearMeData valueForKey:@"play_id"];
        
        [self.navigationController pushViewController:controller animated:YES];
       
    }

    if (collectionView == collectionPlaylist)
    {
        ConcertPlayListViewController *controller = [[ConcertPlayListViewController alloc]initWithNibName:@"ConcertPlayList" bundle:nil];
        controller.index = [[playlistArray objectAtIndex:(indexPath.row)]valueForKey:@"play_id"];
        controller.isFavSideMenu1 = false;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (collectionView == CollectionFeaturedArtist)
    {
        ArtistDetailViewController *controller = [[ArtistDetailViewController alloc]initWithNibName:@"ArtistDetailViewController" bundle:nil];
        NSDictionary *Artist = [FeaturedArtistData objectAtIndex:indexPath.row];
        controller.ArtistId = [Artist valueForKey:@"art_id"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}
- (IBAction)ChangeLocation:(id)sender {
    ChangeLocationViewController *Controller = [[ChangeLocationViewController alloc]initWithNibName:@"ChangeLocationViewController" bundle:nil];
    Controller.delegate = self;
    Controller.modalPresentationStyle = UIModalPresentationCustom;
    
    
    [self presentViewController:Controller animated:true completion:nil];
}


-(void)viewDidAppear:(BOOL)animated
{
    
}

-(void)getAllLocation
{
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    [Utility executeRequestwithServicetype:GET_LOCATION withDictionary:[[NSMutableDictionary alloc]init] withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            LocationArray = [dictresponce valueForKey:@"DATA"];
            NSString *lat = [[LocationArray objectAtIndex:0]valueForKey:@"loc_latitude"];
            [[NSUserDefaults standardUserDefaults]setObject:lat forKey:Latitude];
            
            
            NSString *longitude = [[LocationArray objectAtIndex:0]valueForKey:@"loc_longitude"];
            [[NSUserDefaults standardUserDefaults]setObject:longitude forKey:Logitude];
            
            NSString *locationName = [[LocationArray objectAtIndex:0]valueForKey:@"loc_name"];
            [[NSUserDefaults standardUserDefaults]setObject:locationName forKey:LocationNameDefault];
            
            lblLocation.text = locationName;
            
            
            [self FetchPlaylistByLocation];
        }
        else
        {
            [self FetchPlaylistByLocation];
        }
        
        
        
    }];
}

//
//-(void)getUpcomingConcerts
//{
//
//    if(![Utility connectedToNetwork]){
//        [Utility showNetworkError];
//        return;
//    }
//
//
//    SHOWLOADING(@"Loading")
//    [Utility executeRequestwithServicetype:FETCH_FUTURE_CONCERTS withDictionary:[[NSMutableDictionary alloc]init] withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
//        STOPLOADING()
//       NSLog(@"%@",dictresponce);
//        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
//
//
//
//            NSString *imageURl=[FutureContestShow valueForKey:@"image"];
//            NSString *TracksCount=[FutureContestShow valueForKey:@"totaltrack"];
//            lblUpcomingArtistName.text = [FutureContestShow valueForKey:@"artistname"];
//
//            [imgUpcomingConcert sd_setImageWithURL:[NSURL URLWithString:imageURl]
//                                placeholderImage:[UIImage imageNamed:@""]];
//
//            imgUpcomingConcert.layer.borderWidth = 2.0;
//            imgUpcomingConcert.layer.borderColor = [UIColor colorWithRed:10.0/255.0 green:216.0/255.0 blue:248.0/255.0 alpha:1.0].CGColor;
//
//
//        }
//
//
//
//
//    }];
//}


- (IBAction)btnUpcomingConcertPressed:(id)sender {
    UpcomingConcertViewController *controller = [[UpcomingConcertViewController alloc]initWithNibName:@"UpcomingConcertViewController" bundle:nil];
    controller.isGetTicket = false;
    [self.navigationController pushViewController:controller animated:YES];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

