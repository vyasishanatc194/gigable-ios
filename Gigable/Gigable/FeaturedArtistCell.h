//
//  FeaturedArtistCell.h
//  Gigable
//
//  Created by AppDev03 on 1/16/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeaturedArtistCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgFituredArtist;
@property (strong, nonatomic) IBOutlet UILabel *lblfeaturedArtistName;

@end









