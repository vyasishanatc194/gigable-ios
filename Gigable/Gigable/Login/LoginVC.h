//
//  LoginVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>


@interface LoginVC : UIViewController<GIDSignInUIDelegate, GIDSignInDelegate>
{
    GIDSignIn * signIn;
  
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *errro2;
    IBOutlet UILabel *errro1;
    IBOutlet UILabel *sepPwd;
    IBOutlet UILabel *sepLogin;
    IBOutlet UIView *loginView;
     IBOutlet UITextField *password;
     IBOutlet UITextField *email;
    IBOutlet UIButton *btnFb;
    NSDictionary *dictFb;
    IBOutlet UIButton *btnGoog;

}
- (IBAction)btnSignupClicked:(id)sender;
- (IBAction)btnForgotPasswordClicked:(id)sender;
- (IBAction)btnLoginClicked:(id)sender;
- (IBAction)loginWithFacebook:(id)sender;
- (IBAction)loginWithGoogle:(id)sender;

@end
