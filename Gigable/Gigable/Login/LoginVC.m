//
//  LoginVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "LoginVC.h"
#import "DashboardVC.h"
#import "AppDelegate.h"
#import "Static.h"
#import "Utility.h"
#import "SignUpVC.h"
#import "ForgotPasswordVC.h"
#import "kFacebook.h"
#import "NSObject+SBJSON.h"
#import "ASIFormDataRequest.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@interface LoginVC ()<UITextFieldDelegate>

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [email setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [password setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    if(HEIGHT <500){
        [scrollView setContentSize:CGSizeMake(WIDTH, HEIGHT+100)];
    }
    [loginView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
//    [12/05/16, 10:58:29 PM] Ishan Vyas: abc@abc.com
//    [12/05/16, 10:58:32 PM] Ishan Vyas: verification
//    [12/05/16, 10:58:34 PM] Ishan Vyas: ame kadi didhu
//    [12/05/16, 10:58:35 PM] Ishan Vyas: for now
//        [12/05/16, 10:58:39 PM] Ishan Vyas: pwd 123456
     // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSignupClicked:(id)sender {
    [self.navigationController pushViewController:[[SignUpVC alloc]initWithNibName:@"SignUpVC" bundle:nil] animated:true];

}

- (IBAction)btnForgotPasswordClicked:(id)sender {
    [self.navigationController pushViewController:[[ForgotPasswordVC alloc]initWithNibName:@"ForgotPasswordVC" bundle:nil] animated:true];
}

#pragma mark- Textfield Delegate MEthods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    [self resetFr];
    return  true;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    [textField resignFirstResponder];
    return true;
}

-(void)resetFr
{
    
    CGRect frLb=sepLogin.frame;
    frLb.size.height=1.0;
    sepLogin.frame=frLb;
    errro1.text=@"";
    
    CGRect frLb1=sepPwd.frame;
    frLb1.size.height=1.0;
    sepPwd.frame=frLb1;
    errro2.text=@"";
    
}
#pragma mark- LOogin MEthods
- (IBAction)btnLoginClicked:(id)sender;
{
    if([Utility isStringEmpty:email.text]){
//        [Utility showAlrtWithMessage:@"Please enter email"];
        CGRect frLb=sepLogin.frame;
        frLb.size.height=3.0;
        sepLogin.frame=frLb;
        errro1.text=@"Please enter email";
        
        return;
    }
    
    if(![Utility NSStringIsValidEmail:email.text]){
        CGRect frLb=sepLogin.frame;
        frLb.size.height=3.0;
        sepLogin.frame=frLb;
        errro1.text=@"Please enter valid email";
        return;
    }
    
    if([Utility isStringEmpty:password.text]){
        CGRect frLb1=sepPwd.frame;
        frLb1.size.height=3.0;
        sepPwd.frame=frLb1;
        errro2.text=@"Please enter password";

        
        return;
    }
    
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    
    
    NSString *post = [NSString stringWithFormat: @"lgn_email=%@&lgn_password=%@",email.text,password.text ];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,USER_LOGIN]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               STOPLOADING()
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   NSMutableDictionary * dictRes = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   //NSLog(@"%@",dictRes);
                                   
                                   if([[dictRes valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       [[NSUserDefaults standardUserDefaults]setObject:[dictRes valueForKey:@"DATA"] forKey:KEY_LOGINDICT];
                                       [[NSUserDefaults standardUserDefaults]synchronize];
                                       [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictRes valueForKey:@"DATA"]];
                                       [[AppDelegate initAppdelegate]showDashboardMenu];
                                   }else{
                                       
                                       CGRect frLb=sepLogin.frame;
                                       frLb.size.height=3.0;
                                       sepLogin.frame=frLb;
                                       errro1.text=[[dictRes valueForKey:@"MESSAGES"]objectAtIndex:0];

                                   }
                               }
                           }];

    
    
}

- (IBAction)loginWithFacebook:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
  //  [login logOut];

    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             //NSLog(@"Process error");
         } else if (result.isCancelled) {
             //NSLog(@"Cancelled");
         } else {
             //NSLog(@"Logged in");
             
             if ([result.grantedPermissions containsObject:@"email"]) {
                 if ([FBSDKAccessToken currentAccessToken]) {
                     NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                     [parameters setValue:@"id,name,email" forKey:@"fields"];
                     
                     
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                          if (!error) {
                              dictFb=result;
                              [self checkUserIdForFacebook:[result valueForKey:@"id"] ];

                              //NSLog(@"fetched user:%@", result);
                          }
                      }];
                 }
             }
//                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
//                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//                      if (!error) {
//                          //NSLog(@"fetched user:%@", result);
//                      }
//                  }];
         }
     }];
    
    
       /* [kFacebook FacebookLogin:^(NSDictionary *dictonary, BOOL success) {
            if (success) {
                //NSLog(@"SuccessfullyLogin");
                if(!dictFb){
                dictFb=dictonary;
                [self checkUserIdForFacebook:[dictonary valueForKey:@"id"] ];
//                    [kFacebook FacebookLogOut:^(BOOL success) {
//                        
//                    } ];
                }
               
            }else
                //NSLog(@"Login Fail");
        }];
    */
    
}
-(void)checkUserIdForFacebook:(NSString *)id_fb
{
    
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *myDictInJSON =[dictFb JSONRepresentation];
    
    
   
    SHOWLOADING(@"Loading")
    NSString *postStr1=[NSString stringWithFormat:@"usr_name=%@&lgn_fb_auth_id=%@&lgn_email=%@&usr_gcm_number=%@&usr_imei_number=%@",[dictFb valueForKey:@"name"],[dictFb valueForKey:@"id"],[dictFb valueForKey:@"email"],@"gfh",@"asr3e"];
    
    [Utility executePOSTRequestwithServicetype:CHECKFBLOGIN withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [[NSUserDefaults standardUserDefaults]setObject:[dictresponce valueForKey:@"DATA"] forKey:KEY_LOGINDICT];
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:KEY_Social];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictresponce valueForKey:@"DATA"]];
            [[AppDelegate initAppdelegate]showDashboardMenu];
            
        }else{
            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
        }

    }];
    

    
    
    return;
  /*  ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,CHECKFBLOGIN]]];
    [request setRequestMethod:@"POST"];
    [request setPostValue:@"1qw1" forKey:@"usr_imei_number"];
    [request setPostValue:@"11" forKey:@"usr_gcm_number"];
    [request setPostValue:[dictFb valueForKey:@"name"] forKey:@"usr_name"];
    [request setPostValue:[dictFb valueForKey:@"id"] forKey:@"lgn_fb_auth_id"];
    [request setPostValue:[dictFb valueForKey:@"email"] forKey:@"lgn_email"];

    
      [request setDelegate:self];
    [request start];
    [request setCompletionBlock:^{
        
        //NSLog(@"Responce : %@",[[NSString alloc]initWithData:request.responseData encoding:NSUTF8StringEncoding]);
        
        NSError *error1;
        
        NSMutableDictionary * dictresponce = [NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:&error1];
        
        
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [[NSUserDefaults standardUserDefaults]setObject:[dictresponce valueForKey:@"DATA"] forKey:KEY_LOGINDICT];
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:KEY_Social];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictresponce valueForKey:@"DATA"]];
            [[AppDelegate initAppdelegate]showDashboardMenu];
            
        }else{
            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
        }

      
    }];
    
    [request setFailedBlock:^{
        
        //NSLog(@"%@", request.error);
    }];
    
    [request startAsynchronous];
    
    
   
    */
    
    
   }
- (void)requestStarted:(ASIHTTPRequest *)request;
{
SHOWLOADING(@"Loading")
}
- (void)request:(ASIHTTPRequest *)request didReceiveResponseHeaders:(NSDictionary *)responseHeaders;
{
    //NSLog(@"%@",responseHeaders);
}
- (void)requestFinished:(ASIHTTPRequest *)request;
{
    STOPLOADING()
}
- (void)requestFailed:(ASIHTTPRequest *)request;
{
    STOPLOADING()
}

- (IBAction)loginWithGoogle:(id)sender {
    

    signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;
    
    [[GIDSignIn sharedInstance] signIn];
}


/*gmail signin with google id name and email and gender....*/
//

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    /*When cancel button is clicked on G+ login page...*/
    if (error) {
        //NSLog(@"clicked on cancel");
    }
    /*When the user name and password entered and clicked on sign in...*/
    else {
        
        //NSLog(@"%@",user);
        
               
     if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
        
        SHOWLOADING(@"Loading")
        NSString *postStr1=[NSString stringWithFormat:@"usr_name=%@&lgn_google_auth_id=%@&lgn_email=%@&usr_imei_number=%@&usr_gcm_number=%@",user.profile.name,user.userID,user.profile.email,@"gfh",@"asr3e"];
        
        [Utility executePOSTRequestwithServicetype:GOOGLELOGIN withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
            STOPLOADING()
            if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                [[NSUserDefaults standardUserDefaults]setObject:[dictresponce valueForKey:@"DATA"] forKey:KEY_LOGINDICT];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictresponce valueForKey:@"DATA"]];
                
                [[NSUserDefaults standardUserDefaults]setBool:true forKey:KEY_Social];
                
                [[AppDelegate initAppdelegate]showDashboardMenu];
                
                [[GIDSignIn sharedInstance] signOut];
                
                
                //                [[AppDelegate initAppdelegate]setQueScreen];
            }else{
                [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
            }

        }];
        

        return;
        
        
      /*  ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,GOOGLELOGIN]]];
        [request setRequestMethod:@"POST"];
        [request setPostValue:user.profile.name forKey:@"usr_name"];
        [request setPostValue:user.userID forKey:@"lgn_google_auth_id"];
        [request setPostValue:user.profile.email forKey:@"lgn_email"];
        [request setPostValue:@"11k" forKey:@"usr_imei_number"];
        [request setPostValue:@"11" forKey:@"usr_gcm_number"];

        
        
        [request setDelegate:self];
        [request start];
        [request setCompletionBlock:^{
            
            //NSLog(@"Responce : %@",[[NSString alloc]initWithData:request.responseData encoding:NSUTF8StringEncoding]);
            
            NSError *error1;
            
            NSMutableDictionary * dictresponce = [NSJSONSerialization                                                JSONObjectWithData:request.responseData options:kNilOptions error:&error1];
            
            if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                [[NSUserDefaults standardUserDefaults]setObject:[dictresponce valueForKey:@"DATA"] forKey:KEY_LOGINDICT];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictresponce valueForKey:@"DATA"]];
                
                [[NSUserDefaults standardUserDefaults]setBool:true forKey:KEY_Social];
                
                [[AppDelegate initAppdelegate]showDashboardMenu];
                
                [[GIDSignIn sharedInstance] signOut];
                
                
                //                [[AppDelegate initAppdelegate]setQueScreen];
            }else{
                [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
            }
            
        }];
        [request setFailedBlock:^{
            
            //NSLog(@"%@", request.error);
        }];
        [request startAsynchronous];
        
        
        */
        
        
        
       
        // Perform any operations on signed in user here.
        
        //Saving the login type as gmail in order to open to the same page when the app is closed and opened
        //        [[NSUserDefaults standardUserDefaults] setObject:@"Gmail" forKey:@"LoginType"];
        //        [[NSUserDefaults standardUserDefaults]synchronize];
        //
        //        //setting the user details such as name, id, token, email, image to variable
        //              //
        //        //setting the user details such as name, id, token, email, image to NSUserDefaults
        //        [[NSUserDefaults standardUserDefaults]setObject:Loginname forKey:@"name"];
        //        [[NSUserDefaults standardUserDefaults]synchronize];
        //
        //        [[NSUserDefaults standardUserDefaults]setObject:Loginemail forKey:@"email"];
        //        [[NSUserDefaults standardUserDefaults]synchronize];
        //
        //        [[NSUserDefaults standardUserDefaults]setObject:LoginuserId forKey:@"id"];
        //        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        if (![GIDSignIn sharedInstance].currentUser.profile.hasImage) {
            // There is no Profile Image to be loaded.
            return;
        }
        //        // Load avatar image asynchronously, in background
        //        LoginimageURL =[[GIDSignIn sharedInstance].currentUser.profile imageURLWithDimension:200];
        //
        //        [[NSUserDefaults standardUserDefaults]setObject:[LoginimageURL absoluteString]  forKey:@"imageURL"];
        //        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        
        
    }
    
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //    [myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//when the gmail login got disconnected...this function will get called...
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
    if (error) {
        //NSLog(@"error occured");
    }
}




@end
