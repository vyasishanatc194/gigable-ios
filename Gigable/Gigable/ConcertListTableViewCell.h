//
//  ConcertListTableViewCell.h
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConcertListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblConcertDate;
@property (strong, nonatomic) IBOutlet UILabel *lblConcertPlace;
@property (strong, nonatomic) IBOutlet UILabel *lblConcertPrice;
@end
