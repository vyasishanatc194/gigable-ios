//
//  FeaturedTrackTableViewCell.h
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeaturedTrackTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbltrackTitle;
@property (strong, nonatomic) IBOutlet UIView *viewBorder;
@property (strong, nonatomic) IBOutlet UILabel *lblplaylistname;

@end
