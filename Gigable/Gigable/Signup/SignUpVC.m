//
//  SignUpVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//
#import "MFSideMenu.h"
#import "kFacebook.h"
#import "SignUpVC.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "Static.h"
#import "User.h"
@interface SignUpVC ()

@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [email setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [password setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [userName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [phone setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [confirmPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [location setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];

    // Do any additional setup after loading the view from its nib.
}
- (IBAction)showSideMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //        [self setupMenuBarButtonItems];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (IBAction)btnSignupClicked:(id)sender {
    if([userName.text isEqualToString:@""]){
        CGRect frLb=sepName.frame;
        frLb.size.height=3.0;
        sepName.frame=frLb;
        errName.text=@"Please enter user name";
       // [Utility showAlrtWithMessage:@"Please enter user name"];
                return;
    }

    
    if([email.text isEqualToString:@""]){
        
        CGRect frsepMail=sepMail.frame;
        frsepMail.size.height=3.0;
        sepMail.frame=frsepMail;
        errMail.text=@"Please enter email";

//        [Utility showAlrtWithMessage:@"Please enter email"];
        return;
    }
    
    if(![Utility NSStringIsValidEmail:email.text])
    {
        CGRect frsepMail=sepMail.frame;
        frsepMail.size.height=3.0;
        sepMail.frame=frsepMail;
        errMail.text=@"Please enter valid email";

//        [Utility showAlrtWithMessage:@"Please enter valid email"];
        return;
    }
    if([password.text isEqualToString:@""]){
        CGRect frsepPwd=sepPwd.frame;
        frsepPwd.size.height=3.0;
        sepPwd.frame=frsepPwd;
        errPwd.text=@"Please enter password";
//        [Utility showAlrtWithMessage:@"Please enter password"];
        return;
    }
    
    if([confirmPassword.text isEqualToString:@""]){
        CGRect frsepConf=sepConf.frame;
        frsepConf.size.height=3.0;
        sepConf.frame=frsepConf;
        errConf.text=@"Please enter confirm password";

//        [Utility showAlrtWithMessage:@"Please enter confirm password"];
        return;
    }
    
    
    if(![confirmPassword.text isEqualToString:password.text]){
        CGRect frsepPwd=sepPwd.frame;
        frsepPwd.size.height=3.0;
        sepPwd.frame=frsepPwd;
        errPwd.text=@"Password miss match";
        CGRect frsepConf=sepConf.frame;
        frsepConf.size.height=3.0;
        sepConf.frame=frsepConf;
        errConf.text=@"Password miss match";

//        [Utility showAlrtWithMessage:@"password and confirm password mismatch please enter same password for both"];
        return;
    }
//    if([phone.text isEqualToString:@""]){
//        CGRect frsepGen=sepGen.frame;
//        frsepGen.size.height=3.0;
//        sepGen.frame=frsepGen;
//        errGen.text=@"Please select Gender";
////        [Utility showAlrtWithMessage:@"Please select Gender"];
//        return;
//    }
    

   
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    
//    usr_name = Nimesh usr_phone = 9988779920 lgn_email = niks@gmail.com lgn_password = 123456 confirm_password = 123456 usr_imei_number = 545se usr_gcm_number = 5ffu4 usr_loc_id = 1
//    Location *objL=[arrLocation objectAtIndex:(location.tag-1)];

//    NSString *post = [NSString stringWithFormat: @"usr_name=%@&lgn_email=%@&lgn_password=%@&confirm_password=%@&usr_imei_number=%@&usr_gcm_number=%@&usr_loc_id=%@&usr_gender=%d&usr_phone=12", userName.text,email.text,password.text,confirmPassword.text,@"545se",@"5ffu4",objL.loc_id,phone.tag ];
    
    NSString *post = [NSString stringWithFormat: @"usr_name=%@&lgn_email=%@&lgn_password=%@&confirm_password=%@&usr_imei_number=%@&usr_gcm_number=%@&usr_gender=%ld&usr_phone=12", userName.text,email.text,password.text,confirmPassword.text,@"545se",@"5ffu4",phone.tag ];

    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,USER_SIGNUP]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               STOPLOADING()
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   NSMutableDictionary * dictRes = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   //NSLog(@"%@",dictRes);
                                   NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                                   if([[dictRes valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       [self .navigationController popViewControllerAnimated:true];
                                       [Utility showAlrtWithMessage:[[dictRes valueForKey:@"MESSAGES"] objectAtIndex:0]];
                                   }else {
                                       
                                       CGRect frsepMail=sepMail.frame;
                                       frsepMail.size.height=3.0;
                                       sepMail.frame=frsepMail;
                                       errMail.text=[[dictRes valueForKey:@"MESSAGES"] objectAtIndex:0];

                                       
//[Utility showAlrtWithMessage:[[dictRes valueForKey:@"MESSAGES"] objectAtIndex:0]];

                                   }
                               }
                           }];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    [self resetFr    ];
    if(textField==location){
        [scrollview setContentOffset:CGPointMake(0, 0) animated:true];
        [self getLocationFromWebservice];
        return false;
    }else if(textField==phone){
        [scrollview setContentOffset:CGPointMake(0, 0) animated:true];
        [self showGender];
        return false;
    }{
        if(textField==password || textField==confirmPassword )
        [scrollview setContentOffset:CGPointMake(0, 150) animated:true];
        [scrollview setContentSize:CGSizeMake(WIDTH, HEIGHT+100)];
        return true;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

-(void)resetFr
{
    
    CGRect frLb=sepName.frame;
    frLb.size.height=1.0;
    sepName.frame=frLb;
    errName.text=@"";
    
    CGRect frLb1=sepPwd.frame;
    frLb1.size.height=1.0;
    sepPwd.frame=frLb1;
    errPwd.text=@"";
    
    CGRect frsepMail=sepMail.frame;
    frsepMail.size.height=1.0;
    sepMail.frame=frsepMail;
    errMail.text=@"";
    
    CGRect frsepGen=sepGen.frame;
    frsepGen.size.height=1.0;
    sepGen.frame=frsepGen;
    errGen.text=@"";
    
    CGRect frsepConf=sepConf.frame;
    frsepConf.size.height=1.0;
    sepConf.frame=frsepConf;
    errConf.text=@"";
    
    CGRect frsepPwd=sepPwd.frame;
    frsepPwd.size.height=1.0;
    sepPwd.frame=frsepPwd;
    errConf.text=@"";
    
    
}
-(void)getLocationFromWebservice
{    return;

 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
[Utility executeRequestwithServicetype:GET_LOCATION withDictionary:[[NSMutableDictionary alloc]init] withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
    STOPLOADING()
    //NSLog(@"%@",dictresponce);
    if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
        if([[dictresponce valueForKey:@"COUNT"] intValue]>0){
    arrLocation=[[NSMutableArray alloc]init];
            NSMutableArray *arrTemp=[dictresponce valueForKey:@"DATA"];
            for(NSMutableDictionary *dict in arrTemp){
                Location *objl=[Location initWithResponceDict:dict];
                [arrLocation addObject:objl];
            }
            [self showLocationActionsheet];
        }
    }

    
    

}];
}
-(void)showGender
{
    alertGender = [[UIAlertView alloc]initWithTitle: @"Select Gender"                                               message: nil delegate: self cancelButtonTitle:@"Cancel"
                                     otherButtonTitles: @"Male",@"Female",@"Other",nil];
    
    [alertGender show];
    
}


-(void)showLocationActionsheet
{
    alertLocations = [[UIAlertView alloc]initWithTitle: @"Select Location"
                                               message: nil
                                              delegate: self
                                     cancelButtonTitle:@"Cancel"
                                     otherButtonTitles: nil];
    for(int i=0 ; i< arrLocation.count ; i++){
        Location *objL=[arrLocation objectAtIndex:i];
                [alertLocations addButtonWithTitle:objL.loc_name];

    }
    [alertLocations show];
   
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex   // after animation
{
    if(buttonIndex==alertView.cancelButtonIndex){
        return;
    }
    if(alertView==alertLocations){
        Location *objL=[arrLocation objectAtIndex:(buttonIndex-1)];
        location.text=objL.loc_name;
        location.tag=buttonIndex;

    }
    if(alertView==alertGender){
                phone.text=[alertView buttonTitleAtIndex:buttonIndex];
        phone.tag=buttonIndex;
    }
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


#pragma mark- GOOGLE QAND FACEBOOK SIGNUP METHODS


- (IBAction)signupWithFacebook:(id)sender {
    [kFacebook FacebookLogin:^(NSDictionary *dictonary, BOOL success) {
        if (success) {
            //NSLog(@"%@",dictonary);
            //NSLog(@"SuccessfullyLogin");
//            dictFb=dictonary;
            [self checkUserIdForFacebook:[dictonary valueForKey:@"id"] ];
            //            [kFacebook FacebookLogOut:^(BOOL success) {
            //
            //            }];
        }
            //NSLog(@"Login Fail");
    }];
}

-(void)checkUserIdForFacebook:(NSString *)id_fb
{
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *post = [NSString stringWithFormat:@"usr_fb_auth_id=%@",id_fb];
    
    [Utility executePOSTRequestwithServicetype:CHECKFBLOGIN withPostString:post withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        
        if(error){
        }else{
            //NSLog(@"%@",dictresponce);
            
            if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            }else{
                [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
            }
        }
        
    }];
    
    
}

- (IBAction)signupWithGoogle:(id)sender {
    signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;

    [[GIDSignIn sharedInstance] signIn];
}


/*gmail signin with google id name and email and gender....*/
//

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    /*When cancel button is clicked on G+ login page...*/
    if (error) {
        //NSLog(@"clicked on cancel");
    }
    /*When the user name and password entered and clicked on sign in...*/
    else {
        
        //NSLog(@"%@",user);
        // Perform any operations on signed in user here.
        
        //Saving the login type as gmail in order to open to the same page when the app is closed and opened
//        [[NSUserDefaults standardUserDefaults] setObject:@"Gmail" forKey:@"LoginType"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//        
//        //setting the user details such as name, id, token, email, image to variable
//        Loginemail = user.profile.email;
//        LoginidToken = user.authentication.idToken;
//        Loginname = user.profile.name;
//        LoginuserId = user.userID;
//        
//        //setting the user details such as name, id, token, email, image to NSUserDefaults
//        [[NSUserDefaults standardUserDefaults]setObject:Loginname forKey:@"name"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//        
//        [[NSUserDefaults standardUserDefaults]setObject:Loginemail forKey:@"email"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//        
//        [[NSUserDefaults standardUserDefaults]setObject:LoginuserId forKey:@"id"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        if (![GIDSignIn sharedInstance].currentUser.profile.hasImage) {
            // There is no Profile Image to be loaded.
            return;
        }
//        // Load avatar image asynchronously, in background
//        LoginimageURL =[[GIDSignIn sharedInstance].currentUser.profile imageURLWithDimension:200];
//        
//        [[NSUserDefaults standardUserDefaults]setObject:[LoginimageURL absoluteString]  forKey:@"imageURL"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        
        
    }
    
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
//    [myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//when the gmail login got disconnected...this function will get called...
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
    if (error) {
        //NSLog(@"error occured");
    }
}





@end
@implementation UITextField (custom)
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 8,
                      bounds.size.width - 20, bounds.size.height - 16);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}
@end