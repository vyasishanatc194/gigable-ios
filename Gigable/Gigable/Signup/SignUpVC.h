//
//  SignUpVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>

@interface SignUpVC : UIViewController<UITextFieldDelegate,UIActionSheetDelegate,GIDSignInUIDelegate, GIDSignInDelegate>
{
    GIDSignIn * signIn;
    
     IBOutlet UIScrollView *scrollview;
     IBOutlet UITextField *userName,*phone,*email,*password,*confirmPassword;
     IBOutlet UITextField *location;
    UIAlertView *alertLocations,*alertGender;
    NSMutableArray *arrLocation;
    IBOutlet UILabel *sepName;
    IBOutlet UILabel *errName;
    IBOutlet UILabel *sepMail;
    IBOutlet UILabel *errMail;
    IBOutlet UILabel *sepGen;
    IBOutlet UILabel *errGen;
    IBOutlet UILabel *sepConf;
    IBOutlet UILabel *errConf;
    IBOutlet UILabel *errPwd;
    IBOutlet UILabel *sepPwd;
}
- (IBAction)btnSignupClicked:(id)sender;
- (IBAction)signupWithFacebook:(id)sender;
- (IBAction)signupWithGoogle:(id)sender;
@end
