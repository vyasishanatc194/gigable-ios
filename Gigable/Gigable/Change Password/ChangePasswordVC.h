//
//  LoginVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController
{
    IBOutlet UIView *topView;
     IBOutlet UITextField *oldPwd;
     IBOutlet UIView *resetPwd;
     IBOutlet UITextField *newPwd;
     IBOutlet UITextField *confirmPwd;
}

- (IBAction)btnResetPasswordClicked:(id)sender;
@end
