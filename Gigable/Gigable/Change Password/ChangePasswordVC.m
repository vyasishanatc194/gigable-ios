//
//  LoginVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "DashboardVC.h"
#import "AppDelegate.h"
#import "Static.h"
#import "Utility.h"
#import "SignUpVC.h"
#import "MFSideMenu.h"
@interface ChangePasswordVC ()<UITextFieldDelegate>

@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [newPwd setValue:NAVNBAR_COLOR forKeyPath:@"_placeholderLabel.textColor"];
    [confirmPwd setValue:NAVNBAR_COLOR forKeyPath:@"_placeholderLabel.textColor"];

    [oldPwd setValue:NAVNBAR_COLOR forKeyPath:@"_placeholderLabel.textColor"];
    [AppDelegate initAppdelegate].playerView.hidden=true;
    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //        [self setupMenuBarButtonItems];
    }];
//    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)showSideMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //        [self setupMenuBarButtonItems];
    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    [textField resignFirstResponder];
    return true;
}



- (IBAction)btnResetPasswordClicked:(id)sender

{
    
    if([Utility isStringEmpty:oldPwd.text]){
        [Utility showAlrtWithMessage:@"Please enter old Password"];
        return;
    }

    
    if([Utility isStringEmpty:newPwd.text]){
        [Utility showAlrtWithMessage:@"Please enter new Password"];
        return;
    }
    
    if([Utility isStringEmpty:confirmPwd.text]){
        [Utility showAlrtWithMessage:@"Please enter confirm Password"];
        return;
    }
    
    
    if(![newPwd.text isEqualToString:confirmPwd.text])
    {
        [Utility showAlrtWithMessage:@"Please enter same password for new password and confirm Password"];
        return;
    }
    [newPwd resignFirstResponder];
    [confirmPwd resignFirstResponder];
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *post = [NSString stringWithFormat:@"lgn_id=%@&lgn_password=%@&new_password=%@",[AppDelegate initAppdelegate].objUser.user_id,oldPwd.text,newPwd.text];
    
    [Utility executePOSTRequestwithServicetype:CHANGE_PWD withPostString:post withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        //NSLog(@"%@",dictresponce);
        STOPLOADING()
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            oldPwd.text=nil;
            newPwd.text=nil;
            confirmPwd.text=nil;
            
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            
            [self.navigationController popViewControllerAnimated:true];
            
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
        
    }];
    
    
    
    
}


@end
