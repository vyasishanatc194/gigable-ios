//
//  ArtistCellCollectionViewCell.h
//  Gigable
//
//  Created by AppDev03 on 1/16/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistCellCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgArtist;

@end
