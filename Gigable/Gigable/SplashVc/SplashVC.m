//
//  SplashVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "SplashVC.h"
#import "Utility.h"
#import "Static.h"
#import "LoginVC.h"
#import "SignUpVC.h"
#import "AppDelegate.h"
@interface SplashVC ()

@end

@implementation SplashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"GIGABLE";
    [Utility setNavigationBar:self.navigationController];
    [btnJoin setBackgroundColor:NAVNBAR_COLOR_LIGHT];
//    [[AppDelegate initAppdelegate]setPlayer];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)goToEmail:(id)sender {
}
- (IBAction)goToSignup:(id)sender {
    [self.navigationController pushViewController:[[SignUpVC alloc]initWithNibName:@"SignUpVC" bundle:nil] animated:true];

    
}
- (IBAction)goToLogin:(id)sender {
    [self.navigationController pushViewController:[[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil] animated:true];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    [textField resignFirstResponder];
    return true;
}
@end
