//
//  SplashVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "LoadingVc.h"
#import "Utility.h"
#import "Static.h"
#import "LoginVC.h"
#import "SignUpVC.h"
#import "AppDelegate.h"

@interface LoadingVc ()

@end

@implementation LoadingVc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"GIGABLE";
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(moreProgress)
                                           userInfo:Nil repeats:YES];
    [Utility setNavigationBar:self.navigationController];
//    [[AppDelegate initAppdelegate]setPlayer];
    // Do any additional setup after loading the view from its nib.
//    [self performSelector:@selector(sethomeView) withObject:nil afterDelay:5.0];

}
-(void)sethomeView{
    [[AppDelegate initAppdelegate]showLoginMenu];
}
- (void) moreProgress {
    progressbar.progress += 0.015;
    if(progressbar.progress==1.0){
        [timer invalidate];
        timer=nil;
        [self sethomeView];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)goToEmail:(id)sender {
}
- (IBAction)goToSignup:(id)sender {
    [self.navigationController pushViewController:[[SignUpVC alloc]initWithNibName:@"SignUpVC" bundle:nil] animated:true];

    
}
- (IBAction)goToLogin:(id)sender {
    [self.navigationController pushViewController:[[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil] animated:true];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    [textField resignFirstResponder];
    return true;
}
@end

@implementation UIProgressView (customView)
- (CGSize)sizeThatFits:(CGSize)size {
    CGSize newSize = CGSizeMake(self.frame.size.width, 9);
    return newSize;
}
@end