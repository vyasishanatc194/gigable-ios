//
//  SplashVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashVC : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UIButton *btnJoin;
     IBOutlet UITextField *email;
    IBOutlet UIProgressView *progressbar;
    NSTimer *timer;
}

@end
