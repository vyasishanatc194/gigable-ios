//
//  SplashVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingVc : UIViewController<UITextFieldDelegate>
{
    
     IBOutlet UITextField *email;
    IBOutlet UIProgressView *progressbar;
    NSTimer *timer;
}

@end
