//
//  ChangeLocationViewController.m
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import "ChangeLocationViewController.h"

#import "ArtistDetailViewController.h"
#import "ConcertPlayListViewController.h"
#import "HomeDashboardViewController.h"
#import "HomeDashboardViewController.h"
#import "AppDelegate.h"
#import "Static.h"
#import "Utility.h"
#import "ArtistCellCollectionViewCell.h"
#import "FeaturedArtistCell.h"

#import "UIImageView+WebCache.h"

#import "LocationListCell.h"

@interface ChangeLocationViewController ()
{
    NSMutableArray *LocationArray;
    
}
@end

@implementation ChangeLocationViewController

@synthesize delegate;


- (void)viewDidLoad {
    [super viewDidLoad];
    LocationArray = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getAllLocation];
}


//- (void) myMethodToDoStuff {
//    [self.delegate getLocation:self]; //this will call the method implemented in your other class
//}


-(void)getAllLocation
{
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    [Utility executeRequestwithServicetype:GET_LOCATION withDictionary:[[NSMutableDictionary alloc]init] withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            LocationArray = [dictresponce valueForKey:@"DATA"];
            [_tblLocationList reloadData];
            
        }
        
        
        
        
    }];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     [tableView registerNib:[UINib nibWithNibName:@"LocationListCell" bundle:nil] forCellReuseIdentifier:@"LocationListCell"];
    LocationListCell *Cell = [tableView dequeueReusableCellWithIdentifier:@"LocationListCell"];
    Cell.selectionStyle = UITableViewCellSelectionStyleNone;
    Cell.LocationName.text = [[LocationArray objectAtIndex:indexPath.row]valueForKey:@"loc_name"];
    _tblLocationHeight.constant =  _tblLocationList.contentSize.height;
    return Cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return LocationArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *latitude = [[LocationArray objectAtIndex:indexPath.row]valueForKey:@"loc_latitude"];
    [[NSUserDefaults standardUserDefaults]setObject:latitude forKey:Latitude];
    
    NSString *longitude = [[LocationArray objectAtIndex:indexPath.row]valueForKey:@"loc_longitude"];
    [[NSUserDefaults standardUserDefaults]setObject:longitude forKey:Logitude];
    
    NSString *locationaname = [[LocationArray objectAtIndex:indexPath.row]valueForKey:@"loc_name"];
    [[NSUserDefaults standardUserDefaults]setObject:locationaname forKey:LocationNameDefault];
    
    [self.delegate getLocation:self];
    
    [self dismissViewControllerAnimated:false completion:nil];
}
- (IBAction)btnClosePressed:(id)sender {
    [self dismissViewControllerAnimated:false completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
