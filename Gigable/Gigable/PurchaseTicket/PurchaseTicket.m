//
//  PurchaseTicket.m
//  Gigable
//
//  Created by Kalpit Gajera on 17/02/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "PurchaseTicket.h"
#import "Utility.h"
#import "Static.h"
#import "AppDelegate.h"
#import <Social/Social.h>
#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"


@interface PurchaseTicket ()<SIMenuDelegate>
{
    AppDelegate *delegate;
    BOOL playerHidden;
}
@property (nonatomic, strong) SIMenuTable *table;
@property (nonatomic, strong) UIView *menuContainer;
@end

@implementation PurchaseTicket
@synthesize objShow;
@synthesize arrTic;
@synthesize ticketId,price,totalqty,ticketTypeId;
@synthesize playListDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    delegate=[AppDelegate initAppdelegate];
    playerHidden=[AppDelegate initAppdelegate].playerView.hidden;

    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
    lblPlayTitle.text=delegate.objPlaylist.playName;
    if(self.objShow){
        NSLog(@"%@",objShow);
        lblLocation.text=[objShow valueForKey:@"ven_name"];
        lblShowName.text=[objShow valueForKey:@"show_title"];
        
        NSString *dateString = [objShow valueForKey:@"show_updateddate"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *yourDate = [dateFormatter dateFromString:dateString];
//        dateFormatter.dateFormat = @"dd-MMM-yyyy";
         dateFormatter.dateFormat = @"MMM dd, yyyy";
        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
        lblShowDate.text=[dateFormatter stringFromDate:yourDate];
        delegate.playerView.hidden=true;
        [btntickType.layer setBorderColor:NAVNBAR_COLOR.CGColor];
        [btntickType.layer setBorderWidth:1.0];
    }

   
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
}
- (void)viewWillDisappear:(BOOL)animated; {
    [AppDelegate initAppdelegate].playerView.hidden=playerHidden;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addTickitCount:(UIButton *)sender {
   
    
    if(sender.tag==110){
        
        if([lblTickitCount.text integerValue]==[self.totalqty integerValue]){
            return;
        }
        
            lblTickitCount.text=[NSString stringWithFormat:@"%ld",[lblTickitCount.text integerValue]+1];
            lblTotalCount.text=[NSString stringWithFormat:@"%ld",[lblTickitCount.text integerValue]*[self.price integerValue]];
        //        plus
    }
    
    if(sender.tag==112){
        
        if([lblTickitCount.text integerValue]>0){
            lblTickitCount.text=[NSString stringWithFormat:@"%ld",[lblTickitCount.text integerValue]-1];
            lblTotalCount.text=[NSString stringWithFormat:@"%ld",[lblTickitCount.text integerValue]*[self.price integerValue]];
        }
        // Minus
        
    }
    
    
    
}

- (IBAction)goToPaymentScreen:(id)sender {
    if(isSelect==false){
//        [Utility showAlrtWithMessage:@"Please select ticket type"];
        [self showNotificationView:@"Please select ticket type" :[UIImage imageNamed:@"info.png"]];

        return;
    }
    if([lblTotalCount.text integerValue]==0){
        return;
    }
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    

    
    NSString *postStr=[NSString stringWithFormat:@"utkt_tkt_id=%@&utkt_lgn_id=%@&utkt_qty=%@&ticket_type_id=%@",self.ticketId,[AppDelegate initAppdelegate].objUser.user_id,lblTickitCount.text,self.ticketTypeId];
    [Utility executePOSTRequestwithServicetype:PURCHASE_TIC withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            Payment1 *objp=[[Payment1 alloc]initWithNibName:@"Payment1" bundle:nil];
            objp.orderId=[[dictresponce valueForKey:@"DATA"] valueForKey:@"OrderId"];
            objp.ticId=self.ticketId;
            objp.cost=lblTotalCount.text;
            objp.playListDelegate=self.playListDelegate;
            objp.showName=lblShowName.text;
             [self.navigationController pushViewController:objp animated:true];
        }else{
            [self showNotificationView:[dictresponce valueForKey:@"MESSAGES"] :[UIImage imageNamed:@"info.png"]];

//            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
        }
    }];

    
    
 
//        PaymentViewController *paymentViewController = [[PaymentViewController alloc] initWithNibName:nil bundle:nil];
//    paymentViewController.amount = [NSDecimalNumber decimalNumberWithString:@"10.00"];
//    paymentViewController.backendCharger = self;
//    paymentViewController.delegate = self;
//    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:paymentViewController];
//    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)showTicketPopup:(UIButton *)sender {
    if(dropDown == nil) {
        CGFloat f = 118;
        if(self.arrTic.count >0){
            NSMutableArray *arrName=[[NSMutableArray alloc]init];
                for(int i=0;i<self.arrTic.count;i++){
                    [arrName addObject:[[self.arrTic objectAtIndex:i] valueForKey:@"ticket_type_label"]];
                }
                dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arrName :nil :@"down"];
                dropDown.delegate = self;
            }else{
                [self showNotificationView:@"No Tickets found" :[UIImage imageNamed:@"info.png"]];

//                [Utility showAlrtWithMessage:@"No Tickets found"];
            }
        }
       
    }

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    
    if([btntickType.titleLabel.text isEqualToString:@"Select Seat Type"]){
        
    }else{
        for(int i=0;i<self.arrTic.count;i++){
            if([btntickType.titleLabel.text isEqualToString:[[self.arrTic objectAtIndex:i] valueForKey:@"ticket_type_label"]]){
                
                NSString *booked=[[self.arrTic objectAtIndex:i] valueForKey:@"ticket_booked"];
                NSString *qty=[[self.arrTic objectAtIndex:i] valueForKey:@"ticket_qty"];
                
                if([qty integerValue] >[booked integerValue]){
                    
                self.totalqty=[[self.arrTic objectAtIndex:i] valueForKey:@"ticket_qty"];
                self.ticketId=[[self.arrTic objectAtIndex:i] valueForKey:@"ticket_id"];
                self.ticketTypeId=[[self.arrTic objectAtIndex:i] valueForKey:@"ticket_type_id"];
                self.price=[[self.arrTic objectAtIndex:i] valueForKey:@"ticket_price"];
                    NSString *string = self.price;
                    
                    lblTickitCount.text=@"0";
                    lblTotalCount.text=@"0";
                    if ([string rangeOfString:@"$"].location == NSNotFound) {
                      
                    } else {
                        self.price = [self.price stringByReplacingOccurrencesOfString:@"$"
                                                                           withString:@""];
                    }
                    
                }
                isSelect=true;
                break;
            }
            
        }
    
    
}
}
-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

-(void)showNotificationView:(NSString *)msg :(UIImage *)img
{
    notiImg.image=img;
    lblMsg.text=msg;
       [viewNoti setBackgroundColor:[UIColor colorWithRed:125.0/255.0 green:33.0/255.0 blue:210.0/255.0 alpha:0.8]];
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y += frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:3.0];
                     }];
    
}
-(void)hideNotiView
{
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y -= frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         //                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:0.5];
                     }];
    
}



#pragma mark- Share Methods
- (IBAction)showShareButton:(id)sender {
    if(isShown){
        [shareButton setImage:[UIImage imageNamed:@"shareIconInactive.png"] forState:UIControlStateNormal];
        //        [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
        
        
        // Set delegate and selector to remove from superview when animation completes
        // Move this view to bottom of superview
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        CGRect frame = shareView.frame;
        frame.origin = CGPointMake(0.0,-self.view.bounds.size.height);
        shareView.frame = frame;
        [UIView commitAnimations];
        isShown=false;
        
    }else{
        [shareButton setImage:[UIImage imageNamed:@"shareIconActive.png"] forState:UIControlStateNormal];
        [shareView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
        [view_mainShare setBackgroundColor:[UIColor colorWithRed:15.0/255 green:176.0/255 blue:212.0/255 alpha:0.30]];
        CGRect frame = shareView.frame;
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        
        
        self.view.alpha=1.0f;
        frame.origin = CGPointMake(0.0, 0);
        shareView.frame = frame;
        //        [[shareView layer] addAnimation:animation forKey:@"Slidein"];
        [UIView commitAnimations];
        
        isShown=true;
        
    }
    
}
- (IBAction)showAppLink:(id)sender;
{
    
}

- (IBAction)shareOnTwitter:(id)sender {
    [self showShareButton:nil];
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//    {
//        
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}

- (IBAction)shareOnFacebook:(id)sender {
    [self showShareButton:nil];
    [AppDelegate initAppdelegate].playerView.hidden=true;
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeFacebook];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate initAppdelegate].playerView.hidden=false;
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}


#pragma mark- dropdown Methods

#pragma mark- ANIMATION METHODS
-(IBAction)showMenu:(id)sender{
    
    //    [self onShowMenu];
    if([AppDelegate initAppdelegate].arrPlayList.count==0){
        return;
    }
    [self onShowMenu];
    
}
- (void)onShowMenu
{
    if(menuButton.selected){
        menuButton.selected=false;
        
        [self onHideMenu];
    }else{
        //    if (!self.table) {
        UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect frame = mainWindow.frame;
        //        frame.origin.y += self.view.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
        
        frame.origin.y=80;
        frame.size.height-=150;
        NSMutableArray *iem=[[NSMutableArray alloc]init];
        
        
        if([AppDelegate initAppdelegate].arrPlayList.count >0){
            
            for(int i=0;i<[AppDelegate initAppdelegate].arrPlayList.count;i++){
                PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:i];
                [iem addObject:objs.playName];
            }
        }
        
        self.table = [[SIMenuTable alloc] initWithFrame:frame items:iem];
        self.table.menuDelegate = self;
        self.table.arrData=[AppDelegate initAppdelegate].arrPlayList;
        self.menuContainer=self.view;
        [self.menuContainer addSubview:self.table];
        [self rotateArrow:M_PI];
        [self.table show];
        menuButton.selected=true;
    }
}

- (void)onHideMenu
{
    [self rotateArrow:0];
    [self.table hide];
}

- (void)rotateArrow:(float)degrees
{
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        //        self.menuButton.arrow.layer.transform = CATransform3DMakeRotation(degrees, 0, 0, 1);
    } completion:NULL];
}

#pragma mark -
#pragma mark Delegate methods
- (void)didSelectPlayAtIndex:(NSUInteger)index
{
    [self.navigationController popToRootViewControllerAnimated:true];

//    [self backButtonPressed:nil];
    [self.playListDelegate didSelectPlayAtIndex:index];
    //    [Utility showAlrtWithMessage:@"Play select"];
    return;
    
    
}
- (void)didSelectPlayItemListItemAtIndex:(NSUInteger)index;
{
    [self.navigationController popToRootViewControllerAnimated:true];
    [self.playListDelegate didSelectPlayItemListItemAtIndex:index];
    return;
}
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    
    [self.navigationController popToRootViewControllerAnimated:true];
    [self.playListDelegate didSelectItemAtIndex:index];
    return;
    
    
}

@end
