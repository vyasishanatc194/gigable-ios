//
//  PurchaseTicket.h
//  Gigable
//
//  Created by Kalpit Gajera on 17/02/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Payment1.h"
#import <Stripe/Stripe.h>
#import "PlayListDelegate.h"


@interface PurchaseTicket : UIViewController<NIDropDownDelegate>
{
    
     IBOutlet UIView *topView;
    
    IBOutlet UIButton *btntickType;
    IBOutlet UILabel *lblTickitCount;
    IBOutlet UILabel *lblShowDate;
    IBOutlet UILabel *lblLocation;
    IBOutlet UILabel *lblShowName;
    NIDropDown *dropDown;
    BOOL isSelect;
    
    IBOutlet UIView *viewNoti;
    IBOutlet UIImageView *notiImg;
    IBOutlet UILabel *lblMsg;

    IBOutlet UILabel *lblTotalCount;
    
    
    IBOutlet UIButton *menuButton;
    IBOutlet UIView *view_mainShare;
    IBOutlet UIButton *shareButton;
    IBOutlet UIView *shareView;
    IBOutlet UILabel *lblPlayTitle;
    BOOL isShown;

}
@property (strong,nonatomic)NSString *ticketId,*price,*totalqty,*ticketTypeId;
@property (strong,nonatomic)Show *objShow;
@property (strong,nonatomic)NSMutableArray *arrTic;
- (IBAction)addTickitCount:(UIButton *)sender;
- (IBAction)goToPaymentScreen:(id)sender;
- (IBAction)showTicketPopup:(UIButton *)sender;
@property (nonatomic, weak) id <PlayListDelegate> playListDelegate;


@end
