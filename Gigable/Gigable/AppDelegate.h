//
//  AppDelegate.h
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//
#import "STKAudioPlayer.h"
#import "STKAutoRecoveringHTTPDataSource.h"
#import "SampleQueueId.h"

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "User.h"
#import <CoreLocation/CoreLocation.h>

#define SHOWLOADING(strText) [(AppDelegate *)[[UIApplication sharedApplication]delegate]performSelectorOnMainThread:@selector(showCustomProgressViewWithText:) withObject:strText waitUntilDone:NO];
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)
#define STOPLOADING() [(AppDelegate *)[[UIApplication sharedApplication]delegate]performSelector:@selector(hideCustomProgressView) withObject:nil afterDelay:0.5];
#define kClientID @"1079763300287-ghgl2dss19rpqm0uc1uj02k33fm61v2d.apps.googleusercontent.com"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
{
    UISlider *sliderAudio;
    
    UILabel *lblTrack_playing;
    UILabel *totalTime;
    UILabel *remainingTime;
    UIButton *play;
    NSTimer *updateTimer;
    UINavigationController *nav;
    UIButton *btnfav;
    NSMutableArray *arrShow;
    UIButton *ticket;
    
    UIButton *info;
    BOOL isPlaying;
}
@property (assign,nonatomic)BOOL isRef,isMyFav,isPushed,isPushedShow;;
@property (strong,nonatomic)UIButton *info;

@property (strong,nonatomic)UIView *playerView;
@property (strong,nonatomic)User *objUser;
@property (strong,nonatomic)NSMutableArray *arrLoaction;
@property (strong,nonatomic)    STKAudioPlayer* audioPlayer;
@property (strong,nonatomic)NSMutableArray *arrTracks;
@property (strong, nonatomic) UIWindow *window;
@property (assign,nonatomic)NSInteger index_current_song;
@property (strong,nonatomic)CLLocationManager *locationManager;
@property (strong,nonatomic)PlayList *objPlaylist;
@property (strong,nonatomic)NSMutableArray *arrPlayList;

-(void)showLoginMenu;
+(AppDelegate*)initAppdelegate;
-(void)showCustomProgressViewWithText:(NSString *)strText;
-(void)hideCustomProgressView;
-(void)showDashboardMenu;
-(void)playAudioWithIndex:(NSInteger)index;
-(void)pausePlayer;
-(void)playPlayer;

-(void)checkFavTrack:(Track *)objTr;
-(void)setPushTag;
-(void)showInfo;
-(void)showInfo:(NSInteger *)ind;
@end

