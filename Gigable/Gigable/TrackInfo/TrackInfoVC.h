//
//  TicketInfoVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 17/02/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "User.h"
#import "PlayListDelegate.h"

@interface TrackInfoVC : UIViewController
{
    IBOutlet AsyncImageView *imgArtist;

    IBOutlet UITextView *lblShowDesc;
    IBOutlet UILabel *lblShowDate;
    IBOutlet UILabel *lblLocation;
     IBOutlet UIView *topView;
    IBOutlet UILabel *lblShowName;
    IBOutlet UIButton *fb,*twitterr,*sound,*bc,*www;
    
    
    IBOutlet UIButton *menuButton;
    IBOutlet UIView *view_mainShare;
    IBOutlet UIButton *shareButton;
    IBOutlet UIView *shareView;
    IBOutlet UILabel *lblPlayTitle;
    BOOL isShown;
    
}
@property (strong,nonatomic)Track *objtr;

- (IBAction)openWebsiteLink:(UIButton *)sender;
@property (nonatomic, weak) id <PlayListDelegate> playListDelegate;

@end
