//
//  TicketInfoVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 17/02/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "TrackInfoVC.h"
#import "Utility.h"
#import "Static.h"
#import "PurchaseTicket.h"
#import "AppDelegate.h"
#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import <Social/Social.h>
#import "MFSideMenu.h"
#import "PlayListVC.h"
#import "DashboardVC.h"
@interface TrackInfoVC ()
<SIMenuDelegate>
{
    AppDelegate *delegate;
}
@property (nonatomic, strong) SIMenuTable *table;
@property (nonatomic, strong) UIView *menuContainer;
@end

@implementation TrackInfoVC
@synthesize objtr;
@synthesize playListDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    delegate=[AppDelegate initAppdelegate];
    lblPlayTitle.text=delegate.objPlaylist.playName;
   
    
    if(self.objtr){
        lblLocation.text=self.objtr.albumname;
        lblShowDesc.text=self.objtr.descr;
        lblShowName.text=[NSString stringWithFormat:@"by %@",self.objtr.art_name];
        
        imgArtist.imageURL=[NSURL URLWithString:self.objtr.img_track];
        
        NSString *dateString = self.objtr.createdDate;
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *yourDate = [dateFormatter dateFromString:dateString];
        dateFormatter.dateFormat = @"dd-MMM-yyyy";
        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
        lblShowDate.text=[dateFormatter stringFromDate:yourDate];
        [self getArtistDetail];

    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTrackInfo:) name:@"TRACKUPDATE" object:nil];

    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [delegate.info setImage:[UIImage imageNamed:@"infobuttonActive.png"] forState:UIControlStateNormal];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [delegate.info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];

}
-(void)changeTrackInfo:(NSNotification *) notification
{
    Track *objTr=(Track *)notification.object;
    self.objtr=objTr;
        lblLocation.text=self.objtr.albumname;
        lblShowDesc.text=self.objtr.descr;
        lblShowName.text=[NSString stringWithFormat:@"by %@",self.objtr.art_name];
        
        imgArtist.imageURL=[NSURL URLWithString:self.objtr.img_track];
        
        NSString *dateString = self.objtr.createdDate;
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *yourDate = [dateFormatter dateFromString:dateString];
        dateFormatter.dateFormat = @"dd-MMM-yyyy";
        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
        lblShowDate.text=[dateFormatter stringFromDate:yourDate];
    [self getArtistDetail];
    
}
-(void)getArtistDetail
{
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"art_id=%@",self.objtr.art_id ];
    [Utility executePOSTRequestwithServicetype:GET_ARTIST_DETAIL withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableDictionary *arr=[dictresponce valueForKey:@"DATA"];
            lblShowDesc.text=[arr valueForKey:@"art_shortbio"];
            imgArtist.imageURL=[NSURL URLWithString:[arr valueForKey:@"art_image"]];
      //      [lblShowDesc sizeToFit];
            if([[dictresponce valueForKey:@"art_abclink"]isEqualToString:@""]){
                [bc setBackgroundImage:[UIImage imageNamed:@"bandcamp_inactive.png"] forState:UIControlStateNormal];
                bc.enabled=false;
            }else{
                
                bc.accessibilityLabel=[dictresponce valueForKey:@"art_abclink"];
                [bc setBackgroundImage:[UIImage imageNamed:@"bandcamp_active.png"] forState:UIControlStateNormal];
                bc.enabled=true;
            }
            
            
            if([[dictresponce valueForKey:@"art_facebook"]isEqualToString:@""]){
                [fb setBackgroundImage:[UIImage imageNamed:@"fbIcon.png"] forState:UIControlStateNormal];
                fb.enabled=false;
            }else{
                fb.accessibilityLabel=[dictresponce valueForKey:@"art_facebook"];

                [fb setBackgroundImage:[UIImage imageNamed:@"fbIcon_active.png"] forState:UIControlStateNormal];
                fb.enabled=true;
            }
            
            if([[dictresponce valueForKey:@"art_soundcloud"]isEqualToString:@""]){
                [sound setBackgroundImage:[UIImage imageNamed:@"soundCloud.png"] forState:UIControlStateNormal];
                sound.enabled=false;
            }else{
                sound.accessibilityLabel=[dictresponce valueForKey:@"art_soundcloud"];
                [sound setBackgroundImage:[UIImage imageNamed:@"soundCloud_active.png"] forState:UIControlStateNormal];
                sound.enabled=true;
            }
            

            if([[dictresponce valueForKey:@"art_twitter"]isEqualToString:@""]){
                [twitterr setBackgroundImage:[UIImage imageNamed:@"twitter_Icon.png"] forState:UIControlStateNormal];
                twitterr.enabled=false;
            }else{
                twitterr.accessibilityLabel=[dictresponce valueForKey:@"art_twitter"];
                [twitterr setBackgroundImage:[UIImage imageNamed:@"twitter_Icon_active.png"] forState:UIControlStateNormal];
                twitterr.enabled=true;
            }

            if([[dictresponce valueForKey:@"art_website"]isEqualToString:@""]){
                [www setBackgroundImage:[UIImage imageNamed:@"website inactive.png"] forState:UIControlStateNormal];
                www.enabled=false;
            }else{
                www.accessibilityLabel=[dictresponce valueForKey:@"art_website"];
                [www setBackgroundImage:[UIImage imageNamed:@"website active.png"] forState:UIControlStateNormal];
                www.enabled=true;
            }

            
            
                   }else{
        }
    }];

}

-(IBAction)backButtonPressed:(id)sender
{
    [AppDelegate initAppdelegate].playerView.hidden=false;
    [[AppDelegate initAppdelegate].info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];
    [[AppDelegate initAppdelegate] setPushTag];
    self.navigationController.navigationBarHidden=true;
    [self.navigationController popViewControllerAnimated:true];

//    [UIView transitionWithView:self.navigationController.view
//                      duration:0.75
//                       options:UIViewAnimationOptionTransitionFlipFromLeft
//                    animations:^{
//                        [self.navigationController popViewControllerAnimated:true];
//                    }
//                    completion:nil];
}
-(IBAction)showSideMenu:(id)sender
{
[self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //        [self setupMenuBarButtonItems];
    }];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buyTicketPressed:(id)sender {
    }


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openWebsiteLink:(UIButton *)sender {
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:sender.accessibilityLabel]];
}


#pragma mark- dropdown Methods

#pragma mark- ANIMATION METHODS
-(IBAction)showMenu:(id)sender{
    
    //    [self onShowMenu];
    if([AppDelegate initAppdelegate].arrPlayList.count==0){
        return;
    }
    [self onShowMenu];
    
}
- (void)onShowMenu
{
    if(menuButton.selected){
        menuButton.selected=false;
        
        [self onHideMenu];
    }else{
        //    if (!self.table) {
        UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect frame = mainWindow.frame;
        //        frame.origin.y += self.view.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
        
        frame.origin.y=80;
        frame.size.height-=150;
        NSMutableArray *iem=[[NSMutableArray alloc]init];
        
        
        if([AppDelegate initAppdelegate].arrPlayList.count >0){
            
            for(int i=0;i<[AppDelegate initAppdelegate].arrPlayList.count;i++){
                PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:i];
                [iem addObject:objs.playName];
            }
        }
        
        self.table = [[SIMenuTable alloc] initWithFrame:frame items:iem];
        self.table.menuDelegate = self;
        self.table.arrData=[AppDelegate initAppdelegate].arrPlayList;
        self.menuContainer=self.view;
        [self.menuContainer addSubview:self.table];
        [self rotateArrow:M_PI];
        [self.table show];
        menuButton.selected=true;
    }
}

- (void)onHideMenu
{
    [self rotateArrow:0];
    [self.table hide];
}

- (void)rotateArrow:(float)degrees
{
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        //        self.menuButton.arrow.layer.transform = CATransform3DMakeRotation(degrees, 0, 0, 1);
    } completion:NULL];
}

#pragma mark -
#pragma mark Delegate methods
- (void)didSelectPlayAtIndex:(NSUInteger)index
{
    [self.navigationController popToRootViewControllerAnimated:true];
    
    //    [self backButtonPressed:nil];
    [self.playListDelegate didSelectPlayAtIndex:index];
    //    [Utility showAlrtWithMessage:@"Play select"];
    return;
    
    
}
- (void)didSelectPlayItemListItemAtIndex:(NSUInteger)index;
{
    [self.navigationController popToRootViewControllerAnimated:true];
    [self.playListDelegate didSelectPlayItemListItemAtIndex:index];
    return;
}
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    
//    [self.navigationController popToRootViewControllerAnimated:true];
//    [self.playListDelegate didSelectItemAtIndex:index];
//    return;
    
    [self.table hide];
    PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:index];
    
    if([objs.totaltrack integerValue]==0){
//        [self showNotificationView:@"No Tracks Found" :[UIImage imageNamed:@"info.png"]];
        return;
    }
    //       lblPlayTitle.text=objs.playName;
    
//    [self onHandleMenuTap:nil];
    
    [AppDelegate initAppdelegate].objPlaylist=objs;
//    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    [self showOverLayDetail:objs.play_id withName:objs.playName withObj:objs];
    
}


-(void)showOverLayDetail:(NSString *)idPlayList withName:(NSString *)name withObj:(PlayList *)objP
{
    //    [arrTracks removeAllObjects];
    //    [arrShow removeAllObjects];
    //    [tblView reloadData];
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",idPlayList,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTr,*arrSho;
            
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            if(arrTracs.count >0){
                arrTr=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrTracs)
                {
                    [arrTr addObject:[Track initWithResponceDict:dict]];
                }
                
            }
            //                arrTracks=arrTr;
            //            delegate.index_current_song=0;
            //            [delegate playAudioWithIndex:delegate.index_current_song];
            //            [delegate pausePlayer];
            if(arrShows.count >0){
                arrSho=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows)
                {
                    [arrSho addObject:[Show initWithResponceDict:dict]];
                }
                
            }else{
                
            }
            
            DashboardVC *objDash;
            
            for(UIViewController *vc in [self.navigationController viewControllers]){
                if([vc isKindOfClass:[DashboardVC class]]){
                    
                    objDash=(DashboardVC *)vc;
                    break;
                }
            }

            
            PlayListVC *objPlayList=[[PlayListVC alloc]initWithNibName:@"PlayListVC_New" bundle:nil];
            objPlayList.isTracks=true;
            objPlayList.playdelegate=objDash;
            objPlayList.isOverLay=true;
            objPlayList.arrShows=arrSho;
            
            objPlayList.currentShowName=name;
            objPlayList.arrData=arrTr;
            objPlayList.objPlayList=objP;
            
            [self.navigationController pushViewController:objPlayList animated:true];
        }else{
//            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
    
}

#pragma mark- Share Methods
- (IBAction)showShareButton:(id)sender {
    if(isShown){
        [shareButton setImage:[UIImage imageNamed:@"shareIconInactive.png"] forState:UIControlStateNormal];
        //        [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
        
        
        // Set delegate and selector to remove from superview when animation completes
        // Move this view to bottom of superview
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        CGRect frame = shareView.frame;
        frame.origin = CGPointMake(0.0,-self.view.bounds.size.height);
        shareView.frame = frame;
        [UIView commitAnimations];
        isShown=false;
        
    }else{
        [shareButton setImage:[UIImage imageNamed:@"shareIconActive.png"] forState:UIControlStateNormal];
        [shareView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
        [view_mainShare setBackgroundColor:[UIColor colorWithRed:15.0/255 green:176.0/255 blue:212.0/255 alpha:0.30]];
        CGRect frame = shareView.frame;
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        
        
        self.view.alpha=1.0f;
        frame.origin = CGPointMake(0.0, 0);
        shareView.frame = frame;
        //        [[shareView layer] addAnimation:animation forKey:@"Slidein"];
        [UIView commitAnimations];
        
        isShown=true;
        
    }
    
}
- (IBAction)showAppLink:(id)sender;
{
    
}

- (IBAction)shareOnTwitter:(id)sender {
    [self showShareButton:nil];
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//    {
//        
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}

- (IBAction)shareOnFacebook:(id)sender {
    [self showShareButton:nil];
    [AppDelegate initAppdelegate].playerView.hidden=true;
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeFacebook];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate initAppdelegate].playerView.hidden=false;
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}


@end

