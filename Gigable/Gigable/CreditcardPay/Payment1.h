//
//  Payment1.h
//  RideIn
//
//  Created by TTMac 5 on 07/05/15.
//  Copyright (c) 2015 TTMac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "NIDropDownMM.h"
#import <QuartzCore/QuartzCore.h>
#import <Stripe/Stripe.h>
#import "PlayListDelegate.h"

typedef NS_ENUM(NSInteger, STPBackendChargeResult) {
    STPBackendChargeResultSuccess,
    STPBackendChargeResultFailure,
};

typedef void (^STPTokenSubmissionHandler)(STPBackendChargeResult status, NSError *error);

@interface Payment1 : UIViewController<NIDropDownDelegate,UITextFieldDelegate>
{
    UITextField *txtcardno;
    UITextField *txtcvv;
    IBOutlet UIButton *btnCardType;
    UIButton *btnnext;
    UIButton *btnscancard;
    
    IBOutlet UIView *topView;
    IBOutlet UIButton *btnSelect;
    NIDropDown *dropDown;
    IBOutlet UIButton *btnSelectMM;
    NIDropDownMM *dropDownMM;
    IBOutlet UIButton *btnSelectYY;
    NIDropDownMM *dropDownYY;
    NSArray *arrcardid;
    NSArray *arrcardnm;
    NSArray *arrcardimg;
    NSMutableArray *mutarrcardid;
    NSMutableArray *mutarrcardnm;
    NSMutableArray *mutarrcardimg;
    
    IBOutlet UIScrollView *scrollView;
    int tag;
    IBOutlet UILabel *lblCost;
    IBOutlet UILabel *lblShowName;
    NSString *strcardi;
    NSString *strorgnlccno;
    NSString *strmonth;
    NSString *stryear;
    NSString *strCardHolderName;
    IBOutlet UIView *viewNoti;
    IBOutlet UIImageView *notiImg;
    IBOutlet UILabel *lblMsg;
    
    
    
    IBOutlet UIButton *menuButton;
    IBOutlet UIView *view_mainShare;
    IBOutlet UIButton *shareButton;
    IBOutlet UIView *shareView;
    IBOutlet UILabel *lblPlayTitle;
    BOOL isShown;


}
@property(nonatomic,strong) NSString *strcardi,*showName,*cost;
@property(nonatomic,strong) IBOutlet UITextField *txtCardHolderName;
@property(nonatomic,strong) IBOutlet UITextField *txtcardno;
@property(nonatomic,strong) IBOutlet UITextField *txtcvv;
@property(nonatomic,strong) IBOutlet UIButton *btnnext;
@property(nonatomic,strong) IBOutlet UIButton *btnscancard;
@property(nonatomic) int tag;
@property(nonatomic,strong) NSString *strorgnlccno;
@property (retain, nonatomic) IBOutlet UIButton *btnSelect;
@property(nonatomic,strong) NSString *strmonth,*orderId,*ticId;
@property(nonatomic,strong) NSString *stryear;
@property(nonatomic,strong) NSString *strCardHolderName;
- (IBAction)selectClicked:(id)sender;
-(void)rel;
@property (retain, nonatomic) IBOutlet UIButton *btnSelectMM;
- (IBAction)selectClickedMM:(id)sender;
-(void)relMM;
@property (retain, nonatomic) IBOutlet UIButton *btnSelectYY;
@property (nonatomic, weak) id <PlayListDelegate> playListDelegate;

- (IBAction)selectClickedYY:(id)sender;
-(void)relYY;

@property(nonatomic,strong) NSArray *arrcardnm;
@property(nonatomic,strong) NSArray *arrcardid;
@property(nonatomic,strong) NSArray *arrcardimg;
@property(nonatomic,strong) NSMutableArray *mutarrcardid;
@property(nonatomic,strong) NSMutableArray *mutarrcardnm;
@property(nonatomic,strong) NSMutableArray *mutarrcardimg;

@property(nonatomic,strong) IBOutlet UIButton *btnback;
@property(nonatomic,strong) IBOutlet UIButton *btnleft;
@property(nonatomic,strong) IBOutlet UIImageView *backimg;
- (IBAction)backToView:(id)sender;
- (IBAction)showCardSelection:(id)sender;

@end

