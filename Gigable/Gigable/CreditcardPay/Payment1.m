//
//  Payment1.m
//  RideIn
//
//  Created by TTMac 5 on 07/05/15.
//  Copyright (c) 2015 TTMac 5. All rights reserved.
//
#define ACCEPTABLE_CHARECTERS @"0123456789"
#define kLENGTH 4
#define Number @"0123456789"

#import "Payment1.h"
#import "Static.h"
#import "NIDropDown.h"
#import "NIDropDownMM.h"
#import "NIDropDownYY.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "Constants.h"
#import "ASIFormDataRequest.h"
#import "PaymentPageVC.h"
#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import <Social/Social.h>
@interface Payment1 ()<SIMenuDelegate>
{
    AppDelegate *delegate;
    BOOL playerHidden;

}
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) SIMenuTable *table;
@property (nonatomic, strong) UIView *menuContainer;
@end


@implementation Payment1
@synthesize orderId,ticId,showName,cost;
@synthesize btnSelect,tag,btnSelectMM,btnSelectYY,strcardi;
@synthesize txtcardno,txtcvv,btnnext,btnscancard,arrcardnm,arrcardid,arrcardimg,mutarrcardid,mutarrcardnm,mutarrcardimg,backimg,btnback,btnleft,strorgnlccno,strmonth,stryear,txtCardHolderName,strCardHolderName;
@synthesize playListDelegate;
- (void)viewDidLoad {
    //NSLog(@"%@",self.nibName);
    delegate=[AppDelegate initAppdelegate];
    [AppDelegate initAppdelegate].playerView.hidden=playerHidden;

    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
    lblPlayTitle.text=delegate.objPlaylist.playName;

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }    //NSLog(@"%@",self.nibName);
    
    lblShowName.text=self.showName;
    lblCost.text=self.cost;
    [super viewDidLoad];
    
    //*****Button radius*****//
//    btnnext.layer.cornerRadius = 7;
    btnnext.clipsToBounds = YES;
//    btnscancard.layer.cornerRadius = 7;
    btnscancard.clipsToBounds = YES;
    
    //***** Array Allocation *****//
    arrcardnm=[[NSArray alloc]init];
    arrcardid=[[NSArray alloc]init];
    arrcardimg=[[NSArray alloc]init];
    mutarrcardid=[[NSMutableArray alloc]init];
    mutarrcardnm=[[NSMutableArray alloc]init];
    mutarrcardimg=[[NSMutableArray alloc]init];
    if(HEIGHT <500){
        [scrollView setContentSize:CGSizeMake(WIDTH, HEIGHT+150)];
    }
    
    //*****Get Car Types API URL call*****//
    
    
    [btnCardType.layer setBorderColor:NAVNBAR_COLOR.CGColor];
    [btnCardType.layer setBorderWidth:1.0];
    
    btnSelect.layer.borderWidth = 0;
    btnSelect.layer.borderColor = [[UIColor blackColor] CGColor];
//    btnSelect.layer.cornerRadius = 5;
    
    btnSelectMM.layer.borderWidth = 0;
    btnSelectMM.layer.borderColor = [[UIColor blackColor] CGColor];
//    btnSelectMM.layer.cornerRadius = 5;
    
    btnSelectYY.layer.borderWidth = 0;
    btnSelectYY.layer.borderColor = [[UIColor blackColor] CGColor];
//    btnSelectYY.layer.cornerRadius = 5;
    
    btnSelect.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    // Do any additional setup after loading the view from its nib.
    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];

    
    [txtCardHolderName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [txtcardno setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [txtcvv setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [btnSelect setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSelectMM setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSelectYY setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [txtCardHolderName setTextColor:[UIColor whiteColor]];
        [txtcardno setTextColor:[UIColor whiteColor]];
        [txtcvv setTextColor:[UIColor whiteColor]];

    
}
- (void)viewWillDisappear:(BOOL)animated; {
    [AppDelegate initAppdelegate].playerView.hidden=false;

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    btnscancard.hidden=YES;
    [AppDelegate initAppdelegate].playerView.hidden=true;

  }

//***** Paypal scan card *****//
-(IBAction)paypalcard:(id)sender
{
  
}
#pragma mark - CardIOPaymentViewControllerDelegate



//***** keyboard return *****//
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [scrollView setContentOffset:CGPointMake(0, 0) animated:true];

    return YES;
}

//***** Remove ActivityView *****//
- (void)removeActivityView;
{
}

//***** Submit Register *****//
-(IBAction)regsterClick:(id)sender
{
    
//    [Utility showAlrtWithMessage:@"You have successfully purchased Tickets"];
//    
//    return;
    NSString *strcardno=[NSString stringWithFormat:@"%@",txtcardno.text];
    
    
    if([strcardno hasPrefix:@"X"])
    {
        strcardno=strorgnlccno;
    }
    else
    {
        strcardno = [strcardno stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    
    
    if ([strcardi isEqualToString:@""] || strcardi==nil)
    {
    }
    else
    {
        //NSLog(@"%@",strcardi);
    }
    NSString *strcvv=txtcvv.text;
//    strCardHolderName = txtCardHolderName.text;

    
    
    
    NSString *getMM=@"";
    if ([getMM isEqualToString:@""] || getMM==nil)
    {
        getMM=strmonth;
    }
    NSString *getYY=@"";
    if ([getYY isEqualToString:@""] || getYY==nil)
    {
        getYY=stryear;
    }
    NSInteger intgetMM=[getMM intValue];
    NSInteger intgetYY=[getYY intValue];
    //expiry validation
    NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    formatter.timeZone = destinationTimeZone;
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setDateFormat:@"yyyy"];
    NSString* dateString = [formatter stringFromDate:date];
    //NSLog(@"date--%@",dateString);
    
    NSDateFormatter* formatter1 = [[NSDateFormatter alloc] init];
    NSTimeZone *destinationTimeZone1 = [NSTimeZone systemTimeZone];
    formatter1.timeZone = destinationTimeZone1;
    [formatter1 setDateStyle:NSDateFormatterLongStyle];
    [formatter1 setDateFormat:@"MM"];
    NSString* dateString1 = [formatter1 stringFromDate:date];
    //NSLog(@"date--%@",dateString1);
    
    NSInteger putyear=[dateString intValue];
    //NSLog(@"%li",(long)putyear);
    
    NSInteger putmonth=[dateString1 intValue];
    //NSLog(@"%li",(long)putmonth);
    
    NSInteger newyear=intgetYY-putyear;
    //NSLog(@"newyear==%ld",(long)newyear);
    
    NSInteger newmonth=intgetMM-putmonth;
    //NSLog(@"newyear==%ld",(long)newmonth);
    
    if ([txtcvv.text isEqualToString:@""] || [txtcvv.text isEqualToString:nil])
    {
        UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"CVV Error" message:@"Please enter your CVV No." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alt1 show];
        [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
        return;
    }
    if ([txtcvv.text length]<4)
    {
        if ([txtcvv.text length]==3)
        {
            //NSLog(@"Continue...");
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CVV Error" message:@"Please enter 3 or 4 digits CVV No." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
            return;
        }
        
    }
    

    
    //*****Get Car Types API URL call*****//
    self.responseData = [NSMutableData data];
    
    if (![Stripe defaultPublishableKey]) {
        NSError *error = [NSError errorWithDomain:StripeDomain
                                             code:STPInvalidRequestError
                                         userInfo:@{
                                                    NSLocalizedDescriptionKey: @"Please specify a Stripe Publishable Key in Constants.m"
                                                    }];
        return;
    }
    stryear=btnSelectYY.titleLabel.text;
    strmonth=btnSelectMM.titleLabel.text;
    
        STPCardParams *cardParams = [[STPCardParams alloc] init];
    cardParams.name=txtCardHolderName.text;
    
     cardParams.number = strcardno;
    cardParams.expMonth =[strmonth integerValue];
    NSString *year=[stryear substringFromIndex: [stryear length] - 2];
     cardParams.expYear =[year integerValue];
     cardParams.cvc = txtcvv.text;
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    [[STPAPIClient sharedClient]setPublishableKey:@"pk_test_GAGUG3sxvYMq6lovOuMP3O38"];
    [[STPAPIClient sharedClient] createTokenWithCard:cardParams
                                          completion:^(STPToken *token, NSError *error) {
                                              
                                              
                                              if (error) {
                                                  STOPLOADING()

                                                  [self presentError:error];
                                              }
                                              [self createBackendChargeWithToken:token
                                                                                     completion:^(STPBackendChargeResult result, NSError *error) {
                                                                                         STOPLOADING()

                                                                                         if (error) {
                                                                                             
                                                                                             [self presentError :error];
                                                                                             return;
                                                                                         }
                                                                                         [self paymentSucceeded];
                                                                                     }];
                                          }];

    
}

- (void)presentError:(NSError *)error {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [controller addAction:action];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)paymentSucceeded {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Success" message:@"Payment successfully created!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [controller addAction:action];
    [self presentViewController:controller animated:YES completion:nil];
}


#pragma mark - STPBackendCharging

- (void)createBackendChargeWithToken:(STPToken *)token completion:(STPTokenSubmissionHandler)completion {
    
    if (!BackendChargeURLString) {
        
       
        if(token.tokenId){
         if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
            
      
            NSString *postStr=[NSString stringWithFormat:@"lgn_id=%@&od_id=%@&token=%@",[AppDelegate initAppdelegate].objUser.user_id,self.orderId,token.tokenId];
            
            NSData *postData = [postStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
            [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,MAKE_PAYMENTPROCESS]]];
            //NSLog(@"%@",[NSString stringWithFormat:@"%@%@",MAIN_URL,MAKE_PAYMENTPROCESS]);
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//            [request setValue:@"application/form-data" forHTTPHeaderField:@"Content-Type"];
            [request setTimeoutInterval:20.0];
            [request setHTTPBody:postData];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue currentQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                       NSError *error1;
                                       if(data==nil){
                                       }else{
                                           STOPLOADING()
                                           //NSLog(@"Responce : %@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                           NSMutableDictionary * innerJson = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                           
                                           NSMutableDictionary *dict = [innerJson mutableCopy];
                                           NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                                           [dict removeObjectsForKeys:keysForNullValues];
                                           
                                           if([[dict valueForKey:@"STATUS"]isEqualToString:@"Success"]){
                                               
                                               PaymentPageVC *objP=[[PaymentPageVC alloc]initWithNibName:@"PaymentPageVC" bundle:nil];
                                               objP.ticketId=[[dict valueForKey:@"DATA"]valueForKey:@"utkt_id"];
                                               
                                               [self.navigationController pushViewController:objP animated:true];
                                               [self showNotificationView:[dict valueForKey:@"MESSAGES"] :[UIImage imageNamed:@"info.png"]];
//                                               [Utility showAlrtWithMessage:[dict valueForKey:@"MESSAGES"]];
                                               
                                           }else{
//                                               [Utility showAlrtWithMessage:[dict valueForKey:@"MESSAGES"]];
                                                [self showNotificationView:[dict valueForKey:@"MESSAGES"] :[UIImage imageNamed:@"info.png"]];
                                           }

                                           
                                       }
                                   }];
            

            
             }
        
    }
    }


- (IBAction)selectClicked:(id)sender {
        NSArray * arr = [[NSArray alloc] init];
        arr = [NSArray arrayWithObjects:@"Visa Card", @"Master Card", @"American Express Card", @"Discover Card", @"Diners Club Card", @"JCB Card",nil];
//    arrImage = [NSArray arrayWithObjects:[UIImage imageNamed:@"Visa.png"], [UIImage imageNamed:@"Master.png"], [UIImage imageNamed:@"American.png"], [UIImage imageNamed:@"discover.jpeg"], [UIImage imageNamed:@"diners.gif"], [UIImage imageNamed:@"jcb.jpeg"], nil];
    if(dropDown == nil) {
        CGFloat f = 118;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

//***** MM *****//
- (IBAction)selectClickedMM:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"01", @"02", @"03", @"04", @"05", @"06",@"07", @"08", @"09", @"10", @"11", @"12",nil];
       if(dropDownMM == nil) {
        CGFloat f = 100;
        dropDownMM = [[NIDropDownMM alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDownMM.delegate = self;
    }
    else {
        [dropDownMM hideDropDown:sender];
        [self relMM];
    }
}
- (void) niDropDownDelegateMethodMM: (NIDropDownMM *) sender {
    [self relMM];
}

-(void)relMM
{
    //    [dropDown release];
    dropDownMM = nil;
}
//***** YY *****//
- (IBAction)selectClickedYY:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"2015", @"2016", @"2017", @"2018", @"2019", @"2020",@"2021", @"2022", @"2023", @"2024",nil];
      if(dropDownYY == nil) {
        CGFloat f = 100;
        dropDownYY = [[NIDropDownYY alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDownYY.delegate = self;
    }
    else {
        [dropDownYY hideDropDown:sender];
        [self relYY];
    }
}
- (void) niDropDownDelegateMethodYY: (NIDropDownYY *) sender {
    [self relYY];
}
-(void)relYY
{
    //    [dropDown release];
    dropDownYY = nil;
}
-(int)getLength:(NSString*)number
{
    int length = [number length];
    return length;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==txtcvv)
        
    {
        if(txtcvv.text)
        {
            
            int length = [self getLength:txtcvv.text];
            if(length == 4)
            {
                if(range.length == 0)
                    return NO;
            }
            
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
            
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            
            return [string isEqualToString:filtered];
        }
    }
    else if (textField==txtcardno)
    {
        
        int length = [self getLength:txtcardno.text];
        
        if(length == 20)
            
        {
            
            if(range.length == 0)
                
                return NO;
            
        }
        
      
        //        static int currentLength = 0;
        //        if ((currentLength += [string length]) == 4) {
        //            currentLength = 0;
        //            [txtcardno setText:[NSString stringWithFormat:@"%@%@%c", [textField text], string, ' ']];
        //            return NO;
        //        }
        
        if (string.length > 0) {
            NSUInteger length = textField.text.length;
            int cntr = (int)((length - (length/kLENGTH)) / kLENGTH);
            if (!(((length + 1) % kLENGTH) - cntr)) {
                NSString *str = [textField.text stringByAppendingString:[NSString stringWithFormat:@"%@ ", string]];
                textField.text = str;
                return NO;
            }
        } else {
            if ([textField.text hasSuffix:@" "]) {
                textField.text = [textField.text substringToIndex:textField.text.length - 2];
                return NO;
            }
        }
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
        
        return YES;
        
        
        
    }
    return YES;
}
/*- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //--------> CC Clear Backspace event <-----
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    BOOL isPressedBackspaceAfterSingleSpaceSymbol = range.location == 19 && range.length == 1;
    
    if (isPressedBackspaceAfterSingleSpaceSymbol)
        
    {
        
        //  your actions for deleteBackward actions
        
        txtcardno.text=nil;
        
        
        
    }
    
    else if (range.location==18 && range.length==1){
        
        txtcardno.text=nil;
        
    }
    
    //-------> CVV Clear <----
    if (textField==txtcvv)
        
    {
        if(txtcvv.text)
        {
            
            int length = [self getLength:txtcvv.text];
            if(length == 4)
            {
                if(range.length == 0)
                    return NO;
            }
            
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
            
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            
            return [string isEqualToString:filtered];
        }
    }
    
    //--------> Space Between 4 letter event <------
    
    if (textField==txtcardno)
        
    {
        
        if (string.length > 0)
        {
            
            NSUInteger length = textField.text.length;
            
            int cntr = (int)((length - (length/kLENGTH)) / kLENGTH);
            
            if (!(((length + 1) % kLENGTH) - cntr)) {
                
                NSString *str = [textField.text stringByAppendingString:[NSString stringWithFormat:@"%@ ", string]];
                
                textField.text = str;
                
                return NO;
                
            }
            
        } else {
            
            if ([textField.text hasSuffix:@" "]) {
                
                textField.text = [textField.text substringToIndex:textField.text.length - 2];
                
                return NO;
                
            }
            
        }
        
        //------> Creditcard Max. Length <-----
        
        if(txtcardno.text)
            
        {
            
            int length = [self getLength:txtcardno.text];
            
            if(length == 20)
                
            {
                
                if(range.length == 0)
                    
                    return NO;
                
            }
            
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:Number] invertedSet];
            
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            
            return [string isEqualToString:filtered];
            
        }
    
    }
    
//    
//    else if (textField==txtcardno)
//    {
////        static int currentLength = 0;
////        if ((currentLength += [string length]) == 4) {
////            currentLength = 0;
////            [txtcardno setText:[NSString stringWithFormat:@"%@%@%c", [textField text], string, ' ']];
////            return NO;
////        }
//       
//        if (string.length > 0) {
//            NSUInteger length = textField.text.length;
//            int cntr = (int)((length - (length/kLENGTH)) / kLENGTH);
//            if (!(((length + 1) % kLENGTH) - cntr)) {
//                NSString *str = [textField.text stringByAppendingString:[NSString stringWithFormat:@"%@ ", string]];
//                textField.text = str;
//                return NO;
//            }
//        } else {
//            if ([textField.text hasSuffix:@" "]) {
//                textField.text = [textField.text substringToIndex:textField.text.length - 2];
//                return NO;
//            }
//        }
//        
//        
//        return YES;
//        
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
//        if([string length]==0)
//        {
//            [formatter setGroupingSeparator:@"-"];
//            [formatter setGroupingSize:4];
//            [formatter setUsesGroupingSeparator:YES];
//            [formatter setSecondaryGroupingSize:2];
//            NSString *num = txtcardno.text ;
//            num= [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
//            NSString *str = [formatter stringFromNumber:[NSNumber numberWithDouble:[num doubleValue]]];
//            txtcardno.text=str;
//            //NSLog(@"%@",str);
//            return YES;
//        }
//        else {
//            [formatter setGroupingSeparator:@"-"];
//            [formatter setGroupingSize:2];
//            [formatter setUsesGroupingSeparator:YES];
//            [formatter setSecondaryGroupingSize:2];
//            NSString *num = txtcardno.text ;
//            if(![num isEqualToString:@""])
//            {
//                num= [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
//                NSString *str = [formatter stringFromNumber:[NSNumber numberWithDouble:[num doubleValue]]];
//                txtcardno.text=str;
//            }
//            
//            ////NSLog(@"%@",str);
//            return YES;
      //  }

        
    //}
    return YES;
}*/
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if (textField==txtcvv)
    {
        if ([txtcvv.text length]<4)
        {
            if ([txtcvv.text length]==3)
            {
                //NSLog(@"Continue...");
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter 3 or 4 digits CVV No." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                return YES;
            }
            
        }
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    if(textField==txtcvv){
        [scrollView setContentOffset:CGPointMake(0, 150) animated:true];
    }
    return true;
}


//***** Left Button Click *****//
-(IBAction)leftbtn:(id)sender
{
}

-(IBAction)BtnBack:(id)sender
{
        [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backToView:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)showCardSelection:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Visa Card", @"Master Card", @"American Express Card", @"Discover Card", @"Diners Club Card", @"JCB Card",nil];
    if(dropDown == nil) {
        CGFloat f = 100;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }

}

-(void)showNotificationView:(NSString *)msg :(UIImage *)img
{
    notiImg.image=img;
    lblMsg.text=msg;
       [viewNoti setBackgroundColor:[UIColor colorWithRed:125.0/255.0 green:33.0/255.0 blue:210.0/255.0 alpha:0.8]];
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y += frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:3.0];
                     }];
    
}
-(void)hideNotiView
{
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y -= frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         //                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:0.5];
                     }];
    
}



#pragma mark- Share Methods
- (IBAction)showShareButton:(id)sender {
    if(isShown){
        [shareButton setImage:[UIImage imageNamed:@"shareIconInactive.png"] forState:UIControlStateNormal];
        //        [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
        
        
        // Set delegate and selector to remove from superview when animation completes
        // Move this view to bottom of superview
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        CGRect frame = shareView.frame;
        frame.origin = CGPointMake(0.0,-self.view.bounds.size.height);
        shareView.frame = frame;
        [UIView commitAnimations];
        isShown=false;
        
    }else{
        [shareButton setImage:[UIImage imageNamed:@"shareIconActive.png"] forState:UIControlStateNormal];
        [shareView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
        [view_mainShare setBackgroundColor:[UIColor colorWithRed:15.0/255 green:176.0/255 blue:212.0/255 alpha:0.30]];
        CGRect frame = shareView.frame;
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        
        
        self.view.alpha=1.0f;
        frame.origin = CGPointMake(0.0, 0);
        shareView.frame = frame;
        //        [[shareView layer] addAnimation:animation forKey:@"Slidein"];
        [UIView commitAnimations];
        
        isShown=true;
        
    }
    
}
- (IBAction)showAppLink:(id)sender;
{
    
}

- (IBAction)shareOnTwitter:(id)sender {
    [self showShareButton:nil];
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//    {
    
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}

- (IBAction)shareOnFacebook:(id)sender {
    [self showShareButton:nil];
    [AppDelegate initAppdelegate].playerView.hidden=true;
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeFacebook];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate initAppdelegate].playerView.hidden=false;
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}


#pragma mark- dropdown Methods

#pragma mark- ANIMATION METHODS
-(IBAction)showMenu:(id)sender{
    
    //    [self onShowMenu];
    if([AppDelegate initAppdelegate].arrPlayList.count==0){
        return;
    }
    [self onShowMenu];
    
}
- (void)onShowMenu
{
    if(menuButton.selected){
        menuButton.selected=false;
        
        [self onHideMenu];
    }else{
        //    if (!self.table) {
        UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect frame = mainWindow.frame;
        //        frame.origin.y += self.view.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
        
        frame.origin.y=80;
        frame.size.height-=150;
        NSMutableArray *iem=[[NSMutableArray alloc]init];
        
        
        if([AppDelegate initAppdelegate].arrPlayList.count >0){
            
            for(int i=0;i<[AppDelegate initAppdelegate].arrPlayList.count;i++){
                PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:i];
                [iem addObject:objs.playName];
            }
        }
        
        self.table = [[SIMenuTable alloc] initWithFrame:frame items:iem];
        self.table.menuDelegate = self;
        self.table.arrData=[AppDelegate initAppdelegate].arrPlayList;
        self.menuContainer=self.view;
        [self.menuContainer addSubview:self.table];
        [self rotateArrow:M_PI];
        [self.table show];
        menuButton.selected=true;
    }
}

- (void)onHideMenu
{
    [self rotateArrow:0];
    [self.table hide];
}

- (void)rotateArrow:(float)degrees
{
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        //        self.menuButton.arrow.layer.transform = CATransform3DMakeRotation(degrees, 0, 0, 1);
    } completion:NULL];
}

#pragma mark -
#pragma mark Delegate methods
- (void)didSelectPlayAtIndex:(NSUInteger)index
{
    [self.navigationController popToRootViewControllerAnimated:true];
    
    //    [self backButtonPressed:nil];
    [self.playListDelegate didSelectPlayAtIndex:index];
    //    [Utility showAlrtWithMessage:@"Play select"];
    return;
    
    
}
- (void)didSelectPlayItemListItemAtIndex:(NSUInteger)index;
{
    [self.navigationController popToRootViewControllerAnimated:true];
    [self.playListDelegate didSelectPlayItemListItemAtIndex:index];
    return;
}
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    
    [self.navigationController popToRootViewControllerAnimated:true];
    [self.playListDelegate didSelectItemAtIndex:index];
    return;
    
    
}




@end
