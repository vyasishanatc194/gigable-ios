//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import <MessageUI/MessageUI.h>

#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "DashboardVC.h"
#import "Static.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "ProfileVC.h"
#import "ChangePasswordVC.h"
#import "PlayListVC.h"
#import "FavShowsVC.h"
#import "ConcertPlayListViewController.h"
@interface SideMenuViewController ()<MFMailComposeViewControllerDelegate>
{
    NSString *showCount;
}
@end
@implementation SideMenuViewController

@synthesize demoController;
#pragma mark -
#pragma mark - UITableViewDataSource
-(void)viewWillAppear:(BOOL)animated
{
    [self getMyFavouritesShows];
    showCount=@"";
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getMyFavouritesShows)
                                                 name:@"FavNotification"
                                               object:nil];

}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    cell.backgroundColor=[UIColor blackColor];
    cell.contentView.backgroundColor=[UIColor blackColor];
    tableView.backgroundColor=[UIColor blackColor];
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if([[NSUserDefaults standardUserDefaults]boolForKey:KEY_Social]==true){
        if(indexPath.row==3){
            return 0;
        }
    }
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    if(indexPath.row==0){
        cell.textLabel.text =@"Home";
 cell.imageView.image=[UIImage imageNamed:@"music_side.png"];
    }
    
    if(indexPath.row==1){
        cell.textLabel.text =@"Favorites";
         cell.imageView.image=[UIImage imageNamed:@"fav_side.png"];
    }
    if(indexPath.row==3){
                cell.textLabel.text =@"Profile";
        cell.imageView.image=[UIImage imageNamed:@"profile.png"];

    }
    
   /* if(indexPath.row==5){
                cell.textLabel.text =@"Feedback";
        cell.imageView.image=[UIImage imageNamed:@"setting.png"];

    }*/
   if(indexPath.row==2){
        cell.textLabel.text =@"My Shows";
        cell.imageView.image=[UIImage imageNamed:@"fav_side.png"];
        
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(130, 15, 100, 30)];
        lbl.text=[NSString stringWithFormat:@"%@ Upcoming",showCount];
        [lbl.layer setCornerRadius:15.0];
        lbl.layer.masksToBounds=true;
        [lbl setFont:[UIFont fontWithName:FONTNAME size:14.0]];
        [lbl setTextAlignment:NSTextAlignmentCenter];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setBackgroundColor:NAVNBAR_COLOR];
        [cell.contentView addSubview:lbl];
        
    }

     if(indexPath.row==4){
        cell.textLabel.text =@"Change Password";
        cell.imageView.image=[UIImage imageNamed:@"pwd_side.png"];
        
    }
    
    if(indexPath.row==5){
        cell.imageView.image=[UIImage imageNamed:@"logout.png"];
        cell.textLabel.text =@"Logout";
    }

    cell.textLabel.font=[UIFont fontWithName:FONTNAME size:13.0];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.textLabel.textColor=[UIColor whiteColor];
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if(indexPath.row==5){
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FirstLaunch"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[AppDelegate initAppdelegate]showLoginMenu];
    }
    
    if (indexPath.row==3) {

        
        [[AppDelegate initAppdelegate]pausePlayer];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:[[ProfileVC alloc]initWithNibName:@"ProfileVC" bundle:nil]];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

        
    }
    
    if (indexPath.row==4) {
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:[[ChangePasswordVC alloc]initWithNibName:@"ChangePasswordVC" bundle:nil]];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        
    }
    if (indexPath.row==2) {
        
        
        DashboardVC   *objp = [[DashboardVC alloc] initWithNibName:@"DashboardVC" bundle:nil];
        objp.isFavShowSideMenu=true;

        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:objp];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        
    }
    
    if (indexPath.row==1) {
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
      //  PlayListVC *objp=[[PlayListVC alloc]initWithNibName:@"PlayListVC" bundle:nil];
       // objp.isFav=true;

        ConcertPlayListViewController   *objp = [[ConcertPlayListViewController alloc] initWithNibName:@"ConcertPlayList" bundle:nil];
        objp.isFavSideMenu1=true;
        NSArray *controllers = [NSArray arrayWithObject:objp];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//     DashboardVC   *objp = [[DashboardVC alloc] initWithNibName:@"DashboardVC" bundle:nil];
//        objp.isFavSideMenu=true;
//        NSArray *controllers = [NSArray arrayWithObject:objp];
//        navigationController.viewControllers = controllers;
//        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        
    }
    
    
    if(indexPath.row==0){
    if(!demoController){
    demoController = [[DashboardVC alloc] initWithNibName:@"DashboardVC" bundle:nil];
        
    demoController.title = [NSString stringWithFormat:@"Demo #%ld-%ld", (long)indexPath.section, (long)indexPath.row];
    }else{
        [AppDelegate initAppdelegate].playerView.hidden=false;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SHOWMENU" object:self];

    
    }
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:demoController];
    navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

    }
   /* if (indexPath.row==5) {
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
            [comp setMailComposeDelegate:self];
            if([MFMailComposeViewController canSendMail])
            {
                
                [AppDelegate initAppdelegate].playerView.hidden=true;
                [comp setMailComposeDelegate:self];
                [comp setToRecipients:@[@"feedback@gigable.io"]];
                [comp setSubject:@"Feedback To Gigable"];
                [comp setMessageBody:[NSString stringWithFormat:@"From :%@",[AppDelegate initAppdelegate].objUser.lgn_email] isHTML:NO];
                [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [self presentViewController:comp animated:YES completion:nil];
            }
            else{
                UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"" otherButtonTitles:nil, nil];
                [alrt show];
                
            }
        }
        
    }*/
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [AppDelegate initAppdelegate].playerView.hidden=false;

    if(error)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Email Sending Failure" delegate:nil cancelButtonTitle:@"" otherButtonTitles:nil, nil];
        [alrt show];
        [self dismissViewControllerAnimated:true completion:nil];
    }
    else{
        [self dismissViewControllerAnimated:true completion:nil];
    }
    
}

-(void)getMyFavouritesShows
{
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

//    http://52.34.59.210/app/services/show/listfavouriteshow__1
    NSString *postStr=[NSString stringWithFormat:@"msf_lgn_id=%@",[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:GET_MY_FAV_SHOW withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableDictionary *dictER=[dictresponce valueForKey:@"DATA"];
            showCount=[NSString stringWithFormat:@"%@",[dictER valueForKey:@"upcoming_count"]];
            NSMutableArray *arrCurrent=[dictER valueForKey:@"currentArray"];
            NSMutableArray *arrPast=[dictER valueForKey:@"pastArray"];
             NSMutableArray *arrTic=[dictER valueForKey:@"ticketArray"];
            arrShow=[[NSMutableArray alloc]init];
            [arrShow addObjectsFromArray:arrPast];
            [arrShow addObjectsFromArray:arrTic];

                [arrShow addObjectsFromArray:arrCurrent];
                [self.tableView reloadData];
            
            
            
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
}

@end
