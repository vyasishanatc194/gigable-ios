//
//  ConcertPlalyListCell.h
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConcertPlalyListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgArtist;
@property (strong, nonatomic) IBOutlet UILabel *lblConcertName;
@property (strong, nonatomic) IBOutlet UILabel *lblArtistName;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblPlace;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UIButton *btninfo;

@end
