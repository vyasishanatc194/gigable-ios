//
//  PlayListCell.h
//  Gigable
//
//  Created by Kalpit Gajera on 06/05/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title_Play;
@property (strong, nonatomic) IBOutlet UILabel *trackCount;
@property (strong, nonatomic) IBOutlet UILabel *artistName;
@property (strong, nonatomic) IBOutlet UIButton *btnPlay;
@property (strong, nonatomic) IBOutlet UILabel *playInfo;
@property (strong, nonatomic) IBOutlet UIButton *btnTrackList;
@property (strong, nonatomic) IBOutlet UILabel *sep;
@property (strong, nonatomic) IBOutlet UIButton *btnTrackCount;
- (void)setSelected:(BOOL)selected withCompletionBlock:(void (^)())completion;

@end
