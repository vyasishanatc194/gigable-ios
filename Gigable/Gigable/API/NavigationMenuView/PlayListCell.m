//
//  PlayListCell.m
//  Gigable
//
//  Created by Kalpit Gajera on 06/05/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "PlayListCell.h"
#import "SIMenuConfiguration.h"
#import "UIColor+Extension.h"
#import "SICellSelection.h"
#import <QuartzCore/QuartzCore.h>

@interface PlayListCell ()

@property (nonatomic, strong) SICellSelection *cellSelection;
@end
@implementation PlayListCell
@synthesize title_Play,trackCount,artistName,btnPlay,playInfo,btnTrackList,btnTrackCount;
@synthesize sep;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor color:[SIMenuConfiguration itemsColor] withAlpha:[SIMenuConfiguration menuAlpha]];
        self.textLabel.textColor = [SIMenuConfiguration itemTextColor];
        self.textLabel.textAlignment = NSTextAlignmentLeft;
        //        self.textLabel.shadowColor = [UIColor darkGrayColor];
        //        self.textLabel.shadowOffset = CGSizeMake(0.0, -1.0);
        self.textLabel.font = [UIFont systemFontOfSize:12.0];
        self.selectionStyle = UITableViewCellEditingStyleNone;
        
        self.cellSelection = [[SICellSelection alloc] initWithFrame:self.bounds andColor:[SIMenuConfiguration selectionColor]];
        [self.cellSelection.layer setCornerRadius:6.0];
        [self.cellSelection.layer setMasksToBounds:YES];
        
        self.cellSelection.alpha = 0.0;
        [self.contentView insertSubview:self.cellSelection belowSubview:self.textLabel];
        
        [btnPlay.layer setCornerRadius:btnPlay.frame.size.width/2];
        btnPlay.layer.masksToBounds=true;
        [btnPlay.layer setBorderWidth:1.0];
        [btnPlay.layer setBorderColor:[UIColor whiteColor].CGColor];
//        [btnPlay setImageEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];

    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    return;
    [super setSelected:selected animated:animated];
}

- (void)setSelected:(BOOL)selected withCompletionBlock:(void (^)())completion
{
    float alpha = 0.0;
    if (selected) {
        alpha = 1.0;
    } else {
        alpha = 0.0;
    }
    [UIView animateWithDuration:[SIMenuConfiguration selectionSpeed] animations:^{
        self.cellSelection.alpha = alpha;
    } completion:^(BOOL finished) {
        completion();
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    [btnPlay.layer setCornerRadius:btnPlay.frame.size.width/2];
    btnPlay.layer.masksToBounds=true;
    [btnPlay.layer setBorderWidth:1.0];
    [btnPlay.layer setBorderColor:[UIColor whiteColor].CGColor];
    // Initialization code
}

@end
