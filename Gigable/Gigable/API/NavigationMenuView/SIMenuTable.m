//
//  SAMenuTable.m
//  NavigationMenu
//
//  Created by Ivan Sapozhnik on 2/19/13.
//  Copyright (c) 2013 Ivan Sapozhnik. All rights reserved.
//

#import "SIMenuTable.h"
#import "SIMenuCell.h"
#import "SIMenuConfiguration.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Extension.h"
#import "SICellSelection.h"
#import "PlayListCell.h"
#import "Static.h"
#import "User.h"
@interface SIMenuTable () {
    CGRect endFrame;
    CGRect startFrame;
    NSIndexPath *currentIndexPath;
}
@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) NSArray *items;
@end

@implementation SIMenuTable
@synthesize arrData;
- (id)initWithFrame:(CGRect)frame items:(NSArray *)items
{
    self = [super initWithFrame:frame];
    if (self) {
       
        
        self.items = [NSArray arrayWithArray:items];
        
        self.layer.backgroundColor = [UIColor color:[SIMenuConfiguration mainColor] withAlpha:0.5].CGColor;
        self.clipsToBounds = YES;
        
        endFrame = self.bounds;
        startFrame = endFrame;
        startFrame.origin.y -= self.items.count*[SIMenuConfiguration itemCellHeight];
        
        self.table = [[UITableView alloc] initWithFrame:startFrame style:UITableViewStylePlain];
        self.table.delegate = self;
        self.table.dataSource = self;
        self.table.backgroundColor = [UIColor clearColor];
        self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.table.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.table.separatorColor=[UIColor clearColor];
        UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.table.bounds.size.height, [SIMenuConfiguration menuWidth], self.table.bounds.size.height)];
        header.backgroundColor = [UIColor color:[SIMenuConfiguration itemsColor] withAlpha:[SIMenuConfiguration menuAlpha]];
        header.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.table addSubview:header];

    }
    return self;
}

- (void)show
{
    self.currentSelection = -1;

    [self addSubview:self.table];
    if (!self.table.tableFooterView) {
        [self addFooter];
    }
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] animations:^{
        self.layer.backgroundColor = [UIColor color:[SIMenuConfiguration mainColor] withAlpha:[SIMenuConfiguration backgroundAlpha]].CGColor;
        self.table.frame = endFrame;
        self.table.contentOffset = CGPointMake(0, [SIMenuConfiguration bounceOffset]);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:[self bounceAnimationDuration] animations:^{
            self.table.contentOffset = CGPointMake(0, 0);
        }];
    }];
}

- (void)hide
{
    [UIView animateWithDuration:[self bounceAnimationDuration] animations:^{
        self.table.contentOffset = CGPointMake(0, [SIMenuConfiguration bounceOffset]);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:[SIMenuConfiguration animationDuration] animations:^{
            self.layer.backgroundColor = [UIColor color:[SIMenuConfiguration mainColor] withAlpha:0.0].CGColor;
            self.table.frame = startFrame;
        } completion:^(BOOL finished) {
//            [self.table deselectRowAtIndexPath:currentIndexPath animated:NO];
            SIMenuCell *cell = (SIMenuCell *)[self.table cellForRowAtIndexPath:currentIndexPath];
            [cell setSelected:NO withCompletionBlock:^{

            }];
            currentIndexPath = nil;
            [self removeFooter];
            [self.table removeFromSuperview];
            [self removeFromSuperview];
        }];
    }];
}

- (float)bounceAnimationDuration
{
    float percentage = 28.57;
    return [SIMenuConfiguration animationDuration]*percentage/100.0;
}

- (void)addFooter
{
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [SIMenuConfiguration menuWidth], self.table.bounds.size.height - (self.items.count * [SIMenuConfiguration itemCellHeight]))];
    self.table.tableFooterView = footer;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBackgroundTap:)];
    [footer addGestureRecognizer:tap];
}

- (void)removeFooter
{
    self.table.tableFooterView = nil;
}

- (void)onBackgroundTap:(id)sender
{
    [self.menuDelegate didBackgroundTap];
}

- (void)dealloc
{
    self.items = nil;
    self.table = nil;
    self.menuDelegate = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [cell.contentView
     setBackgroundColor:[UIColor colorWithRed:15.0/255 green:176.0/255 blue:212.0/255 alpha:0.3]];
    cell.backgroundColor=[UIColor colorWithRed:15.0/255 green:176.0/255 blue:212.0/255 alpha:0.3];
    cell.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];

    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    int rowHeight;
    if ([indexPath row] == self.currentSelection) {
        rowHeight = 135;
    } else rowHeight = [SIMenuConfiguration itemCellHeight];
    return rowHeight;
    
        
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    PlayListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayListCell"];
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PlayListCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
   

    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    PlayList *obj=[self.arrData objectAtIndex:indexPath.row];
    cell.playInfo.text=obj.play_desc;
//    cell.trackCount.text=[NSString stringWithFormat:@"%@ Tracks",obj.totaltrack];
//  
//    cell.artistName.text=obj.artist;
//    cell.title_Play.text = [self.items objectAtIndex:indexPath.row];
    cell.btnPlay.tag=indexPath.row;
    [cell.btnPlay addTarget:self action:@selector(playAllTracks:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnTrackList.tag=indexPath.row;
    [cell.btnTrackList addTarget:self action:@selector(playTracks:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnTrackCount.tag=indexPath.row;
    [cell.btnTrackCount addTarget:self action:@selector(playTracksCount:) forControlEvents:UIControlEventTouchUpInside];

    
    if(self.currentSelection == indexPath.row){
        cell.playInfo.hidden=false;
        cell.btnTrackList.hidden=false;
        cell.artistName.hidden=false;
        cell.title_Play.font =[UIFont fontWithName:FONTNAMEBOLD size:13.0];

    }else{
        cell.playInfo.hidden=true;
        cell.btnTrackList.hidden=true;
        cell.artistName.hidden=true;
    }
    
    CGRect lblFr=cell.sep.frame;
    lblFr.origin.y=48;
    
    
    cell.sep.frame=lblFr;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlayList *obj=[self.arrData objectAtIndex:indexPath.row];
    if([obj.totaltrack integerValue]==0){
        return;
    }

    self.currentSelection = indexPath.row;
    // save height for full text label
    [tableView beginUpdates];
    
    PlayListCell *cell = (PlayListCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.title_Play.font =[UIFont fontWithName:FONTNAMEBOLD size:13.0];

    cell.playInfo.hidden=false;
    cell.btnTrackList.hidden=false;
    cell.artistName.hidden=false;
    CGRect lblFr=cell.sep.frame;
    lblFr.origin.y=134;
    lblFr.size.height=0.5;
    cell.sep.frame=lblFr;
    [tableView endUpdates];

    [cell setSelected:YES withCompletionBlock:^{
    }];


//    return;
//    SIMenuCell *cell = (SIMenuCell *)[tableView cellForRowAtIndexPath:indexPath];
//    [cell setSelected:YES withCompletionBlock:^{
//        [self.menuDelegate didSelectItemAtIndex:indexPath.row];
//    }];
    
}



-(void)playTracksCount:(UIButton *)btn
{
    [self.menuDelegate didSelectPlayItemListItemAtIndex:btn.tag];
    
}

-(void)playTracks:(UIButton *)btn
{
    [self.menuDelegate didSelectItemAtIndex:btn.tag];

}
-(void)playAllTracks:(UIButton *)btn{
    [self.menuDelegate didSelectPlayAtIndex:btn.tag];

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView reloadData];
//    PlayList *obj=[self.arrData objectAtIndex:indexPath.row];
//    if([obj.totaltrack integerValue]==0){
//        return;
//    }
//
    
//    PlayListCell *cell = (PlayListCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.title_Play.font =[UIFont fontWithName:FONTNAME size:13.0];
//    
//    cell.playInfo.hidden=true;
//    cell.btnTrackList.hidden=true;
//    cell.artistName.hidden=true;
//
//    [cell setSelected:NO withCompletionBlock:^{
//
//    }];
}

@end
