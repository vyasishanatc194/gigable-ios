//
//  SAMenuTable.h
//  NavigationMenu
//
//  Created by Ivan Sapozhnik on 2/19/13.
//  Copyright (c) 2013 Ivan Sapozhnik. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SIMenuDelegate <NSObject>
- (void)didBackgroundTap;
- (void)didSelectItemAtIndex:(NSUInteger)index;
- (void)didSelectPlayAtIndex:(NSUInteger)index;
- (void)didSelectPlayItemListItemAtIndex:(NSUInteger)index;

@end

@interface SIMenuTable : UIView <UITableViewDataSource, UITableViewDelegate>
{
}
@property (strong,nonatomic)NSMutableArray *arrData;
@property (nonatomic, weak) id <SIMenuDelegate> menuDelegate;
@property (nonatomic) int currentSelection;

- (id)initWithFrame:(CGRect)frame items:(NSArray *)items;
- (void)show;
- (void)hide;

@end
