//
//  SignUpVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileVC : UIViewController<UITextFieldDelegate,UIActionSheetDelegate>
{
    
     IBOutlet UIScrollView *scrollview;
     IBOutlet UIView *topView;
     IBOutlet UITextField *userName,*phone,*email;
     IBOutlet UITextField *location;
    UIAlertView *alertLocations,*alertGender;
    NSMutableArray *arrLocation;
    
    IBOutlet UIView *viewNoti;
    IBOutlet UIImageView *notiImg;
    IBOutlet UILabel *lblMsg;
}
- (IBAction)btnSignupClicked:(id)sender;
@end
