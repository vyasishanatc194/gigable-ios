//
//  SignUpVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 24/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "ProfileVC.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "Static.h"
#import "User.h"
#import "MFSideMenu.h" 
@interface ProfileVC ()
{
    AppDelegate *delegate;
}
@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate=[AppDelegate initAppdelegate];
    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
    
    [email setValue:NAVNBAR_COLOR forKeyPath:@"_placeholderLabel.textColor"];
    [userName setValue:NAVNBAR_COLOR forKeyPath:@"_placeholderLabel.textColor"];
    [phone setValue:NAVNBAR_COLOR forKeyPath:@"_placeholderLabel.textColor"];
    [location setValue:NAVNBAR_COLOR forKeyPath:@"_placeholderLabel.textColor"];
    
    email.text=delegate.objUser.lgn_email;
        userName.text=delegate.objUser.userName;
    
    if([delegate.objUser.gender isEqualToString:@"Male"]){
    phone.text=@"Male";
        phone.tag=1;
    }
    
    if([delegate.objUser.gender isEqualToString:@"Female"]){
        phone.text=@"Female";
                phone.tag=2;
    }
    
    if([delegate.objUser.gender isEqualToString:@"Other"]){
        phone.text=@"Other";
                phone.tag=3;
    }
    if(delegate.objUser.objLocation){
        location.text=delegate.objUser.objLocation.loc_name;
    
    }
    
    if([AppDelegate initAppdelegate].arrLoaction.count >0){
        arrLocation=[AppDelegate initAppdelegate].arrLoaction;
    }
    
    for(int i=0;i<arrLocation.count;i++){
        Location *objl=[arrLocation objectAtIndex:i];
        if([objl.loc_id  isEqualToString:[AppDelegate initAppdelegate].objUser.locId]){
            location.tag=i+1;
        }
    }
    
    
    delegate.playerView.hidden=true;
    //    [self getProfile];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)showSideMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //        [self setupMenuBarButtonItems];
    }];
    
}
- (IBAction)btnSignupClicked:(id)sender {
    if([userName.text isEqualToString:@""]){
        [self showNotificationView:@"Please enter user name" :[UIImage imageNamed:@"info.png"]];

//        [Utility showAlrtWithMessage:@"Please enter user name"];
                return;
    }

    
     if([phone.text isEqualToString:@""]){
         [self showNotificationView:@"Please select Gender" :[UIImage imageNamed:@"info.png"]];

//        [Utility showAlrtWithMessage:@"Please select Gender"];
        return;
    }
//    if([location.text isEqualToString:@""]){
//        
//        [Utility showAlrtWithMessage:@"Please select Location"];
//        return;
//    }
    

   
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    
    /*
     lgn_id = 16
     usr_loc_id = 1
     usr_name = Joy
     usr_phone = 9988779920
     Usr_gender = 1
 */

    Location *objL=[arrLocation objectAtIndex:(location.tag-1)];
    NSString *post = [NSString stringWithFormat: @"lgn_id=%@&usr_name=%@&usr_loc_id=%@&usr_gender=%d&usr_phone=1",[AppDelegate initAppdelegate].objUser.user_id, userName.text,objL.loc_id,phone.tag ];
    
    [Utility executePOSTRequestwithServicetype:GET_PROFILE withPostString: post withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [[NSUserDefaults standardUserDefaults]setObject:[dictresponce valueForKey:@"DATA"] forKey:KEY_LOGINDICT];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictresponce valueForKey:@"DATA"]];
            delegate.isRef=true;
//            [Utility showAlrtWithMessage:@"Profile updated"];
            [self showNotificationView:@"Profile updated" :[UIImage imageNamed:@"info.png"]];

        }else{
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];

//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];


}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
   
    if(textField==location){
        [scrollview setContentOffset:CGPointMake(0, 0) animated:true];
        [self getLocationFromWebservice];
        return false;
    }else if(textField==phone){
        [scrollview setContentOffset:CGPointMake(0, 0) animated:true];
        [self showGender];
        return false;
    }
    return true;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}
-(void)getLocationFromWebservice
{
    return;

    if(arrLocation.count>0)
    {
        [self showLocationActionsheet];
        return;
    }
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
[Utility executeRequestwithServicetype:GET_LOCATION withDictionary:[[NSMutableDictionary alloc]init] withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
    STOPLOADING()
    //NSLog(@"%@",dictresponce);
    if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
        if([[dictresponce valueForKey:@"COUNT"] intValue]>0){
    arrLocation=[[NSMutableArray alloc]init];
            NSMutableArray *arrTemp=[dictresponce valueForKey:@"DATA"];
            for(NSMutableDictionary *dict in arrTemp){
                Location *objl=[Location initWithResponceDict:dict];
                [arrLocation addObject:objl];
            }
            [self showLocationActionsheet];
        }
    }

    
    

}];
}
-(void)showGender
{
    alertGender = [[UIAlertView alloc]initWithTitle: @"Select Gender"
                                               message: nil
                                              delegate: self
                                     cancelButtonTitle:@"Cancel"
                                     otherButtonTitles: @"Male",@"Female",@"Other",nil];
    
    [alertGender show];
    
}


-(void)showLocationActionsheet
{
    alertLocations = [[UIAlertView alloc]initWithTitle: @"Select Location"
                                               message: nil
                                              delegate: self
                                     cancelButtonTitle:@"Cancel"
                                     otherButtonTitles: nil];
    for(int i=0 ; i< arrLocation.count ; i++){
        Location *objL=[arrLocation objectAtIndex:i];
                [alertLocations addButtonWithTitle:objL.loc_name];

    }
    [alertLocations show];
   
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 9_0);  // after animation
{
    if(buttonIndex==alertView.cancelButtonIndex){
        return;
    }
    if(alertView==alertLocations){
        Location *objL=[arrLocation objectAtIndex:(buttonIndex-1)];
        location.text=objL.loc_name;
        location.tag=buttonIndex;

    }
    if(alertView==alertGender){
                phone.text=[alertView buttonTitleAtIndex:buttonIndex];
        phone.tag=buttonIndex;

    }
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)showNotificationView:(NSString *)msg :(UIImage *)img
{
    notiImg.image=img;
    lblMsg.text=msg;
       [viewNoti setBackgroundColor:[UIColor colorWithRed:125.0/255.0 green:33.0/255.0 blue:210.0/255.0 alpha:0.8]];
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y += frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:3.0];
                     }];
    
}
-(void)hideNotiView
{
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y -= frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         //                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:0.5];
                     }];
    
}
@end
@implementation UITextField (custom)
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 8,
                      bounds.size.width - 20, bounds.size.height - 16);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}
@end