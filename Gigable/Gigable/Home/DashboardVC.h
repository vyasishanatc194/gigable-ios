//
//  DashboardVC.h
//  Gigable
//
//  Created by Kalpit Gajera on 26/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayListDelegate.h"

@interface DashboardVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    
     IBOutlet UIView *bgblurView;
     IBOutlet UIPageControl *pageControl;
     IBOutlet UIScrollView *scrollView;
     IBOutlet UIView *topView;

     IBOutlet UIView *top_buttonView;
     IBOutlet UILabel *lblPlayTitle;
     IBOutlet UITableView *tblView;
     IBOutlet UIButton *menuButton;
    UILabel *lblCity;
    BOOL isLocalPlayList;
    NSString *strCity,*strCountry;
    NSMutableArray *arrTracks,*arrShow;
    IBOutlet UIView *view_monthSelection;
    int Tag,pageCount;;
    BOOL isHidden;
    IBOutlet UIView *monthSelectionView;
    IBOutlet UIButton *month1;
    IBOutlet UIButton *month2;
    IBOutlet UIButton *month3;
    IBOutlet UIButton *month4;
    IBOutlet UIButton *month5;
    IBOutlet UIButton *month6;

    IBOutlet UIButton *btnClose;
    
    IBOutlet UILabel *lblMiles;
    IBOutlet UISlider *lblSlider;
    IBOutlet UIView *viewNoti;

    IBOutlet UIView *view_Miles;
    IBOutlet UIImageView *notiImg;
    IBOutlet UILabel *lblMsg;
    
    IBOutlet UIView *view_mainShare;
    IBOutlet UIButton *shareButton;
    BOOL isFav;
    BOOL isShown;
    IBOutlet UIView *shareView;
    
    BOOL isPlay;
    
    UILabel *lblTick;

    UILabel *lblHeader;
    NSMutableArray *arrCurrent;
    NSMutableArray *arrPast;
    NSMutableArray *arrTic;
}

@property (assign,nonatomic)BOOL showNavMenu,isLocal,isFavSideMenu,isFavShowSideMenu;
@property (nonatomic, weak) id <PlayListDelegate> playListDelegate;

-(IBAction)selectMonth:(UIButton *)sender;
- (IBAction)hideMonthView:(id)sender;

- (IBAction)changeMiles:(UISlider *)sender;
- (IBAction)showShareButton:(id)sender;

- (IBAction)showAppLink:(id)sender;
- (IBAction)shareOnTwitter:(id)sender;
- (IBAction)shareOnFacebook:(id)sender;

@end
