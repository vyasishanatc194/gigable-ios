//
//  DashboardVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 26/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "DashboardVC.h"
#import "Static.h"
#import "Utility.h"
#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import "MFSideMenu.h"
#import "ASIFormDataRequest.h"
#import "PlayListVC.h"
#import "AppDelegate.h"
#import "HDNotificationView.h"
#import <Social/Social.h>
#import "TicketInfoVC.h"
#import "PaymentPageVC.h"
#import "PurchaseTicket.h"
@interface DashboardVC ()< UITableViewDataSource,SIMenuDelegate,PLAYSELECTDELEGATE,UIGestureRecognizerDelegate>

{
    UIView *playerView;
    AppDelegate *delegate;
    BOOL isRefresh;
    PlayList *objCurr;
    
}
@property (nonatomic, strong) SIMenuTable *table;
@property (nonatomic, strong) UIView *menuContainer;
@property (strong,nonatomic)    STKAudioPlayer* audioPlayer;
@end

@implementation DashboardVC
@synthesize playListDelegate;
@synthesize audioPlayer,isLocal,isFavSideMenu,isFavShowSideMenu;
@synthesize showNavMenu;
- (void)viewDidLoad {
    self.playListDelegate=self;
    self.navigationController.navigationBarHidden=true;
    [super viewDidLoad];
    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
    delegate=[AppDelegate initAppdelegate];
    bgblurView.hidden=true;
    
    
    
    if(![[NSUserDefaults standardUserDefaults]valueForKey:@"FirstLaunch"]){
        [[NSUserDefaults standardUserDefaults]setInteger:3 forKey:@"Month"];
        [[NSUserDefaults standardUserDefaults]setFloat:300.0 forKey:@"Distance"];
        
        scrollView.hidden=false;
        pageControl.hidden=false;
        [self showintroView];
    }else{
        scrollView.hidden=true;
        pageControl.hidden=true;
        [self getAllShows];
    }
    
    lblSlider.value=[[NSUserDefaults standardUserDefaults]floatForKey:@"Distance"];
    lblMiles.text=[NSString stringWithFormat:@"%.0f",lblSlider.value];
    [self setMonthButton];
    Tag=0;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getCurrDetail)
                                                 name:@"FavNoti"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshPlaylist)
                                                 name:@"FavClick"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLocalPlay) name:@"SHOWMENU" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:@"LOCUPDATE" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locUpdate) name:@"PROFUPDATE" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setPlayIndicator) name:@"PLAYCHANGE" object:nil];
    
    // Do any additional setup after loading the view from its nib.
    
    pageCount=3;
    
}


-(void)getLocalPlay
{
    Tag=0;
    [self getAllShows];
    [[AppDelegate initAppdelegate]pausePlayer];
}
-(void)setPlayIndicator
{
    [tblView reloadData];
}
-(void)locUpdate{
    isRefresh=true;
    
}

-(void)showNotificationView:(NSString *)msg :(UIImage *)img
{
    notiImg.image=img;
    lblMsg.text=msg;
    [viewNoti setBackgroundColor:[UIColor colorWithRed:125.0/255.0 green:33.0/255.0 blue:210.0/255.0 alpha:0.8]];
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         CGRect frame = topView.frame;
                         frame.origin.y += frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:3.0];
                     }];
    
}
-(void)hideNotiView
{
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         CGRect frame = topView.frame;
                         frame.origin.y -= frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         //                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:0.5];
                     }];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[AppDelegate initAppdelegate].info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];
    [AppDelegate initAppdelegate].isPushed=false;
    
    if([AppDelegate initAppdelegate].isRef==false){
        if(self.showNavMenu==true){
            if([AppDelegate initAppdelegate].arrPlayList.count>0){
                [self showMenu:nil];
            }
        }
        
        
        strCountry=@"";
        strCity=@"";
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:[AppDelegate initAppdelegate].locationManager.location
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           for (CLPlacemark *placemark in placemarks) {
                               if (error){
                                   //NSLog(@"Geocode failed with error: %@", error);
                                   return;
                                   
                               }
                               CLPlacemark *placemark = [placemarks objectAtIndex:0];
                               
                               //NSLog(@"placemark.country %@",placemark.country);
                               strCity=placemark.locality;
                               strCountry=placemark.administrativeArea;
                               //NSLog(@"placemark.locality %@",placemark.locality);
                               
                               [lblCity setText:[NSString stringWithFormat:@"%@, %@",strCity,strCountry]];
                               [tblView reloadData];
                               
                           }
                       }];
        
        
    }
    if([AppDelegate initAppdelegate].isRef==true){
        [AppDelegate initAppdelegate].playerView.hidden=true;
        [[AppDelegate initAppdelegate].arrPlayList removeAllObjects];
        [arrShow removeAllObjects];
        [arrTracks removeAllObjects];
        lblPlayTitle.text=@"";
        [self getAllShows];
        [AppDelegate initAppdelegate].isRef=false;
    }
    
    if(isHidden==true){
        delegate.playerView.hidden=false;
        isHidden=false;
    }
    
    
    if(self.isFavSideMenu==true){
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 50)];
        
        lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(10,0, WIDTH-20, 50)];
        lblHeader.font=[UIFont fontWithName:FONTNAME size:20.0];
        lblHeader.textAlignment=NSTextAlignmentCenter;
        lblHeader.textColor=[UIColor whiteColor];
        
        
        [view addSubview:lblHeader];
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 49, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [view addSubview:lblSep];
        [view setBackgroundColor:[UIColor clearColor]];
        tblView.tableHeaderView=view;
        bgblurView.hidden=false;
        
        [self getMyFavourites];
    }
    
    if(self.isFavShowSideMenu){
        delegate.playerView.hidden=true;
        [self getMyFavouritesShows];
        [tblView setFrame:CGRectMake(0,80, WIDTH, HEIGHT-80)];
        
    }
    
}
-(void)reloadData
{
    [tblView reloadData];
}
-(void)showintroView{
    top_buttonView.hidden=true;
    tblView.hidden=true;
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    for(int i=0; i<4; i++)
    {
        CGRect frame;
        frame.origin.x = scrollView.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = scrollView.frame.size;
        
        UIView *subview = [[UIView alloc] initWithFrame:frame];
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 50, WIDTH-90, 100)];
        [lbl setFont:[UIFont fontWithName:FONTNAME size:16.0]];
        [lbl setTextAlignment:NSTextAlignmentLeft];
        //lbl.text=@"Gigable will use your location to create a playlist of music by artists that are performing nearby. \n \nThere is something to do tonight";
        lbl.text=@"GIGABLE will use your location to create a playlist of music by artists that are performing nearby.";
        lbl.numberOfLines=10;
        lbl.alpha=1.0;
        [lbl setTextColor:[UIColor whiteColor]];
        [subview addSubview:lbl];
        [lbl sizeToFit];
        
        if(i==2){
            
            lblCity=[[UILabel alloc]initWithFrame:CGRectMake(40, lbl.frame.origin.y+lbl.frame.size.height+5, WIDTH-80, 30)];
            [lblCity setFont:[UIFont fontWithName:FONTNAMEBOLD size:14.0]];
            //                                  [lblCity setText:[NSString stringWithFormat:@"%@,%@",strCity,strCountry]];
            [lblCity setTextColor:[UIColor whiteColor]];
            [subview addSubview:lblCity];
            
            
            
            lbl.text=@"Great! you are all set, to build your local playlist GIGABLE will use your location:";
            UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(40, lblCity.frame.origin.y+lblCity.frame.size.height+5, WIDTH-100, 200)];
            [lbl setFont:[UIFont fontWithName:FONTNAME size:16.0]];
            [lbl setTextAlignment:NSTextAlignmentLeft];
            
            lbl.text=@"You can always change your location whenever you need to.";
            lbl.numberOfLines=3;
            [lbl setTextColor:[UIColor whiteColor]];
            
            //                                  [lbl setBackgroundColor:NAVNBAR_COLOR];
            
            [subview addSubview:lbl];
            [lbl sizeToFit];
            
            UIButton *btnStartUp=[[UIButton alloc]initWithFrame:CGRectMake(40, 220, 160, 80)];
            [btnStartUp.titleLabel setFont:[UIFont fontWithName:FONTNAMEBOLD size:13.0]];
            [btnStartUp addTarget:self action:@selector(getLoaclPlayList) forControlEvents:UIControlEventTouchUpInside];
            
            btnStartUp.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            // you probably want to center it
            btnStartUp.titleLabel.textAlignment = NSTextAlignmentLeft; // if you want to
            [btnStartUp setTitle: @"Build my local \nplaylist" forState: UIControlStateNormal];
            [btnStartUp setBackgroundColor:NAVNBAR_COLOR_LIGHT];
            
            [subview addSubview:btnStartUp];
            
        }else if(i==3){
            
            [lbl setFrame:CGRectMake(40, 50, WIDTH-60, 60)];
            lbl.text=@"Sorry but we couldn't find any shows in the next 3 months, within 300 miles of your location.";
            
            UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(40, lblCity.frame.origin.y+lblCity.frame.size.height+5, WIDTH-80, 200)];
            [lbl setFont:[UIFont fontWithName:FONTNAME size:16.0]];
            [lbl setTextAlignment:NSTextAlignmentLeft];
            
            lbl.text=@"Hint: try adjusting the radius and time";
            lbl.numberOfLines=3;
            [lbl setTextColor:[UIColor whiteColor]];
            [subview addSubview:lbl];
            [lbl sizeToFit];
            
            UIButton *btnStartUp=[[UIButton alloc]initWithFrame:CGRectMake(40, 200, 170, 70)];
            btnStartUp.alpha=1.0;
            [btnStartUp.titleLabel setFont:[UIFont fontWithName:FONTNAMEBOLD size:13.0]];
            
            btnStartUp.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            // you probably want to center it
            btnStartUp.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
            [btnStartUp setTitle: @"Change\nSettings" forState: UIControlStateNormal];
            [btnStartUp setBackgroundColor:NAVNBAR_COLOR_LIGHT];
            
            [subview addSubview:btnStartUp];
        }
        else
        {
            
            UIButton *btnStartUp=[[UIButton alloc]initWithFrame:CGRectMake(40, 160, 170, 70)];
            [btnStartUp setBackgroundColor:NAVNBAR_COLOR_LIGHT];
            [btnStartUp.titleLabel setFont:[UIFont fontWithName:FONTNAMEBOLD size:13.0]];
            btnStartUp.alpha=1.0;
            
            [btnStartUp setTitle:@"Setup GIGABLE" forState:UIControlStateNormal];
            [btnStartUp addTarget:self action:@selector(skipSetup) forControlEvents:UIControlEventTouchUpInside];
            [subview addSubview:btnStartUp];
            
        }
        
        
        
        [scrollView addSubview:subview];
    }
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * 4, scrollView.frame.size.height);
    
}
-(void)getLoaclPlayList{
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",@"local_playlist",[AppDelegate initAppdelegate].objUser.locId,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            if(arrTracs.count >0){
                scrollView.hidden=true;
                tblView.hidden=false;
                top_buttonView.hidden=false;
                pageControl.hidden=true;
                [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"FirstLaunch"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self getAllShows];
            }else{
                pageCount=4;
            }
            
            if(arrShows.count==0){
                [self showNotificationView:@"No shows returned. Please try different settings" :[UIImage imageNamed:@"info.png"]];
            }
            
            // [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictRes valueForKey:@"DATA"]];
            //[[AppDelegate initAppdelegate]showDashboardMenu];
            
        }else{
            //   [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
            
            
        }
    }];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- TABLEVIEW METHOD
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.isFavShowSideMenu){
        return 3;    //count of section
        
    }
    
    return 1;    //count of section
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(self.isFavShowSideMenu){
        if(section==0){
            UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
            
            lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(10,10, WIDTH-20, 30)];
            lblHeader.font=[UIFont fontWithName:FONTNAME size:20.0];
            lblHeader.textAlignment=NSTextAlignmentLeft;
            lblHeader.textColor=[UIColor whiteColor];
            
            lblHeader.text=[NSString stringWithFormat:@"My Shows"];
            
            
            lblTick=[[UILabel alloc]initWithFrame:CGRectMake(10,40, WIDTH-20, 20)];
            lblTick.font=[UIFont fontWithName:FONTNAME size:13.0];
            lblTick.textAlignment=NSTextAlignmentLeft;
            lblTick.textColor=NAVNBAR_COLOR;
            lblTick.text=[NSString stringWithFormat:@"Tickets (%ld)",(unsigned long)arrTic.count];
            
            [view addSubview:lblTick];
            [view addSubview:lblHeader];
            
            UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 59, WIDTH, 0.5)];
            [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
            [view addSubview:lblSep];
            [view setBackgroundColor:[UIColor clearColor]];
            
            return  view;
        }
        if(section==1){
            UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 20)];
            lblTick=[[UILabel alloc]initWithFrame:CGRectMake(10,0, WIDTH-20, 20)];
            lblTick.font=[UIFont fontWithName:FONTNAME size:13.0];
            lblTick.textAlignment=NSTextAlignmentLeft;
            lblTick.textColor=NAVNBAR_COLOR;
            lblTick.text=[NSString stringWithFormat:@"Watching... (%ld)",(unsigned long)arrCurrent.count];
            
            [view addSubview:lblTick];
            UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 19, WIDTH, 0.5)];
            [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
            [view addSubview:lblSep];
            [view setBackgroundColor:[UIColor clearColor]];
            return  view;
            
        }
        if(section==2){
            UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 20)];
            lblTick=[[UILabel alloc]initWithFrame:CGRectMake(10,0, WIDTH-20, 20)];
            lblTick.font=[UIFont fontWithName:FONTNAME size:13.0];
            lblTick.textAlignment=NSTextAlignmentLeft;
            lblTick.textColor=NAVNBAR_COLOR;
            lblTick.text=[NSString stringWithFormat:@"Past Shows (%ld)",(unsigned long)arrPast.count];
            
            [view addSubview:lblTick];
            UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 19, WIDTH, 0.5)];
            [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
            [view addSubview:lblSep];
            [view setBackgroundColor:[UIColor clearColor]];
            return  view;
            
        }
        return nil;
    }
    
    if(Tag==1  || Tag==2){
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
        
        UIButton *btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 0, 20, 40)];
        
        [btn setImage:[UIImage imageNamed:@"Back_track.png"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(backButtonFromtrackList) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(25,0, WIDTH-50, 40)];
        lbl.font=[UIFont fontWithName:FONTNAME size:20.0];
        lbl.textAlignment=NSTextAlignmentCenter;
        lbl.textColor=[UIColor whiteColor];
        if(Tag==1){
            lbl.text=[NSString stringWithFormat:@"%lu   Shows in this playlist",(unsigned long)arrShow.count];
        }else{
            lbl.text=[NSString stringWithFormat:@"%lu Tracks in this playlist",(unsigned long)arrTracks.count];
        }
        [view addSubview:lbl];
        
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 39, WIDTH, 0.5)];
        [lblSep setBackgroundColor:NAVNBAR_COLOR];
        [view addSubview:lblSep];
        [view setBackgroundColor:[UIColor clearColor]];
        return view;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(self.isFavShowSideMenu){
        if(section==0){
            return 60;
        }else{
            return 20;
        }
        
    }
    
    if(Tag==2){
        return 40;
    }
    
    if(Tag==1 || Tag==2 ){
        return 0;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.isFavShowSideMenu){
        return 80;
        
    }
    
    if(self.isFavSideMenu){
        
        return 50;
        
    }
    if(Tag==2 ){
        return 60;
    }
    return 60;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.isFavShowSideMenu){
        if(section==0){
            return arrTic.count;
        }
        if(section==1){
            return arrCurrent.count;
        }
        if(section==2){
            return arrPast.count;
        }
        
    }
    
    
    if(self.isFavSideMenu){
        return arrTracks.count;
    }
    
    if(Tag==0){
        return 5;    //count number of row from counting array hear cataGorry is An Array
    }
    if(Tag==1){
        return arrShow.count;
    }
    if(Tag==2){
        return arrTracks.count;
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    cell.backgroundColor=[UIColor clearColor];
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([tblView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tblView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tblView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tblView setLayoutMargins:UIEdgeInsetsZero];
    }
}




- (void)setCellColor:(UIColor *)color ForCell:(UITableViewCell *)cell {
    cell.contentView.backgroundColor = color;
    cell.backgroundColor = color;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                  reuseIdentifier:MyIdentifier] ;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    
    
    if(self.isFavSideMenu==true){
        Track *objSh=[arrTracks objectAtIndex:indexPath.row];
        cell.textLabel.text=objSh.trk_title;
        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
        cell.textLabel.textColor=[UIColor whiteColor];
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0,49, WIDTH, 0.5)];
        
        
        
        
        UIView *accessView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
        UIImageView *imgB=[[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 30, 30)];
        
        if(delegate.index_current_song==indexPath.row){
            imgB.image=[UIImage imageNamed:@"CurrentPlaying.png"];
        }
        [accessView addSubview:imgB];
        
        UIButton *btnStar=[[UIButton alloc]initWithFrame:CGRectMake(35,10, 30,30)];
        [btnStar setImage:[UIImage imageNamed:@"close_over.png"] forState:UIControlStateNormal];
        [btnStar addTarget:self action:@selector(favTrackClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnStar.tag=indexPath.row;
        [btnStar.layer setCornerRadius:15.0];
        [btnStar.layer setBorderColor:NAVNBAR_COLOR.CGColor];
        [btnStar.layer setBorderWidth:1.0];
        [accessView addSubview:btnStar];
        
        cell.accessoryView=accessView;
        
        
        
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor =CELL_SELECTTION_COLOR;
        [cell setSelectedBackgroundView:bgColorView];
        return cell;
    }
    else if(self.isFavShowSideMenu){
        if(indexPath.section==0){
            
            Show *objSh=[arrTic objectAtIndex:indexPath.row];
            //        cell.textLabel.text=objSh.show_title;
            //        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
            //        cell.textLabel.textColor=[UIColor whiteColor];
            
            NSString *dateString = objSh.show_Full_Date;
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            NSDate *yourDate = [dateFormatter dateFromString:dateString];
            dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";
            //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
            
            UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 15, WIDTH-100, 15)];
            lblDate.font=[UIFont fontWithName:FONTNAME size:13.0];
            [lblDate setText:[dateFormatter stringFromDate:yourDate]];
            lblDate.textColor=[UIColor whiteColor];
            
            [cell.contentView addSubview:lblDate];
            
            UILabel *lblShowName=[[UILabel alloc]initWithFrame:CGRectMake(5, 30, WIDTH-100, 20)];
            lblShowName.font=[UIFont fontWithName:FONTNAMEBOLD size:15.0];
            lblShowName.textColor=[UIColor whiteColor];
            [lblShowName setText:objSh.show_title];
            
            [cell.contentView addSubview:lblShowName];
            
            
            UILabel *lblDesc=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, WIDTH-100, 15)];
            lblDesc.font=[UIFont fontWithName:FONTNAME size:14.0];
            [lblDesc setText:objSh.show_desc];
            lblDesc.textColor=[UIColor whiteColor];
            
            [cell.contentView addSubview:lblDesc];
            
            UIButton *btnTic=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 33,33)];
            [btnTic setImage:[UIImage imageNamed:@"active-ticket-icon.png"] forState:UIControlStateNormal];
            [btnTic addTarget:self action:@selector(purchaseTicClicked:) forControlEvents:UIControlEventTouchUpInside];
            btnTic.accessibilityLabel=objSh.show_id;
            btnTic.tag=indexPath.row;
            cell.accessoryView =btnTic;
            
        }
        if(indexPath.section==1){
            Show *objSh=[arrCurrent objectAtIndex:indexPath.row];
            //        cell.textLabel.text=objSh.show_title;
            //        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
            //        cell.textLabel.textColor=[UIColor whiteColor];
            
            NSString *dateString = objSh.show_Full_Date;
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            NSDate *yourDate = [dateFormatter dateFromString:dateString];
            dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";
            //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
            
            UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 15, WIDTH-100, 15)];
            lblDate.font=[UIFont fontWithName:FONTNAME size:13.0];
            [lblDate setText:[dateFormatter stringFromDate:yourDate]];
            lblDate.textColor=[UIColor whiteColor];
            
            [cell.contentView addSubview:lblDate];
            
            UILabel *lblShowName=[[UILabel alloc]initWithFrame:CGRectMake(5, 30, WIDTH-100, 20)];
            lblShowName.font=[UIFont fontWithName:FONTNAMEBOLD size:15.0];
            lblShowName.textColor=[UIColor whiteColor];
            [lblShowName setText:objSh.show_title];
            
            [cell.contentView addSubview:lblShowName];
            
            
            UILabel *lblDesc=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, WIDTH-100, 15)];
            lblDesc.font=[UIFont fontWithName:FONTNAME size:14.0];
            [lblDesc setText:objSh.show_desc];
            lblDesc.textColor=[UIColor whiteColor];
            
            [cell.contentView addSubview:lblDesc];
            UIView *view_acc=[[UIView alloc]initWithFrame:CGRectMake(0, 5, 65, 80)];
            [view_acc setBackgroundColor:[UIColor clearColor]];
            
            //        UIButton *btnTic=[[UIButton alloc]initWithFrame:CGRectMake(0, 18, 27,42)];
            UIButton *btnTic=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 20,31)];
            
            [btnTic setImage:[UIImage imageNamed:@"purchaseTick.png"] forState:UIControlStateNormal];
            [btnTic addTarget:self action:@selector(showTicketInfo:) forControlEvents:UIControlEventTouchUpInside];
            btnTic.accessibilityLabel=objSh.show_id;
            btnTic.tag=indexPath.row;
            [view_acc addSubview:btnTic];
            
            
            UIButton *btnDel=[[UIButton alloc]initWithFrame:CGRectMake(35, 25, 30,30)];
            [btnDel addTarget:self action:@selector(deleteShowClicked:) forControlEvents:UIControlEventTouchUpInside];
            btnDel.tag=indexPath.row;
            btnDel.accessibilityLabel=objSh.show_id;
            
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(44, 34, 12,12)];
            [img setImage:[UIImage imageNamed:@"close_over.png"]];
            [view_acc addSubview:img];
            
            
            [btnDel.layer setCornerRadius:15.0];
            [btnDel.layer setBorderColor:NAVNBAR_COLOR.CGColor];
            [btnDel.layer setBorderWidth:1.0];
            [view_acc addSubview:btnDel];
            cell.accessoryView=view_acc;
            
        }
        if(indexPath.section==2){
            Show *objSh=[arrPast objectAtIndex:indexPath.row];
            
            NSString *dateString = objSh.show_Full_Date;
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            NSDate *yourDate = [dateFormatter dateFromString:dateString];
            dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";
            //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
            
            UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 15, WIDTH-100, 15)];
            lblDate.font=[UIFont fontWithName:FONTNAME size:13.0];
            [lblDate setText:[dateFormatter stringFromDate:yourDate]];
            lblDate.textColor=[UIColor whiteColor];
            
            [cell.contentView addSubview:lblDate];
            
            UILabel *lblShowName=[[UILabel alloc]initWithFrame:CGRectMake(5, 30, WIDTH-100, 20)];
            lblShowName.font=[UIFont fontWithName:FONTNAMEBOLD size:15.0];
            lblShowName.textColor=[UIColor whiteColor];
            [lblShowName setText:objSh.show_title];
            
            [cell.contentView addSubview:lblShowName];
            
            
            UILabel *lblDesc=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, WIDTH-100, 15)];
            lblDesc.font=[UIFont fontWithName:FONTNAME size:14.0];
            [lblDesc setText:objSh.show_desc];
            lblDesc.textColor=[UIColor whiteColor];
            
            [cell.contentView addSubview:lblDesc];
            
        }
        
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0,79, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [cell.contentView addSubview:lblSep];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor =CELL_SELECTTION_COLOR;
        [cell setSelectedBackgroundView:bgColorView];
        return cell;
    }else{
        
        
        if(Tag==0){
            
            CGRect frame=CGRectMake(70, 0, 70,60);
            
            if(indexPath.row==0){
                UIImageView *imgView=[[UIImageView alloc]initWithFrame:frame];
                imgView.contentMode=UIViewContentModeCenter;
                imgView.image=[UIImage imageNamed:@"Pin_dash.png"];
                
                [cell.contentView addSubview:imgView];
            }else{
                UILabel *lbl=[[UILabel alloc]initWithFrame:frame];
                lbl.text=@"0";
                [lbl adjustsFontSizeToFitWidth];
                if(indexPath.row==2){
                    if(arrShow.count >0){
                        lbl.text=[NSString stringWithFormat:@"%ld",(unsigned long)arrShow.count];
                    }else{
                        lbl.text=@"0";
                    }
                }
                
                if(indexPath.row==1){
                    if(arrTracks.count >0){
                        lbl.text=[NSString stringWithFormat:@"%ld",(unsigned long)arrTracks.count];
                    }else{
                        lbl.text=@"0";
                    }
                }
                if(indexPath.row==3 || indexPath.row==4){
                    if(indexPath.row==3){
                        lbl.frame=CGRectMake(60, 0, 80,60);
                        lbl.text=[NSString stringWithFormat:@"%.0f",[[NSUserDefaults standardUserDefaults]floatForKey:@"Distance"]];
                        
                    }else{
                        lbl.text=[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"Month"]];
                    }
                }
                lbl.textAlignment=NSTextAlignmentCenter;
                lbl.layer.cornerRadius=25;
                lbl.layer.masksToBounds=true;
                lbl.font=[UIFont fontWithName:FONTNAME size:30];
                lbl.textColor=[UIColor whiteColor];
                [cell.contentView addSubview:lbl];
            }
            CGRect lblframe=CGRectMake(140, 0, WIDTH-130,60 );
            UILabel *lblTitle=[[UILabel alloc]initWithFrame:lblframe];
            
            if(indexPath.row==0){
                
                lblTitle.text=[NSString stringWithFormat:@"%@, %@",strCity,strCountry];
                
            }
            if(indexPath.row==2){
                
                lblTitle.text=@"Shows";
            }
            if(indexPath.row==1){
                lblTitle.text=@"Tracks";
            }
            
            if(indexPath.row==3){
                if([AppDelegate initAppdelegate].arrPlayList.count >1){
                    lblTitle.text=@"Miles";
                }else{
                    lblTitle.text=@"Miles";
                }
            }
            
            if(indexPath.row==4){
                lblTitle.text=@"Months";
            }
            
            lblTitle.font=[UIFont fontWithName:FONTNAME size:14.0];
            lblTitle.textAlignment=NSTextAlignmentLeft;
            
            lblTitle.textColor=[UIColor whiteColor];
            cell.textLabel.textColor=[UIColor whiteColor];
            
            [cell.contentView addSubview:lblTitle];
            
        }
        if(Tag==1){
            
            Show *objSh=[arrShow objectAtIndex:indexPath.row];
            cell.textLabel.text=objSh.show_title;
            cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
            cell.textLabel.textColor=[UIColor whiteColor];
            
            UIImageView *imgB=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"play_indicator.png"]];
            cell.accessoryView=imgB;
            
            
        }
        if(Tag==2){
            
            if(delegate.index_current_song==indexPath.row){
                UIImageView *imgB=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"CurrentPlaying.png"]];
                cell.accessoryView=imgB;
            }
            
            UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0,59, WIDTH, 0.5)];
            [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
            [cell.contentView addSubview:lblSep];
            [lblSep setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.2]];
            [cell.contentView addSubview:lblSep];
            
            Track *objSh=[arrTracks objectAtIndex:indexPath.row];
            cell.textLabel.text=objSh.trk_title;
            cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
            cell.textLabel.textColor=[UIColor whiteColor];
            
            cell.detailTextLabel.text=objSh.art_name;
            cell.detailTextLabel.font=[UIFont fontWithName:FONTNAME size:13.0];
            cell.detailTextLabel.textColor=[UIColor colorWithWhite:1.0 alpha:0.6];
            if(isFav==true){
                UIView *accessView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
                UIImageView *imgB=[[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 30, 30)];
                
                if(delegate.index_current_song==indexPath.row){
                    imgB.image=[UIImage imageNamed:@"CurrentPlaying.png"];
                    [accessView addSubview:imgB];
                }
                UIButton *btnStar=[[UIButton alloc]initWithFrame:CGRectMake(30,0, 50, 50)];
                [btnStar setImage:[UIImage imageNamed:objSh.isFav?@"Star-Icon-Active.png":@"Star-Icon.png"] forState:UIControlStateNormal];
                [btnStar addTarget:self action:@selector(favTrackClicked:) forControlEvents:UIControlEventTouchUpInside];
                btnStar.tag=indexPath.row;
                [accessView addSubview:btnStar];
                cell.accessoryView=accessView;
                
            }
            
        }
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor =CELL_SELECTTION_COLOR;
        [cell setSelectedBackgroundView:bgColorView];
    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    if(self.isFavShowSideMenu==true){
        if(indexPath.section==1){
            Show *objSh=[arrCurrent objectAtIndex:indexPath.row];
            TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
            objT.objShow=objSh;
            [self.navigationController pushViewController:objT animated:true];
            
        }
        
        return;
    }
    
    
    if(self.isFavSideMenu==true){
        if(delegate.isMyFav==true){
            delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
            
        }
        [delegate playAudioWithIndex:indexPath.row];
        return;
    }
    if(Tag==0){
        if(indexPath.row==2){
            
            PlayListVC *objPlayList=[[PlayListVC alloc]initWithNibName:@"PlayListVC" bundle:nil];
            objPlayList.playdelegate=self;
            objPlayList.isTracks=false;
            objPlayList.arrData=arrShow;
            objPlayList.objPlayList=objCurr;
            objPlayList.playListDelegate=self.playListDelegate;
            [self.navigationController pushViewController:objPlayList animated:true];
            
            return;
        }
        
        if(indexPath.row==1){
            
            if(arrTracks.count >0){
                
                if([[AppDelegate initAppdelegate].objPlaylist.play_id isEqualToString:objCurr.play_id]){
                    Tag=2;
                    bgblurView.hidden=true;;
                    [tblView reloadData];
                    return;
                    
                }
                
                Tag=2;
                bgblurView.hidden=true;;
                lblPlayTitle.text=objCurr.playName;
                [AppDelegate initAppdelegate].objPlaylist=objCurr;
                [self getPlayLisTDetail:objCurr.play_id];
                [tblView reloadData];
                
            }
            return;
        }
        if(indexPath.row==3){
            [self showMilesSelectionSheet];
        }
        
        if(indexPath.row==4){
            [self showMonthSelectionSheet];
        }
        
    }
    
    if(Tag==1){
        if(indexPath.row==1){
            Tag=1;
            Show *objS=[arrShow objectAtIndex:indexPath.row];
            bgblurView.hidden=false;
            tblView.scrollEnabled=true;
            
            [self getPlayLisTDetail:objS.show_id];
            
        }
    }
    if(Tag==2){
        //            bgblurView.hidden=false;
        //            tblView.scrollEnabled=true;
        delegate.arrTracks=arrTracks;
        delegate.index_current_song=indexPath.row;
        [delegate playAudioWithIndex:delegate.index_current_song];
        [tblView reloadData];
        
    }
    
    
}
-(void)favTrackClicked:(UIButton *)btn{
    
    
    Track *objTr=[arrTracks objectAtIndex:btn.tag];
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"usrplay_lgn_id=%@&utp_trk_id=%@",[AppDelegate initAppdelegate].objUser.login_id,objTr.id_track];
    
    [Utility executePOSTRequestwithServicetype:MAKE_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
            if(objTr.isFav==true){
                objTr.isFav=false;
                [arrTracks removeObjectAtIndex:btn.tag];
                //                [tblView reloadData];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"FavClick"
                 object:self];
                [[AppDelegate initAppdelegate]checkFavTrack:objTr];
                
            }else{
                objTr.isFav=true;
            }
        }else{
            //            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
    
}
#pragma mark- MARK-TOP MENU METHODS


- (IBAction)showSideMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //        [self setupMenuBarButtonItems];
    }];
    
}
#pragma mark- ANIMATION METHODS
-(IBAction)showMenu:(id)sender{
    if([AppDelegate initAppdelegate].isRef==true){
        return;
    }
    if([AppDelegate initAppdelegate].arrPlayList.count==0){
        return;
    }
    [self onShowMenu];
}
- (void)onShowMenu
{
    if(menuButton.selected){
        menuButton.selected=false;
        
        [self onHideMenu];
    }else{
        //    if (!self.table) {
        UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect frame = mainWindow.frame;
        //        frame.origin.y += self.view.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
        
        frame.origin.y=80;
        frame.size.height-=150;
        NSMutableArray *iem=[[NSMutableArray alloc]init];
        
        
        if([AppDelegate initAppdelegate].arrPlayList.count >0){
            
            for(int i=0;i<[AppDelegate initAppdelegate].arrPlayList.count;i++){
                PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:i];
                [iem addObject:objs.playName];
            }
        }
        
        self.table = [[SIMenuTable alloc] initWithFrame:frame items:iem];
        self.table.menuDelegate = self;
        self.table.arrData=[AppDelegate initAppdelegate].arrPlayList;
        self.menuContainer=self.view;
        [self.menuContainer addSubview:self.table];
        [self rotateArrow:M_PI];
        [self.table show];
        menuButton.selected=true;
    }
}

- (void)onHideMenu
{
    [self rotateArrow:0];
    [self.table hide];
}

- (void)rotateArrow:(float)degrees
{
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        //        self.menuButton.arrow.layer.transform = CATransform3DMakeRotation(degrees, 0, 0, 1);
    } completion:NULL];
}

#pragma mark -
#pragma mark Playlist Top Delegate methods
- (void)didSelectPlayAtIndex:(NSUInteger)index
{
    [tblView setFrame:CGRectMake(0,80, WIDTH, HEIGHT-80-135)];
    
    self.isFavShowSideMenu=false;
    self.isFavSideMenu=false;
    PlayList *obbjs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:index];
    tblView.tableHeaderView=nil;
    bgblurView.hidden=true;;
    
    
    if([obbjs.totaltrack integerValue]==0){
        //        [Utility showAlrtWithMessage:@"No Tracks Found"];
        [self showNotificationView:@"No Tracks Found" :[UIImage imageNamed:@"info.png"]];
        return;
    }
    
    
    lblPlayTitle.text=obbjs.playName;
    [self onHandleMenuTap:nil];
    
    
    if([obbjs.play_id isEqualToString:@"local_playlist"]){
        
        Tag=0;
        bgblurView.hidden=true;;
        
    }else{
        if([obbjs.play_id isEqualToString:@"fav_playlist"]){
            isFav=true;
        }else{
            isFav=false;
        }
        
        Tag=2;
        //        bgblurView.hidden=false;
        
    }
    
    isPlay=true;
    Tag=0;
    
    
    lblPlayTitle.text=obbjs.playName;
    [AppDelegate initAppdelegate].objPlaylist=obbjs;
    [self getPlayLisTDetail:obbjs.play_id];
    objCurr=obbjs;
    
    
}

- (void)didSelectPlayItemListItemAtIndex:(NSUInteger)index;
{
    [tblView setFrame:CGRectMake(0,80, WIDTH, HEIGHT-80-135)];
    
    self.isFavShowSideMenu=false;
    tblView.tableHeaderView=nil;
    bgblurView.hidden=true;;
    
    self.isFavSideMenu=false;
    PlayList *obbjs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:index];
    
    if([obbjs.totaltrack integerValue]==0){
        [self showNotificationView:@"No Tracks Found" :[UIImage imageNamed:@"info.png"]];
        
        return;
    }
    
    lblPlayTitle.text=obbjs.playName;
    [self onHandleMenuTap:nil];
    
    
    //    if([obbjs.play_id isEqualToString:@"local_playlist"]){
    //
    //        Tag=0;
    //        bgblurView.hidden=true;;
    //
    //    }else{
    //        if([obbjs.play_id isEqualToString:@"fav_playlist"]){
    //            isFav=true;
    //        }else{
    //            isFav=false;
    //        }
    if([obbjs.play_id isEqualToString:@"fav_playlist"]){
        isFav=true;
    }else{
        isFav=false;
    }
    
    Tag=2;
    bgblurView.hidden=false;
    
    //    }
    //
    //    isPlay=true;
    //    Tag=0;
    
    
    lblPlayTitle.text=obbjs.playName;
    [AppDelegate initAppdelegate].objPlaylist=obbjs;
    [self getPlayLisTDetail:obbjs.play_id];
    objCurr=obbjs;
    
}
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    [tblView setFrame:CGRectMake(0,80, WIDTH, HEIGHT-80-135)];
    
    //    self.isFavSideMenu=false;
    //    self.isFavShowSideMenu=false;
    tblView.tableHeaderView=nil;
    bgblurView.hidden=true;;
    
    PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:index];
    
    if([objs.totaltrack integerValue]==0){
        [self showNotificationView:@"No Tracks Found" :[UIImage imageNamed:@"info.png"]];
        return;
    }
    //       lblPlayTitle.text=objs.playName;
    
    [self onHandleMenuTap:nil];
    
    [AppDelegate initAppdelegate].objPlaylist=objs;
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    [self showOverLayDetail:objs.play_id withName:objs.playName withObj:objs];
    
}


-(void)showOverLayDetail:(NSString *)idPlayList withName:(NSString *)name withObj:(PlayList *)objP
{
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",idPlayList,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTr,*arrSho;
            
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            if(arrTracs.count >0){
                arrTr=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrTracs)
                {
                    [arrTr addObject:[Track initWithResponceDict:dict]];
                }
                
            }
            //                arrTracks=arrTr;
            //            delegate.index_current_song=0;
            //            [delegate playAudioWithIndex:delegate.index_current_song];
            //            [delegate pausePlayer];
            if(arrShows.count >0){
                arrSho=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows)
                {
                    [arrSho addObject:[Show initWithResponceDict:dict]];
                }
                
            }else{
                
            }
            isHidden=true;
            PlayListVC *objPlayList=[[PlayListVC alloc]initWithNibName:@"PlayListVC_New" bundle:nil];
            objPlayList.isTracks=true;
            objPlayList.playdelegate=self;
            objPlayList.isOverLay=true;
            objPlayList.arrShows=arrSho;
            
            objPlayList.currentShowName=name;
            objPlayList.arrData=arrTr;
            objPlayList.objPlayList=objP;
            [AppDelegate initAppdelegate].playerView.hidden=true;
            
            [self.navigationController pushViewController:objPlayList animated:true];
        }else{
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
    
}

- (void)didBackgroundTap
{
    
    //    self.menuButton.isActive = !self.menuButton.isActive;
    [self onHandleMenuTap:nil];
}


- (void)onHandleMenuTap:(id)sender
{
    if (!menuButton.selected) {
        menuButton.selected=true;
        //NSLog(@"On show");
        [self onShowMenu];
    } else {
        //NSLog(@"On hide");
        [self onHideMenu];
        menuButton.selected=false;
    }
}
#pragma mark SCROLLVIEW DELEGATE
-(void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    if([sender isKindOfClass:[UITableView class]]){
        return;
    }
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage=page;
    
    if(page==1){
        [self getPermissionForLocation];
        
    }
    if(page==pageCount){
        scrollView.hidden=true;
        tblView.hidden=false;
        top_buttonView.hidden=false;
        pageControl.hidden=true;
        [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"FirstLaunch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self getAllShows];
        
    }
}
-(void)getPermissionForLocation
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
}


-(void)skipSetup
{
    [self getPermissionForLocation];
    scrollView.hidden=true;
    tblView.hidden=false;
    top_buttonView.hidden=false;
    pageControl.hidden=true;
    [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"FirstLaunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self getAllShows];
    
}


#pragma mark- WEBSERVICE METHODS
-(void)getAllPlaylist
{
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    //http://54.200.186.222/app/services/playlist/fetchplaylistbylocation
    //    Paramter : utp_loc_id [Compulsory] playlist_distance
    //    distance_type (1 => KM, 2 => Miles)
    //    month_range
    
    [[AppDelegate initAppdelegate].arrPlayList removeAllObjects];
    NSString *postStr=[NSString stringWithFormat:@"lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",[AppDelegate initAppdelegate].objUser.login_id,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude ,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude ,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTra=[dictresponce valueForKey:@"DATA"] ;
            if(arrTra.count >0){
                [AppDelegate initAppdelegate].arrPlayList=[[NSMutableArray alloc]init];
                for(int i=0;i<arrTra.count;i++)
                {
                    NSMutableDictionary *dict=[arrTra objectAtIndex:i];
                    [[AppDelegate initAppdelegate].arrPlayList addObject:[PlayList initWithResponceDict:dict]];
                    
                    if([[dict valueForKey:@"play_id"]isEqualToString:@"local_playlist"]){
                        if(self.isFavSideMenu)
                        {
                            
                        }else if(self.isFavShowSideMenu){
                        }else{
                            
                            
                            PlayList *obbjs=[PlayList initWithResponceDict:dict];
                            lblPlayTitle.text=obbjs.playName;
                            
                            [AppDelegate initAppdelegate].objPlaylist=obbjs;
                            
                            
                            [self getPlayLisTDetail:obbjs.play_id];
                            objCurr=obbjs;
                        }
                    }
                    
                    
                    
                    
                    [tblView reloadData];
                }
                
            }
            [tblView reloadData];
            STOPLOADING()
            
        }else{
            //            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
}
-(void)refreshPlaylist
{
    if(self.isFavSideMenu){
        [self getMyFavourites];
    }
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    //http://54.200.186.222/app/services/playlist/fetchplaylistbylocation
    //    Paramter : utp_loc_id [Compulsory] playlist_distance
    //    distance_type (1 => KM, 2 => Miles)
    //    month_range
    
    [[AppDelegate initAppdelegate].arrPlayList removeAllObjects];
    NSString *postStr=[NSString stringWithFormat:@"lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",[AppDelegate initAppdelegate].objUser.login_id,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude ,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude ,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTra=[dictresponce valueForKey:@"DATA"] ;
            if(arrTra.count >0){
                [AppDelegate initAppdelegate].arrPlayList=[[NSMutableArray alloc]init];
                for(int i=0;i<arrTra.count;i++)
                {
                    NSMutableDictionary *dict=[arrTra objectAtIndex:i];
                    [[AppDelegate initAppdelegate].arrPlayList addObject:[PlayList initWithResponceDict:dict]];
                    [tblView reloadData];
                }
                
            }
            if([objCurr.play_id isEqualToString:@"fav_playlist"])
            {
                //NSLog(@"FAV");
                [self getPlayLisTDetail:objCurr.play_id];
            }
            
            [tblView reloadData];
            STOPLOADING()
            
        }else{
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
}


-(void)getCurrDetail
{
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",objCurr.play_id,[AppDelegate initAppdelegate].objUser.locId,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            
            if(arrShows.count >0){
                arrShow=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows)
                {
                    [arrShow addObject:[Show initWithResponceDict:dict]];
                }
                
                [tblView reloadData];
            }
        }
    }];
    
    
    
    return;
    
    /*  ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,GET_PLAY_LIST]]];
     [request setRequestMethod:@"POST"];
     [request setPostValue:objCurr.play_id forKey:@"utp_play_id"];
     //    [request setPostValue:[AppDelegate initAppdelegate].objUser.locId forKey:@"utp_loc_id"];
     [request setPostValue:[AppDelegate initAppdelegate].objUser.user_id forKey:@"lgn_id"];
     [request setPostValue:[NSString stringWithFormat:@"%f", [AppDelegate initAppdelegate].locationManager.location.coordinate.latitude] forKey:@"latitude"];
     [request setPostValue:[NSString stringWithFormat:@"%f", [AppDelegate initAppdelegate].locationManager.location.coordinate.longitude] forKey:@"longitude"];
     [request setPostValue:@"2" forKey:@"distance_type"];
     [request setPostValue:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"]] forKey:@"playlist_distance"];
     [request setPostValue:[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]] forKey:@"month_range"];
     
     
     [request setDelegate:self];
     [request startAsynchronous];
     [request setCompletionBlock:^{
     
     
     NSError *error1;
     
     NSMutableDictionary * dictresponce = [NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:&error1];
     
     
     //NSLog(@"%@",dictresponce);
     if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
     NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
     
     if(arrShows.count >0){
     arrShow=[[NSMutableArray alloc]init];
     for(NSMutableDictionary *dict in arrShows)
     {
     [arrShow addObject:[Show initWithResponceDict:dict]];
     }
     
     [tblView reloadData];
     }
     }
     STOPLOADING()
     }];
     
     [request setFailedBlock:^{
     
     }];
     
     [request start];
     
     */
}
-(void)getAllShows
{
    //    if(delegate.arrLoaction.count >0){
    //        for(Location *objL in delegate.arrLoaction){
    //            if([objL.loc_id isEqualToString:delegate.objUser.locId]){
    //                delegate.objUser.objLocation=objL;
    //                break;
    //            }
    //        }
    //    }
    
    
    [self getAllPlaylist];
    return;
    
}

-(void)getPlayLisTDetail:(NSString *)idPlayList
{
    
    
    //    [arrTracks removeAllObjects];
    //    [arrShow removeAllObjects];
    [tblView reloadData];
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",idPlayList,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            if(arrTracs.count >0){
                arrTracks=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrTracs)
                {
                    [arrTracks addObject:[Track initWithResponceDict:dict]];
                }
                delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
                delegate.index_current_song=0;
                
                [delegate playAudioWithIndex:delegate.index_current_song];
                [delegate pausePlayer];
                if(isPlay==true){
                    [delegate playAudioWithIndex:delegate.index_current_song];
                    isPlay=false;
                }
            }else{
                // arrTracks=[[NSMutableArray alloc]init];
                [arrTracks removeAllObjects];
                if([idPlayList isEqualToString:@"local_playlist"]){
                    
                    [self showNotificationView:@"Change your playlist" :[UIImage imageNamed:@"info.png"]];
                    
                }
            }
            if(arrShows.count >0){
                arrShow=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows)
                {
                    [arrShow addObject:[Show initWithResponceDict:dict]];
                }
            }else{
                [arrShow removeAllObjects];
                //                            [Utility showAlrtWithMessage:@"No shows returned. Please try different settings"];
                
                [self showNotificationView:@"No shows returned. Please try different settings" :[UIImage imageNamed:@"info.png"]];
            }
            
            [tblView reloadData];
        }else{
            //            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
    return;
    /*  ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,GET_PLAY_LIST]]];
     [request setRequestMethod:@"POST"];
     [request setPostValue:idPlayList forKey:@"utp_play_id"];
     //    [request setPostValue:[AppDelegate initAppdelegate].objUser.locId forKey:@"utp_loc_id"];
     [request setPostValue:[AppDelegate initAppdelegate].objUser.user_id forKey:@"lgn_id"];
     [request setPostValue:[NSString stringWithFormat:@"%f", [AppDelegate initAppdelegate].locationManager.location.coordinate.latitude] forKey:@"latitude"];
     [request setPostValue:[NSString stringWithFormat:@"%f", [AppDelegate initAppdelegate].locationManager.location.coordinate.longitude] forKey:@"longitude"];
     [request setPostValue:@"2" forKey:@"distance_type"];
     [request setPostValue:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"]] forKey:@"playlist_distance"];
     [request setPostValue:[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]] forKey:@"month_range"];
     
     [request setDelegate:self];
     [request startAsynchronous];
     [request setCompletionBlock:^{
     
     //NSLog(@"Responce : %@",[[NSString alloc]initWithData:request.responseData encoding:NSUTF8StringEncoding]);
     
     NSError *error1;
     
     NSMutableDictionary * dictresponce = [NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:&error1];
     if(error1){
     //NSLog(@" Error %@",error1.localizedDescription);
     //NSLog(@" Error %@",error1);
     }
     //NSLog(@"Play list detail %@",dictresponce);
     if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
     NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
     NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
     if(arrTracs.count >0){
     arrTracks=[[NSMutableArray alloc]init];
     for(NSMutableDictionary *dict in arrTracs)
     {
     [arrTracks addObject:[Track initWithResponceDict:dict]];
     }
     delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
     delegate.index_current_song=0;
     
     [delegate playAudioWithIndex:delegate.index_current_song];
     [delegate pausePlayer];
     if(isPlay==true){
     [delegate playAudioWithIndex:delegate.index_current_song];
     isPlay=false;
     }
     }else{
     // arrTracks=[[NSMutableArray alloc]init];
     [arrTracks removeAllObjects];
     if([idPlayList isEqualToString:@"local_playlist"]){
     
     [self showNotificationView:@"Change your playlist" :[UIImage imageNamed:@"info.png"]];
     
     }
     }
     if(arrShows.count >0){
     arrShow=[[NSMutableArray alloc]init];
     for(NSMutableDictionary *dict in arrShows)
     {
     [arrShow addObject:[Show initWithResponceDict:dict]];
     }
     }else{
     [arrShow removeAllObjects];
     [self showNotificationView:@"No shows returned. Please try different settings" :[UIImage imageNamed:@"info.png"]];
     }
     
     [tblView reloadData];
     }else{
     //            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
     [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
     
     }
     STOPLOADING()
     }];
     
     [request setFailedBlock:^{
     
     //NSLog(@"%@", request.error);
     }];
     
     [request start];*/
    
}
- (void)requestStarted:(ASIHTTPRequest *)request;
{
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
}
- (void)request:(ASIHTTPRequest *)request didReceiveResponseHeaders:(NSDictionary *)responseHeaders;
{
    //NSLog(@"%@",responseHeaders);
}
- (void)requestFinished:(ASIHTTPRequest *)request;
{
    
}
- (void)requestFailed:(ASIHTTPRequest *)request;
{
    STOPLOADING()
}


-(void)changeSelection:(PlayList *)objPlayl;
{
    [AppDelegate initAppdelegate].objPlaylist=objPlayl;
    [self getPlayLisTDetail:objPlayl.play_id];
    lblPlayTitle.text=objPlayl.playName;
    
}


-(void)changePlyTitle:(PlayList *)objPlayl withArrTr:(NSMutableArray *)arrTrac withShow:(NSMutableArray *)arrsh
{
    lblPlayTitle.text=objPlayl.playName;
    [AppDelegate initAppdelegate].objPlaylist=objPlayl;
    objCurr=objPlayl;
    arrShow=arrsh;
    arrTracks=arrTrac;
    isHidden=true;
    self.isFavSideMenu=false;
    //    self.isFavShowSideMenu=false;
    //    if([objPlayl.play_id isEqualToString:@"local_playlist"]){
    Tag=0;
    bgblurView.hidden=true;;
    
    //    }else{
    //        Tag=2;
    //        bgblurView.hidden=false;
    //
    //    }
    if([objPlayl.play_id isEqualToString:@"fav_playlist"]){
        isFav=true;
    }else{
        isFav=false;
    }
    
    [tblView reloadData];
    
}
-(void)showMilesSelectionSheet
{
    
    CGRect frame = btnClose.frame;
    frame.origin = CGPointMake(frame.origin.x, view_Miles.frame.origin.y-50);
    btnClose.frame = frame;
    
    
    view_Miles.hidden=false;
    monthSelectionView.hidden=true;
    
    [view_monthSelection setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.65]];
    
    [view_Miles setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self                                                                                           action:@selector(handleTapFromView:)];
    [view_monthSelection addGestureRecognizer:tapGestureRecognizer];
    tapGestureRecognizer.delegate = self;
    [self slideIn];
    
}
-(void)showMonthSelectionSheet
{
    CGRect frame = btnClose.frame;
    frame.origin = CGPointMake(frame.origin.x, monthSelectionView.frame.origin.y-50);
    btnClose.frame = frame;
    
    view_Miles.hidden=true;
    monthSelectionView.hidden=false;
    [view_monthSelection setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.65]];
    
    [monthSelectionView setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(handleTapFromView:)];
    [view_monthSelection addGestureRecognizer:tapGestureRecognizer];
    tapGestureRecognizer.delegate = self;
    [self slideIn];
    
    
}


-(IBAction)selectMonth:(UIButton *)sender;{
    
    [[NSUserDefaults standardUserDefaults]setInteger:sender.tag forKey:@"Month"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [month1 setBackgroundColor:[UIColor clearColor]];
    [month2 setBackgroundColor:[UIColor clearColor]];[month3 setBackgroundColor:[UIColor clearColor]];[month4 setBackgroundColor:[UIColor clearColor]];[month5 setBackgroundColor:[UIColor clearColor]];[month6 setBackgroundColor:[UIColor clearColor]];
    [sender setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    [self setMonthButton];
    [self slideOut];
    //    [self getAllPlaylist];
    [self getPlayLisTDetail:objCurr.play_id];
    
}

- (IBAction)hideMonthView:(id)sender {
    [self slideOut];
}
#pragma mark - Custom ActionSheet Methods

- (void)slideOut {
    [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
    
    // Set delegate and selector to remove from superview when animation completes
    // Move this view to bottom of superview
    CGRect frame = view_monthSelection.frame;
    frame.size.height=self.view.frame.size.height;
    frame.origin = CGPointMake(0.0, self.view.bounds.size.height);
    view_monthSelection.frame = frame;
    [UIView commitAnimations];
    [AppDelegate initAppdelegate].playerView.hidden=false;
}



- (void)slideIn {
    
    [btnClose.layer setCornerRadius:btnClose.frame.size.width/2];
    [btnClose.layer setBorderColor:NAVNBAR_COLOR.CGColor];
    [btnClose.layer setBorderWidth:1.0];
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    // (PranaviOS): Put in for iPhone 6 / 6 Plus Refinement
    // set initial location at bottom of view
    
    CGRect frame = view_monthSelection.frame;
    frame.size.height=self.view.frame.size.height;
    // set up an animation for the transition between the views
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromTop];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    self.view.alpha=1.0f;
    frame.origin = CGPointMake(0.0, 0);
    view_monthSelection.frame = frame;
    [[view_monthSelection layer] addAnimation:animation forKey:@"TransitionToActionSheet"];
    
}


#pragma mark - Tap Gesture Recognizer Method

- (void) handleTapFromView: (UITapGestureRecognizer *)recognizer
{
    [self slideOut];
}

-(void)setMonthButton
{
    
    NSInteger mont=[[NSUserDefaults standardUserDefaults]integerForKey:@"Month"];
    
    if(mont==1){
        [month1 setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    }
    
    if(mont==2){
        [month2 setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    }
    
    if(mont==3){
        [month3 setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    }
    
    if(mont==4){
        [month4 setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    }
    
    if(mont==5){
        [month5 setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    }
    
    if(mont==6){
        [month6 setBackgroundColor:[UIColor colorWithRed:15.0/255.0 green:176.0/255 blue:212.0/255 alpha:0.8]];
    }
    
}
- (IBAction)changeMiles:(UISlider *)sender {
    
    [[NSUserDefaults standardUserDefaults]setFloat:sender.value forKey:@"Distance"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    lblMiles.text=[NSString stringWithFormat:@"%.0f",sender.value];
    
    
}

- (IBAction)showShareButton:(id)sender {
    if(isShown){
        [shareButton setImage:[UIImage imageNamed:@"shareIconInactive.png"] forState:UIControlStateNormal];
        //        [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
        
        
        // Set delegate and selector to remove from superview when animation completes
        // Move this view to bottom of superview
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        CGRect frame = shareView.frame;
        frame.origin = CGPointMake(0.0,-self.view.bounds.size.height);
        shareView.frame = frame;
        [UIView commitAnimations];
        isShown=false;
        
    }else{
        [shareButton setImage:[UIImage imageNamed:@"shareIconActive.png"] forState:UIControlStateNormal];
        [shareView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
        [view_mainShare setBackgroundColor:[UIColor colorWithRed:15.0/255 green:176.0/255 blue:212.0/255 alpha:0.30]];
        CGRect frame = shareView.frame;
        
        // set up an animation for the transition between the views
        //        CATransition *animation = [CATransition animation];
        //        [animation setDuration:0.5];
        //        [animation setType:kCATransitionPush];
        //        [animation setSubtype:kCATransitionFromTop];
        //        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        
        
        self.view.alpha=1.0f;
        frame.origin = CGPointMake(0.0, 0);
        shareView.frame = frame;
        //        [[shareView layer] addAnimation:animation forKey:@"Slidein"];
        [UIView commitAnimations];
        
        isShown=true;
        
    }
    
}
- (IBAction)showAppLink:(id)sender;
{
    
}

- (IBAction)shareOnTwitter:(id)sender {
    [self showShareButton:nil];
    //    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    //    {
    
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
    [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
    
    [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
    
    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
        switch(result) {
                //  This means the user cancelled without sending the Tweet
            case SLComposeViewControllerResultCancelled:
                break;
                //  This means the user hit 'Send'
            case SLComposeViewControllerResultDone:
                break;
        }
        
        //  dismiss the Tweet Sheet
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:NO completion:^{
                //NSLog(@"Tweet Sheet has been dismissed.");
            }];
        });
    };
    
    //  Set the initial body of the Tweet
    
    [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
    
    //  Adds an image to the Tweet.  For demo purposes, assume we have an
    //  image named 'larry.png' that we wish to attach
    if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
        //NSLog(@"Unable to add the image!");
    }
    
    //  Add an URL to the Tweet.  You can add multiple URLs.
    if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
        //NSLog(@"Unable to add the URL!");
    }
    
    //  Presents the Tweet Sheet to the user
    [self presentViewController:tweetSheet animated:YES completion:nil];
    //    }
    
}

- (IBAction)shareOnFacebook:(id)sender {
    [self showShareButton:nil];
    [AppDelegate initAppdelegate].playerView.hidden=true;
    //    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    //    {
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
    [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
    
    [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
    
    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
        switch(result) {
                //  This means the user cancelled without sending the Tweet
            case SLComposeViewControllerResultCancelled:
                break;
                //  This means the user hit 'Send'
            case SLComposeViewControllerResultDone:
                break;
        }
        
        //  dismiss the Tweet Sheet
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppDelegate initAppdelegate].playerView.hidden=false;
            [self dismissViewControllerAnimated:NO completion:^{
                //NSLog(@"Tweet Sheet has been dismissed.");
            }];
        });
    };
    
    //  Set the initial body of the Tweet
    [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
    
    //  Adds an image to the Tweet.  For demo purposes, assume we have an
    //  image named 'larry.png' that we wish to attach
    if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
        //NSLog(@"Unable to add the image!");
    }
    
    //  Add an URL to the Tweet.  You can add multiple URLs.
    if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
        //NSLog(@"Unable to add the URL!");
    }
    
    //  Presents the Tweet Sheet to the user
    [self presentViewController:tweetSheet animated:YES completion:nil];
    //    }
    
}
- (IBAction)callSliderChange:(id)sender {
    [self slideOut];
    [self getPlayLisTDetail:objCurr.play_id];
    //    [self getAllPlaylist];
}


#pragma mark-
#pragma mark- SIDE VIEW FAVOURITE METHID
-(void)getMyFavourites
{
    [arrTracks removeAllObjects];
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"usrplay_lgn_id=%@",[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:GET_MY_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
            if(arr.count >0){
                arrTracks=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arr)
                {
                    [arrTracks addObject:[Track initWithResponceDict:dict]];
                }
                delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
                delegate.index_current_song=0;
                [delegate playAudioWithIndex:delegate.index_current_song];
                [delegate pausePlayer];
                delegate.isMyFav=true;
                lblPlayTitle.text=@"Favorites Playlist";
            }
            [tblView reloadData];
            lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
            
            
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
}


#pragma mark- FAV SHOW SIDE MENU
-(void)purchaseTicClicked:(UIButton *)btn{
    Show *objSh=[arrTic objectAtIndex:btn.tag];
    
    PaymentPageVC *objP=[[PaymentPageVC alloc]initWithNibName:@"PaymentPageVC" bundle:nil];
    objP.ticketId=objSh.tickId;
    [self.navigationController pushViewController:objP animated:true];
    
    
}

-(IBAction)showTicketInfo:(UIButton *)sender
{
    Show *objSh=[arrCurrent objectAtIndex:sender.tag];
//    NSLog(@"%@",[arrCurrent valueForKey:@"Show"]);
    //    TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
    //    objT.objShow=objSh;
    //    [self.navigationController pushViewController:objT animated:true];
    //    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    
    
    //    Show *objSh=[arrShow objectAtIndex:btn.tag];
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"show_id=%@",objSh.show_id];
    [Utility executePOSTRequestwithServicetype:DETAIL_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableDictionary *dictRes=[dictresponce valueForKey:@"DATA"];
            PurchaseTicket *objP=[[PurchaseTicket alloc]initWithNibName:@"PurchaseTicket" bundle:nil];
            objP.objShow=objSh;
            objP.arrTic=[dictRes valueForKey:@"Tickets"];
            [self.navigationController pushViewController:objP animated:true];
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    
    
}

-(void)deleteShowClicked:(UIButton *)btn
{
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"msf_show_id=%@&msf_lgn_id=%@",btn.accessibilityLabel,[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:DELETE_SHOW_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [arrCurrent removeObjectAtIndex:btn.tag];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNotification"
             object:self];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNoti"
             object:self];
            [tblView reloadData];
            //            lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
            
            
        }else{
            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
        }
    }];
    
}

-(void)getMyFavouritesShows
{
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"msf_lgn_id=%@",[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:GET_MY_FAV_SHOW withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
            if(arr.count >0){
                
                NSMutableDictionary *dictER=[dictresponce valueForKey:@"DATA"];
                
                NSMutableArray *arrtempt=[dictER valueForKey:@"currentArray"];
                NSMutableArray *arrtempt1=[dictER valueForKey:@"pastArray"];
                NSMutableArray *arrtem=[dictER valueForKey:@"ticketArray"];
                
                
                
                if(arrtempt.count >0){
                    arrCurrent=[[NSMutableArray alloc]init];
                    for(NSMutableDictionary *dict in arrtempt)
                    {
                        Show *objs=[Show initWithResponceDict:dict];
                        [arrCurrent addObject:objs];
                    }
                }
                if(arrtempt1.count >0){
                    arrPast=[[NSMutableArray alloc]init];
                    
                    for(NSMutableDictionary *dict in arrtempt1)
                    {
                        Show *objs=[Show initWithResponceDict:dict];
                        [arrPast addObject:objs];
                    }
                }
                
                if(arrtem.count >0){
                    arrTic=[[NSMutableArray alloc]init];
                    
                    for(NSMutableDictionary *dict in arrtem)
                    {
                        Show *objs=[Show initWithResponceDict:dict];
                        
                        [arrTic addObject:objs];
                    }
                }
                
            }
            [tblView reloadData];
            //            lblHeader.text=[NSString stringWithFormat:@"%lu   Shows in my favorites",(unsigned long)arrShow.count];
            
            
        }else{
            //            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
}


-(void)backButtonFromtrackList{
    Tag=0;
    bgblurView.hidden=true;
    [tblView reloadData];
}

@end
