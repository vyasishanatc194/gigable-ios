//
//  PlaylistCell.h
//  Gigable
//
//  Created by AppDev03 on 1/16/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaylistCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayList;
@property (strong, nonatomic) IBOutlet UILabel *lblPlayListName;
@property (strong, nonatomic) IBOutlet UILabel *lblTrackCount;


@end
