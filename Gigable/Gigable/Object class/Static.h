//#define NAVNBAR_COLOR [UIColor colorWithRed:29.0/255 green:207.0/255 blue:246.0/255 alpha:1.0]

//#define NAVNBAR_COLOR_LIGHT [UIColor colorWithRed:29.0/255 green:207.0/255 blue:246.0/255 alpha:0.6]

#define NAVNBAR_COLOR [UIColor colorWithRed:31.0/255 green:199.0/255 blue:237.0/255 alpha:1.0]

#define NAVNBAR_COLOR_LIGHT [UIColor colorWithRed:31.0/255 green:199.0/255 blue:237.0/255 alpha:0.8]
#define CELL_SELECTTION_COLOR [UIColor colorWithRed:29.0/255 green:207.0/255 blue:246.0/255 alpha:0.5]

#define FONTNAME  @"GothamBook"
#define FONT_LIGHT  @"GothamBook"
#define FONTNAMEBOLD @"GothamBold"
#define FONTCOLOR @"555555" //@"3f444b"
#define TBLVIEWCOLOR @"f5f5f5"
#define TBLHEADER_COLOR @"e5e5e5"
#define SEP_COLOR @"dddddd"
#define SELECTION_COLOR @"d9d9d9"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height

#define FONTWIDTH ISIPHONE ? 20 :30
#define SATSANG_COL [UIColor colorWithRed:96.0/255 green:55.0/255 blue:24.0/255 alpha:1.0]
#define VID_COL [UIColor colorWithRed:241.0/255 green:32.0/255 blue:45.0/255 alpha:1.0]
#define EVENT_COL [UIColor colorWithRed:59.0/255 green:125.0/255 blue:31.0/255 alpha:1.0]

// SlideShow
/* Live for client

 [28/04/16, 12:45:11 PM] Ishan Vyas: Gigable NEw Staging server :-
[28/04/16, 12:45:12 PM] Ishan Vyas: http://52.34.59.210/
 
 */

#define MAIN_URL @"http://52.34.59.210/app/services"  //Test
//#define MAIN_URL @"https://www.gigable.io/app/services" // Live

#define USER_SIGNUP @"/user/signup"
#define GET_LOCATION @"/location/getalllocation"
#define USER_LOGIN @"/user/login"
#define FORGOT_PASSWORD @"/user/forgotpassword"
#define GET_PLAY_LIST @"/playlist/index"
#define MAKE_FAV @"/playlist/favourite"
#define GET_MY_FAV @"/playlist/fetchuserfavourite"
#define DETAIL_PLAYLIST @"/playlist/showdetails"
#define VERIFY_CODE @"/user/verifycode"
#define RESET_PWD @"/user/newpassword"
#define GET_PLAYLIST @"/playlist/fetchplaylistbylocation"
#define GET_PROFILE @"/user/profile"
#define CHANGE_PWD @"/user/changepassword"
#define GOOGLELOGIN @"/user/googlelogin"
#define MARK_SHOW_FAV @"/show/addfavouriteshow"
#define GET_MY_FAV_SHOW @"/show/listfavouriteshowdev"//
#define DELETE_SHOW_FAV @"/show/deletefavouriteshow"
#define GET_TICKET_FROM_TRACK @"/ticket/getticket"
#define PURCHASE_TIC @"/ticket/bookticket"
#define GET_ARTIST_DETAIL @"/playlist/artistdetails"
#define MAKE_PAYMENTPROCESS @"/ticket/paymentprocess"
#define GET_ORDER_DETAIL @"/ticket/orderdetails"

#define FETCH_PLAYLIST_BY_LOCATION @"/playlist/fetchplaylistbylocation"
#define FETCH_FUTURE_CONCERTS @"/playlist/futureplaylist"
#define FETCH_PLAYLIST_BY_INDEX @"/playlist/index"
#define GET_ARTIST_DETAIL @"/playlist/artistdetails"

#define KEY_LOGINDICT @"LOGININFO"
#define KEY_Social @"SocialSignup"


#define Latitude @"LATITUDE"
#define Logitude @"LONGITUDE"
#define LocationNameDefault @"LOCATIONANAME"



#define CHECKFBLOGIN @"/user/facebooklogin"
