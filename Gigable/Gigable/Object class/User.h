//
//  User.h
//  Gigable
//
//  Created by Kalpit Gajera on 02/02/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Location : NSObject

@property (strong,nonatomic)NSString *loc_id,*loc_latitude,*loc_longitude,*loc_name;
+(Location *)initWithResponceDict:(NSMutableDictionary *)dict;
@end

@interface User : NSObject
@property (strong,nonatomic)NSString *login_id,*user_id,*gender,*lgn_email,*locId,*phone,*userName;
@property (strong,nonatomic)Location *objLocation;
+(User *)initWithResponceDict:(NSMutableDictionary *)dict;

@end



@interface Show : NSObject
@property (strong,nonatomic)NSString *show_id,*show_title,*loc_id,*loc_name,*play_name,*totalshow,*show_desc,*show_date,*show_Full_Date,*showTime,*tickId,*art_image,*art_name,*ven_name,*show_updateddate;
@property (assign,nonatomic)BOOL isFav,isPast,isPurchased;

+(Show *)initWithResponceDict:(NSMutableDictionary *)dict;

@end

@interface Track : NSObject
@property (strong,nonatomic)NSString *art_id,*art_name,*gnr_id,*loc_id,*loc_name,*play_name,*totaltrack,*trk_audio,*trk_title,*id_track,*gnr_name,*createdDate,*img_track,*albumname,*descr,*play_id;
 @property (assign,nonatomic)BOOL isFav,ticket_status;

+(Track *)initWithResponceDict:(NSMutableDictionary *)dict;

@end

@interface PlayList : NSObject

@property (strong,nonatomic)NSString *play_id,*playName,*month_range,*play_desc,*playlist_distance,*totaltrack,*artist,*distance_type;
+(PlayList *)initWithResponceDict:(NSMutableDictionary *)dict;
@end

