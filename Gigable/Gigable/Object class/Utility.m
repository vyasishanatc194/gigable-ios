//
//  Utility.m
//  Drawing&Note
//
//  Created by Macmini-17 on 15/02/13.
//  Copyright (c) 2013 Stefano Acerbetti. All rights reserved.
//

#import "Utility.h"
#import "Static.h"
#import "JTAlertView.h"

@implementation Utility
@synthesize PlayingMode;
+(void)setNavigationBar:(UINavigationController *)navController
{
    
    navController.navigationBar.barTintColor=NAVNBAR_COLOR_LIGHT;
    [navController.navigationBar setTintColor:[UIColor whiteColor]];
//    navController.navigationBar.translucent = NO;
    [navController.navigationBar setTitleTextAttributes:@{
 NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:FONTNAME size:18.0]
}];
    
//    [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
//    [[UINavigationBar appearance]setBarTintColor:NAVNBAR_COLOR];
//    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
//    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor whiteColor]];
    

  }

+(NSArray*)setLeftBarButtonItemWithTarget:(id)target withAction:(SEL)action andWithImage:(NSString *)imageName;{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:imageName];
    backBtn.frame = CGRectMake(0, 0, 50,44);
    [backBtn setImage:backBtnImage forState:UIControlStateNormal];
    [backBtn setBackgroundColor:NAVNBAR_COLOR];
    [backBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6
    return  [NSArray arrayWithObjects:negativeSpacer, backButton/*this will be the button which u actually need*/, nil];
}


+ (UIColor *) getColor: (NSString *) hexColor
{
	unsigned int red, green, blue;
	NSRange range;
	range.length = 2;
	range.location = 0;
	[[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
	range.location = 2;
	[[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
	range.location = 4;
	[[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
	
	return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green/255.0f) blue:(float)(blue/255.0f) alpha:1.0f];
}


+(BOOL) isInAppPurchased{
   // BOOL isFeaturePurchased=[MKStoreManager isFeaturePurchased:IN_APP_KEY_FREE_TO_PRO];
  /*   BOOL isFeaturePurchased=[MKStoreManager isFeaturePurchased:IN_APP_KEY_FILEMINI_FREE_TO_PR0];
           return isFeaturePurchased;*/
    return 0;
}


+(NSString *)createDirectoryWithFolderName:(NSString *) strFolderName{
    
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *strFolderPath=[[Utility documentDirectory] stringByAppendingPathComponent:strFolderName];
    [fm createDirectoryAtPath:strFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    return strFolderPath;
}
+(NSString *)documentDirectory{
    
    NSArray* dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                            NSUserDomainMask, YES);
    
    return  [dirPaths objectAtIndex:0];
}
+(void)setTheuserDefaultsValue{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"NoteCount"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // This is the first launch ever
    }
}
+(float)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}
+(void)formatTheFileNameLabel:(UILabel *)lbl{
    lbl.backgroundColor=[UIColor clearColor];
    lbl.numberOfLines=1;
    [lbl setAdjustsFontSizeToFitWidth:TRUE];
    lbl.textColor=[UIColor darkGrayColor];
    lbl.font=[UIFont fontWithName:FONTNAME size:15.0];
    
}
+(void)formatDetailLabel:(UILabel *)lbl{
    lbl.backgroundColor=[UIColor clearColor];
    lbl.numberOfLines=1;
    [lbl setAdjustsFontSizeToFitWidth:TRUE];
     lbl.textColor=[UIColor grayColor];
    lbl.font=[UIFont fontWithName:FONT_LIGHT size:13.0];
    
}


+(NSString *)stringForDate:(NSDate *)date withFormater:(NSString *)strFormater{
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:strFormater];
    NSString *strdate=[[formatter stringFromDate:date]lowercaseString];
    return strdate;
}

+(void)showAlrtWithTitle:(NSString *)strTitle andWithMessage:(NSString *)strMessage{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:strTitle
                          message:strMessage
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

+(void)showAlrtWithMessage:(NSString *)strMessage{
    
    
    [[[iToast makeText:strMessage]setGravity:iToastGravityCenter] show];
    
   /* JTAlertView *alertView = [[JTAlertView alloc] initWithTitle:strMessage andImage:[UIImage imageNamed:@"bg_app.png"]];
    alertView.size = CGSizeMake(280,50);
    alertView.popAnimation = true;
    alertView.parallaxEffect =true;
    alertView.font=[UIFont fontWithName:FONTNAME size:15.0];
    [alertView hideWithDelay:1.5 animated:true];
    [alertView showInSuperview:[[UIApplication sharedApplication] keyWindow] withCompletion:nil animated:true];*/

//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:strMessage
//                          message:nil
//                          delegate: nil
//                          cancelButtonTitle:@"OK"
//                          otherButtonTitles:nil];
//    [alert show];
}
+(void)showNetworkError{
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Network Error"
                          message:@"Please check your internet connection"
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}


+(void)setTheUpdatedUsersValue;{
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"NewUpadate3.2"])
    {
        
        
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"Repeat"];
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"shuffle"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NewUpadate3.2"];
        
        [[NSUserDefaults standardUserDefaults]setValue:@"3 Seconds" forKey:@"slideshowTime"];
        [[NSUserDefaults standardUserDefaults]setValue:@"rippleEffect" forKey:@"Transition"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // This is the first launch ever
    }
    
    
}
/*
 +(NSString*)getUDIDecode{
 int sum=0;
 NSString *uid = [[UIDevice currentDevice] uniqueIdentifier];
 uid = [uid stringByReplacingOccurrencesOfString:@"-" withString:@""];
 for (int i=0; i<[uid length]; i++) {
 unichar ch = [uid characterAtIndex:i];
 sum=sum+ch;
 }
 //NSLog(@"%d",sum);
 return [NSString stringWithFormat:@"%d",sum];
 
 }
 */
+(NSString *)fileSize:(CGFloat )fileSize{
    
    fileSize =fileSize/1048576;
    
    NSString *strFileSize;
    if (fileSize>=1.000)
    {
        strFileSize=[NSString stringWithFormat:@"%0.1f MB",fileSize];
    }
    else
    {
        fileSize=fileSize*1024;
        strFileSize=[NSString stringWithFormat:@"%0.0f KB",fileSize];
    }
    return strFileSize;
}

+(float)getOsVersion;
{
    float version = [[[UIDevice currentDevice] systemVersion]floatValue];
    return version;

}

-(void)formatTheFileNameLabel:(UILabel *)lbl{
    lbl.backgroundColor=[UIColor clearColor];
    lbl.numberOfLines=1;
    [lbl setAdjustsFontSizeToFitWidth:TRUE];
    lbl.textColor=[Utility getColor:FONTCOLOR];
    lbl.font=[UIFont fontWithName:FONTNAME size:18.0];
    
}
-(void)formatDetailFileName:(UILabel *)lbl{
    lbl.backgroundColor=[UIColor clearColor];
    lbl.font=[UIFont fontWithName:FONTNAMEBOLD size:12.0];
    lbl.textColor=[Utility getColor:@"626262"];
}

-(NSString *)getTheFormatDate:(NSDate *)date{
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateStyle:1040];
    NSString *strdate=[formatter stringFromDate:date];
    return  [NSString stringWithFormat:@"Modified:%@",strdate];
    
}
+(UIImage *)getFileIcon:(NSString*)filename{
    if(filename && [filename length]>0)
    {     //   //NSLog(@"%@",filename);
        NSString *strExtnsion=[[filename pathExtension] lowercaseString];
        
        if ([strExtnsion isEqualToString:@"doc"]||[strExtnsion isEqualToString:@"docx"]   || [strExtnsion isEqualToString:@"rtf"]|| [strExtnsion isEqualToString:@"txt"]  ){
            return [UIImage imageNamed:@"doc.png"];
            
        }
            else if ([strExtnsion isEqualToString:@"pptx"] || [strExtnsion isEqualToString:@"ppt"]) {
                return [UIImage imageNamed:@"ppt.png"];
            }
        else if ([strExtnsion isEqualToString:@"pdf"]) {
            return [UIImage imageNamed:@"pdf.png"];
        }
        else if ([strExtnsion isEqualToString:@"rar"] || [strExtnsion isEqualToString:@"cbr"]|| [strExtnsion isEqualToString:@"zip"]) {
            return [UIImage imageNamed:@"zip_file.png"];
        }
        else if ([strExtnsion isEqualToString:@"m4a"] ||
                 [strExtnsion isEqualToString:@"caf"] ||
                 [strExtnsion isEqualToString:@"wav"] ||
                 [strExtnsion isEqualToString:@"mp3"] ||
                 [strExtnsion isEqualToString:@"wave"]||
                 [strExtnsion isEqualToString:@"aac"] ||
                 [strExtnsion isEqualToString:@"aiff"] ||
                 [strExtnsion isEqualToString:@"amr"]) {
            return [UIImage imageNamed:@"music.png"];
            
        }
        else if ([strExtnsion isEqualToString:@"xl"] || [strExtnsion isEqualToString:@"xls"] || [strExtnsion isEqualToString:@"iwork"])
        {
            return [UIImage imageNamed:@"excel.png"];
            
        }
        else if ([strExtnsion isEqualToString:@"jpeg"]|| [strExtnsion isEqualToString:@"jpg"]|| [strExtnsion isEqualToString:@"png"]|| [strExtnsion isEqualToString:@"bmp"]|| [strExtnsion isEqualToString:@"gif"]|| [strExtnsion isEqualToString:@"raw"]|| [strExtnsion isEqualToString:@"tiff"]) {
            return [UIImage imageNamed:@"img.png"];
            
        }
        else if ([strExtnsion isEqualToString:@"mov"] || [strExtnsion isEqualToString:@"avi"] || [strExtnsion isEqualToString:@"3gp"] || [strExtnsion isEqualToString:@"mp4"]) {
            return [UIImage imageNamed:@"mov.png"];
            
        }
        else if ([strExtnsion isEqualToString:@"html"] || [strExtnsion isEqualToString:@"php"] || [strExtnsion isEqualToString:@"htm"] ) {
            return [UIImage imageNamed:@"webpage.png"];
            
        }
        else {
            return [UIImage imageNamed:@"other.png"];
        }
    }else
        return nil;
    
}

+(void)showNofileSelectedAlert
{
    [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"no_file",nil) message:NSLocalizedString(@"select_file",nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok",nil), nil]show];
}
+ (BOOL) connectedToNetwork
{
    
        // Create zero addy
        struct sockaddr_in zeroAddress;
        bzero(&zeroAddress, sizeof(zeroAddress));
        zeroAddress.sin_len = sizeof(zeroAddress);
        zeroAddress.sin_family = AF_INET;
        
        // Recover reachability flags
        SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
        SCNetworkReachabilityFlags flags;
        
        BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
        CFRelease(defaultRouteReachability);
        
        if (!didRetrieveFlags)
        {
            //NSLog(@"Error. Could not recover network reachability flags");
            return NO;
        }
        BOOL isReachable = flags & kSCNetworkFlagsReachable;
        BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
        BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
        NSURL *testURL = [NSURL URLWithString:@"http://www.apple.com/"];
        NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
        NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:self];
        
        return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
}


+(UIImage *)changeImageColorWithImage:(UIImage *)image WithColor:(UIColor *)color
{
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, image.CGImage);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
     return [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation: UIImageOrientationDownMirrored];

}

+(void)executePOSTRequestwithServicetype:(NSString *)serviceType withPostString:(NSString *)postString  withBlock:(void (^)(NSMutableDictionary *dictresponce,NSError *error))block {
    if(![Utility connectedToNetwork]){
        [self showNetworkError];
        block(nil,nil);
        
    }
NSData *postData = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
[request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,serviceType]]];
    //NSLog(@"%@",[NSString stringWithFormat:@"%@%@",MAIN_URL,serviceType]);
[request setHTTPMethod:@"POST"];
[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
[request setTimeoutInterval:20.0];
[request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               NSError *error1;
                               if(data==nil){
                                   block(nil,error);
                               }else{
//                                   NSLog(@"Responce : %@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                   NSMutableDictionary * innerJson = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   
                                   NSMutableDictionary *dict = [innerJson mutableCopy];
                                   NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                                   [dict removeObjectsForKeys:keysForNullValues];
                                   block(dict,error); // Call back the block passed into your method
                               }
                           }];

    
}





+(void)executeRequestwithServicetype:(NSString *)serviceType withDictionary:(NSMutableDictionary *)dict  withBlock:(void (^)(NSMutableDictionary *dictresponce,NSError *error))block {
    if(![Utility connectedToNetwork]){
        [self showNetworkError];
        block(nil,nil);

    }

    NSError *error = Nil;
   
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:&error];
    NSString *str=[[NSString alloc]initWithData:requestData encoding:NSUTF8StringEncoding];
    //NSLog(@"json body: %@",str);
    NSData* responseData = nil;
    NSURL *url1=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",MAIN_URL,serviceType] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"%@",url1);
    
    responseData = [NSMutableData data] ;
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url1];
    NSString *bodydata=[NSString stringWithFormat:@"json=%@",str];
    [request setHTTPMethod:@"POST"];
    
    [request setTimeoutInterval:60.0];
    NSData *req=[NSData dataWithBytes:[bodydata UTF8String] length:[bodydata length]];
    [request setHTTPBody:req];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               NSError *error1;
                               if(data==nil){
                                   block(nil,error);
                               }else{
                                   //NSLog(@"Responce : %@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                   NSMutableDictionary * innerJson = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   block(innerJson,error); // Call back the block passed into your method
                               }
                           }];
    
}
+ (UIImage *)imageFromMovie:(NSURL *)movieURL atTime:(NSTimeInterval)time1 {
    AVAsset *asset = [AVAsset assetWithURL:movieURL];
    
    //  Get thumbnail at the very start of the video
    CMTime thumbnailTime = [asset duration];
    thumbnailTime.value = 0;
    
    //  Get image from the video at the given time
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbnailTime actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return thumbnail;
}
/* Taken from http://stackoverflow.com/questions/1347562/getting-thumbnail-from-a-video-url-or-data-in-iphone-sdk/4330647#4330647
-(void)generateImage
{
    AVURLAsset *asset=[[AVURLAsset alloc] initWithURL:self.url options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform=TRUE;
    [asset release];
    CMTime thumbTime = CMTimeMakeWithSeconds(0,30);
    
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded) {
            //NSLog(@"couldn't generate thumbnail, error:%@", error);
        }
        [button setImage:[UIImage imageWithCGImage:im] forState:UIControlStateNormal];
        thumbImg=[[UIImage imageWithCGImage:im] retain];
        [generator release];
    };
    
    CGSize maxSize = CGSizeMake(320, 180);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
    
}
*/
+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)isStringEmpty:(NSString *)string {
    if([string length] == 0) { //string is empty or nil
        return YES;
    }
    
    if(![[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        //string is all whitespace
        return YES;
    }
    
    return NO;
}

@end
