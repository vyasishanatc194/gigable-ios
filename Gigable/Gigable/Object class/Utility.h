//
//  Utility.h
//  Drawing&Note
//
//  Created by Macmini-17 on 15/02/13.
//  Copyright (c) 2013 Stefano Acerbetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "iToast.h"
typedef enum{
    normalMode =0,
    SlideShowMode =1
} PlayingMode;

typedef void (^onGetSuccess)(BOOL success,NSArray *arrProduct);

@interface Utility : NSObject{
    
}

@property(nonatomic,assign) PlayingMode PlayingMode;
+(void)setNavigationBar:(UINavigationController *)navController;
+ (UIColor *) getColor: (NSString *) hexColor;
+(NSString *)createDirectoryWithFolderName:(NSString *) strFolderName;
+(NSString *)documentDirectory;
+ (float)folderSize:(NSString *)folderPath;
+(void)formatTheFileNameLabel:(UILabel *)lbl;
+(void)setTheUpdatedUsersValue;
//+(NSString*)getUDIDecode;
+(NSString *)fileSize:(CGFloat )fileSize;
+(BOOL) isInAppPurchased;
+(void)showAlrtWithTitle:(NSString *)strTitle andWithMessage:(NSString *)strMessage;
+(NSString *)stringForDate:(NSDate *)date withFormater:(NSString *)strFormater;
+(NSArray*)setLeftBarButtonItemWithTarget:(id)target withAction:(SEL)action andWithImage:(NSString *)imageName;
+(float)getOsVersion;
+(void)formatDetailLabel:(UILabel *)lbl;
+( UIImage *)getFileIcon:(NSString*)filename;
+(void)showNofileSelectedAlert;
+(BOOL) connectedToNetwork;
+(void)showNetworkError;
+(UIImage *)changeImageColorWithImage:(UIImage *)image WithColor:(UIColor *)color;
+(void)showAlrtWithMessage:(NSString *)strMessage;
+(void)executePOSTRequestwithServicetype:(NSString *)serviceType withPostString:(NSString *)postString  withBlock:(void (^)(NSMutableDictionary *dictresponce,NSError *error))block;

+ (void)executeRequestwithServicetype:(NSString *)serviceType withDictionary:(NSMutableDictionary *)dict  withBlock:(void (^)(NSMutableDictionary *dictresponce,NSError *error))block;
+ (UIImage *)imageFromMovie:(NSURL *)movieURL atTime:(NSTimeInterval)time ;
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;
+ (BOOL)isStringEmpty:(NSString *)string;


@end

