//
//  User.m
//  Gigable
//
//  Created by Kalpit Gajera on 02/02/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "User.h"


@implementation Location
@synthesize loc_id,loc_latitude,loc_longitude,loc_name;
+(Location *)initWithResponceDict:(NSMutableDictionary *)dict
{
    Location *objL=[[Location alloc]init];
    objL.loc_id=[dict valueForKey:@"loc_id"];
    objL.loc_latitude=[dict valueForKey:@"loc_latitude"];
    objL.loc_longitude=[dict valueForKey:@"loc_longitude"];
    objL.loc_name=[dict valueForKey:@"loc_name"];
    
    return objL;
}
@end

@implementation User
@synthesize login_id,user_id,gender,lgn_email,locId,phone,userName,objLocation;
+(User *)initWithResponceDict:(NSMutableDictionary *)dict
{
    User *objL=[[User alloc]init];
    objL.login_id=[dict valueForKey:@"lgn_id"];
    objL.user_id=[dict valueForKey:@"lgn_id"];
    objL.gender=[dict valueForKey:@"usr_gender"];
    objL.lgn_email=[dict valueForKey:@"lgn_email"];
    objL.locId=[dict valueForKey:@"usr_loc_id"];
    objL.phone=[dict valueForKey:@"usr_phone"];
    objL.userName=[dict valueForKey:@"usr_name"];

    return objL;

}
@end




@implementation Show
@synthesize show_id,show_title,loc_id,loc_name,play_name,totalshow,show_desc,show_date,show_Full_Date,showTime,isPast,isPurchased,tickId,art_image,art_name,ven_name,show_updateddate;
+(Show *)initWithResponceDict:(NSMutableDictionary *)dict
{
    Show *objL=[[Show alloc]init];
    objL.show_id=[dict valueForKey:@"show_id"];
    objL.show_title=[dict valueForKey:@"show_title"];
    objL.loc_id=[dict valueForKey:@"loc_id"];
    objL.loc_name=[dict valueForKey:@"loc_name"];
    objL.play_name=[dict valueForKey:@"play_name"];
    objL.totalshow=[dict valueForKey:@"totalshow"];
    objL.show_desc=[dict valueForKey:@"show_desc"];
    objL.show_date=[dict valueForKey:@"play_createddate"];
objL.show_Full_Date=[NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"show_date"],[dict valueForKey:@"show_time"]];
    objL.show_date=[dict valueForKey:@"show_time"];
    objL.isFav=[[dict valueForKey:@"msf_show_fav_status"] boolValue];
    objL.isPurchased=[[dict valueForKey:@"flag_for_purchase_ticket"] boolValue];
    objL.isPast=[[dict valueForKey:@"flag_for_past_show"] boolValue];
    objL.tickId=[dict valueForKey:@"utkt_id"];
    objL.art_name=[dict valueForKey:@"art_name"];
    objL.art_image=[dict valueForKey:@"art_image"];
    objL.ven_name=[dict valueForKey:@"ven_name"];
    objL.show_updateddate=[dict valueForKey:@"show_updateddate"];

    return objL;
    
}
@end
@implementation Track
@synthesize art_id,art_name,gnr_id,loc_id,loc_name,play_name,totaltrack,trk_audio,trk_title,id_track,gnr_name,createdDate,img_track,albumname,descr,play_id,ticket_status;
+(Track *)initWithResponceDict:(NSMutableDictionary *)dict
{
    Track *objL=[[Track alloc]init];
    objL.art_id=[dict valueForKey:@"art_id"];

    objL.art_name=[dict valueForKey:@"art_name"];
    objL.gnr_id=[dict valueForKey:@"gnr_id"];
    
    objL.gnr_name=[dict valueForKey:@"gnr_name"];

    objL.loc_id=[dict valueForKey:@"loc_id"];
    objL.loc_name=[dict valueForKey:@"loc_name"];
    objL.play_name=[dict valueForKey:@"play_name"];
    objL.play_id=[dict valueForKey:@"play_id"];

    objL.totaltrack=[dict valueForKey:@"totaltrack"];
    objL.trk_audio=[dict valueForKey:@"trk_audio"];
    objL.trk_title=[dict valueForKey:@"trk_title"];
    objL.id_track=[dict valueForKey:@"utp_trk_id"];
    
    objL.createdDate=[dict valueForKey:@"play_createddate"];
    
    objL.img_track=[dict valueForKey:@"albm_image"];
    objL.albumname=[dict valueForKey:@"albm_name"];
    objL.descr=[dict valueForKey:@"albm_description"];
    objL.isFav=[[dict valueForKey:@"favourite_status"] boolValue];
    objL.ticket_status=[[dict valueForKey:@"ticket_status"] boolValue];

    return objL;
    
}
@end

@implementation PlayList

@synthesize play_id,playName,month_range,play_desc,playlist_distance,totaltrack,artist,distance_type;
+(PlayList *)initWithResponceDict:(NSMutableDictionary *)dict
{
    PlayList *objL=[[PlayList alloc]init];
    objL.play_id=[dict valueForKey:@"play_id"];
    objL.playName=[dict valueForKey:@"play_name"];
    objL.month_range=[dict valueForKey:@"month_range"];
    objL.play_desc=[dict valueForKey:@"play_desc"];
    objL.totaltrack=[dict valueForKey:@"totaltrack"];
    objL.playlist_distance=[NSString stringWithFormat:@"%@",[dict valueForKey:@"playlist_distance"]];
    objL.distance_type=@"Miles Or Streaming";

    NSMutableArray *arrArti=[dict valueForKey:@"artist"];
    NSString *str;
    if(arrArti.count >0){
        for(int i=0;i<arrArti.count;i++){
            
            if(i==0){
                str=[arrArti objectAtIndex:i];
            }else{
                str=[NSString stringWithFormat:@"%@,%@",str,[arrArti objectAtIndex:i]];
            }
        }
    }
    
    objL.artist=str;

    return objL;

}
@end


