//
//  DashboardVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 26/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "ShowsListVC.h"
#import "Static.h"
#import "Utility.h"
#import "DOPDropDownMenu.h"
#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import "MFSideMenu.h"
#import "STKAudioPlayer.h"
#import "STKAutoRecoveringHTTPDataSource.h"
#import "SampleQueueId.h"
#import "TicketInfoVC.h"
#import "AppDelegate.h"
#import "PurchaseTicket.h"

#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import <Social/Social.h>
#import "DashboardVC.h"

#import "PlayListVC.h"
@interface ShowsListVC ()<DOPDropDownMenuDataSource, DOPDropDownMenuDelegate, UITableViewDataSource,SIMenuDelegate>
{
    UIView *playerView;
    AppDelegate *delegate;
    UILabel *lblHeader;
}
@property (nonatomic, strong) SIMenuTable *table;
@property (nonatomic, strong) UIView *menuContainer;
@property (strong,nonatomic)STKAudioPlayer* audioPlayer;


@end

@implementation ShowsListVC
@synthesize isTracks,arrData,isFav,isTick;
@synthesize arrShows;
@synthesize currentShowName;
@synthesize audioPlayer;
@synthesize playdelegate;
- (void)viewDidLoad {
    self.navigationController.navigationBarHidden=true;
    [super viewDidLoad];
    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
    delegate=[AppDelegate initAppdelegate];
    bgblurView.hidden=true;
    lblPlayTitle.text=delegate.objPlaylist.playName;

    if(isTick==true){
        lblTopTitle.text=@"Purchase Ticket";
        arrShow=self.arrShows;

    }else{
        lblTopTitle.text=@"My Shows";

        [self getMyFavouritesShows];
    }
        Tag=1;
    lblTopTitle.hidden=true;
    menuButton.hidden=FALSE;
    arrow.hidden=FALSE;
    lblPlayTitle.hidden=FALSE;
    [backBtn setImage:[UIImage imageNamed:@"sidemenu.png"] forState:UIControlStateNormal];
    
    
    if(Tag==1){
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 50)];
        
        UIButton *btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 0, 20, 50)];
        
        [btn setImage:[UIImage imageNamed:@"Back_track.png"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];

        
        lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(30,0, WIDTH-50, 50)];
        lblHeader.font=[UIFont fontWithName:FONTNAME size:20.0];
        lblHeader.textAlignment=NSTextAlignmentLeft;
        lblHeader.textColor=[UIColor whiteColor];
        if(isTick==true){
            lblTopTitle.text=@"Shows";
            //NSLog(@"Write artist name here");
            lblHeader.text=[NSString stringWithFormat:@"%d shows for this artist",arrShow.count];
            
        }else{
            lblHeader.text=[NSString stringWithFormat:@"My Shows"];
        }
        [view addSubview:lblHeader];
        
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 49, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [view addSubview:lblSep];
        [view setBackgroundColor:[UIColor clearColor]];
        tblView.tableHeaderView=view;
    }


}

-(void)backButton
{
    [AppDelegate initAppdelegate].isPushedShow=false;
    [self.navigationController popViewControllerAnimated:true];

//    [UIView transitionWithView:self.navigationController.view
//                      duration:0.75
//                       options:UIViewAnimationOptionTransitionFlipFromLeft
//                    animations:^{
//                        
//                        [self.navigationController popViewControllerAnimated:true];
//                    }
//                    completion:nil];
    

}
-(void)viewWillAppear:(BOOL)animated{
    [tblView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- TABLEVIEW METHOD
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;    //count of section
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
           return 80;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(Tag==1){
        return arrShow.count;
    }
    
    
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    cell.backgroundColor=[UIColor clearColor];

    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([tblView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tblView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tblView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tblView setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier] ;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
  
    
        Show *objSh=[arrShow objectAtIndex:indexPath.row];
//        cell.textLabel.text=objSh.show_title;
//        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
//        cell.textLabel.textColor=[UIColor whiteColor];
    
    NSString *dateString = objSh.show_Full_Date;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:dateString];
    dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";

    //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);

        UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 15, WIDTH-100, 15)];
        lblDate.font=[UIFont fontWithName:FONTNAME size:13.0];
        [lblDate setText:[dateFormatter stringFromDate:yourDate]];
        lblDate.textColor=[UIColor whiteColor];

        [cell.contentView addSubview:lblDate];
        
        UILabel *lblShowName=[[UILabel alloc]initWithFrame:CGRectMake(5, 30, WIDTH-100, 20)];
        lblShowName.font=[UIFont fontWithName:FONTNAMEBOLD size:15.0];
        lblShowName.textColor=[UIColor whiteColor];
        [lblShowName setText:objSh.show_title];
        
        [cell.contentView addSubview:lblShowName];
        
        
        UILabel *lblDesc=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, WIDTH-100, 15)];
        lblDesc.font=[UIFont fontWithName:FONTNAME size:14.0];
        [lblDesc setText:objSh.show_desc];
        lblDesc.textColor=[UIColor whiteColor];

        [cell.contentView addSubview:lblDesc];
        
        
        UIView *view_acc=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 65, 80)];
        [view_acc setBackgroundColor:[UIColor clearColor]];
        


        UIButton *btnTic=[[UIButton alloc]initWithFrame:CGRectMake(0, 18, 27,42)];
        [btnTic setImage:[UIImage imageNamed:@"purchaseTick.png"] forState:UIControlStateNormal];
        [btnTic addTarget:self action:@selector(favShowClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnTic.accessibilityLabel=objSh.show_id;
    btnTic.tag=indexPath.row;
        [view_acc addSubview:btnTic];

    
    UIButton *btnDel=[[UIButton alloc]initWithFrame:CGRectMake(35, 20, 40, 40)];
    [btnDel setImage:[UIImage imageNamed:objSh.isFav?@"Star-Icon-Active.png":@"fav_mark.png"] forState:UIControlStateNormal];
    [btnDel addTarget:self action:@selector(deleteShowClicked:) forControlEvents:UIControlEventTouchUpInside];
    btnDel.tag=indexPath.row;
    btnDel.accessibilityLabel=objSh.show_id;


//    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(47,25, 30,30)];
//    [img setImage:[UIImage imageNamed:objSh.isFav?@"Star-Icon-Active.png":@"fav_mark.png"] ];
//
//    [view_acc addSubview:img];

    
//    [btnDel.layer setCornerRadius:20.0];
//    [btnDel.layer setBorderColor:NAVNBAR_COLOR.CGColor];
//    [btnDel.layer setBorderWidth:1.0];
    [view_acc addSubview:btnDel];
    
    
    cell.accessoryView=view_acc;

        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0,79, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [cell.contentView addSubview:lblSep];
       UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor =CELL_SELECTTION_COLOR;
    [cell setSelectedBackgroundView:bgColorView];


    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];

    if(Tag==1){
        
        Show *objSh=[arrShow objectAtIndex:indexPath.row];
        TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
        objT.objShow=objSh;
        [self.navigationController pushViewController:objT animated:true];
    }
    if(Tag==2 || Tag==3){
            bgblurView.hidden=false;
            tblView.scrollEnabled=true;
        if(delegate.isMyFav==true){
            delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];

        }
        [delegate playAudioWithIndex:indexPath.row];
    }

    
}

#pragma mark- MARK-TOP MENU METHODS

- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu {
    return 1;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column {
    return 3;
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath {
    switch (indexPath.column) {
        case 0: return @"Hello 1";
            break;
        case 1: return @"Hello 2";
            break;
        case 2: return @"Hello 3";
            break;
        default:
            return nil;
            break;
    }
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath {
    //NSLog(@"column:%li row:%li", (long)indexPath.column, (long)indexPath.row);
}

- (IBAction)showSideMenu:(id)sender {

        [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
            //        [self setupMenuBarButtonItems];
        }];
        
    

}
- (void)didBackgroundTap
{

//    self.menuButton.isActive = !self.menuButton.isActive;
    [self onHandleMenuTap:nil];
}


- (void)onHandleMenuTap:(id)sender
{
    if (!menuButton.selected) {
        menuButton.selected=true;
        //NSLog(@"On show");
        [self onShowMenu];
    } else {
        //NSLog(@"On hide");
        [self onHideMenu];
                menuButton.selected=false;
    }
}



#pragma mark- WEBSERVICE METHODS

-(void)getAllShows
{
//    if(delegate.arrLoaction.count >0){
//        for(Location *objL in delegate.arrLoaction){
//            if([objL.loc_id isEqualToString:delegate.objUser.locId]){
//                delegate.objUser.objLocation=objL;
//                break;
//            }
//        }
//    }
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    
    NSString *postStr=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@&distance_type=%@&playlist_distance=%f&month_range=%ld",[AppDelegate initAppdelegate].objUser.locId,[AppDelegate initAppdelegate].objUser.locId ,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            if(arrTracs.count >0){
                arrTracks=[[NSMutableArray alloc]init];
            for(NSMutableDictionary *dict in arrTracs)
            {

                [arrTracks addObject:[Track initWithResponceDict:dict]];
            }
                
            }
            
            [tblView reloadData];
        }else{
//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];

        }
    }];
    
}



-(void)favShowClicked:(UIButton *)btn{

    Show *objSh=[arrShow objectAtIndex:btn.tag];
//    TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
//    objT.objShow=objSh;
//    [self.navigationController pushViewController:objT animated:true];
//    [AppDelegate initAppdelegate].playerView.hidden=true;
//    Show *objSh=[arrShow objectAtIndex:btn.tag];
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"show_id=%@",objSh.show_id];
    [Utility executePOSTRequestwithServicetype:DETAIL_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableDictionary *dictRes=[dictresponce valueForKey:@"DATA"];
            PurchaseTicket *objP=[[PurchaseTicket alloc]initWithNibName:@"PurchaseTicket" bundle:nil];
            objP.objShow=objSh;
            objP.arrTic=[dictRes valueForKey:@"Tickets"];
            [self.navigationController pushViewController:objP animated:true];
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    

    
}
-(void)deleteShowClicked:(UIButton *)btn
{
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    Show *objSh=[arrShow objectAtIndex:btn.tag];
    
        NSString *postStr=[NSString stringWithFormat:@"msf_show_id=%@&msf_lgn_id=%@",objSh.show_id,[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:MARK_SHOW_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            objSh.isFav=!objSh.isFav;
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNotification"
             object:self];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNoti"
             object:self];
            [tblView reloadData];
        }else{
//            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
            [self showNotificationView:[dictresponce valueForKey:@"MESSAGES"] :[UIImage imageNamed:@"info.png"]];

        }
    }];

}
-(void)getMyFavouritesShows
{
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"msf_lgn_id=%@",[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:GET_MY_FAV_SHOW withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
            if(arr.count >0){
                arrShow=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arr)
                {
                    [arrShow addObject:[Show initWithResponceDict:dict]];
                }
                }
            [tblView reloadData];
            lblHeader.text=[NSString stringWithFormat:@"%lu Shows for this artist",(unsigned long)arrShow.count];


        }else{
//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];

        }
    }];
}

-(void)showNotificationView:(NSString *)msg :(UIImage *)img
{
    notiImg.image=img;
    lblMsg.text=msg;
       [viewNoti setBackgroundColor:[UIColor colorWithRed:125.0/255.0 green:33.0/255.0 blue:210.0/255.0 alpha:0.8]];
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y += frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:3.0];
                     }];
    
}
-(void)hideNotiView
{
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y -= frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         //                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:0.5];
                     }];
    
}



#pragma mark- dropdown Methods

#pragma mark- ANIMATION METHODS
-(IBAction)showMenu:(id)sender{
    
    //    [self onShowMenu];
    if([AppDelegate initAppdelegate].arrPlayList.count==0){
        return;
    }
    [self onShowMenu];
    
}
- (void)onShowMenu
{
    if(menuButton.selected){
        menuButton.selected=false;
        
        [self onHideMenu];
    }else{
        //    if (!self.table) {
        UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect frame = mainWindow.frame;
        //        frame.origin.y += self.view.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
        
        frame.origin.y=80;
        frame.size.height-=150;
        NSMutableArray *iem=[[NSMutableArray alloc]init];
        
        
        if([AppDelegate initAppdelegate].arrPlayList.count >0){
            
            for(int i=0;i<[AppDelegate initAppdelegate].arrPlayList.count;i++){
                PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:i];
                [iem addObject:objs.playName];
            }
        }
        
        self.table = [[SIMenuTable alloc] initWithFrame:frame items:iem];
        self.table.menuDelegate = self;
        self.table.arrData=[AppDelegate initAppdelegate].arrPlayList;
        self.menuContainer=self.view;
        [self.menuContainer addSubview:self.table];
        [self rotateArrow:M_PI];
        [self.table show];
        menuButton.selected=true;
    }
}

- (void)onHideMenu
{
    [self rotateArrow:0];
    [self.table hide];
}

- (void)rotateArrow:(float)degrees
{
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        //        self.menuButton.arrow.layer.transform = CATransform3DMakeRotation(degrees, 0, 0, 1);
    } completion:NULL];
}

#pragma mark -
#pragma mark Delegate methods
- (void)didSelectPlayAtIndex:(NSUInteger)index
{
    [self.navigationController popToRootViewControllerAnimated:true];
    
    //    [self backButtonPressed:nil];
    [self.playListDelegate didSelectPlayAtIndex:index];
    //    [Utility showAlrtWithMessage:@"Play select"];
    return;
    
    
}
- (void)didSelectPlayItemListItemAtIndex:(NSUInteger)index;
{
    [self.navigationController popToRootViewControllerAnimated:true];
    [self.playListDelegate didSelectPlayItemListItemAtIndex:index];
    return;
}
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    
//    [self.navigationController popToRootViewControllerAnimated:true];
//    [self.playListDelegate didSelectItemAtIndex:index];
//    return;
    
    
    
    [self.table hide];
    PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:index];
    
    if([objs.totaltrack integerValue]==0){
        //        [self showNotificationView:@"No Tracks Found" :[UIImage imageNamed:@"info.png"]];
        return;
    }
    //       lblPlayTitle.text=objs.playName;
    
    //    [self onHandleMenuTap:nil];
    
    [AppDelegate initAppdelegate].objPlaylist=objs;
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    [self showOverLayDetail:objs.play_id withName:objs.playName withObj:objs];
    
}


-(void)showOverLayDetail:(NSString *)idPlayList withName:(NSString *)name withObj:(PlayList *)objP
{
    //    [arrTracks removeAllObjects];
    //    [arrShow removeAllObjects];
    //    [tblView reloadData];
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",idPlayList,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTr,*arrSho;
            
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            if(arrTracs.count >0){
                arrTr=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrTracs)
                {
                    [arrTr addObject:[Track initWithResponceDict:dict]];
                }
                
            }
            //                arrTracks=arrTr;
            //            delegate.index_current_song=0;
            //            [delegate playAudioWithIndex:delegate.index_current_song];
            //            [delegate pausePlayer];
            if(arrShows.count >0){
                arrSho=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows)
                {
                    [arrSho addObject:[Show initWithResponceDict:dict]];
                }
                
            }else{
                
            }
            
            DashboardVC *objDash;
            
            for(UIViewController *vc in [self.navigationController viewControllers]){
                if([vc isKindOfClass:[DashboardVC class]]){
                    
                    objDash=(DashboardVC *)vc;
                    break;
                }
            }
            
            
            PlayListVC *objPlayList=[[PlayListVC alloc]initWithNibName:@"PlayListVC_New" bundle:nil];
            objPlayList.isTracks=true;
            objPlayList.playdelegate=objDash;
            objPlayList.isOverLay=true;
            objPlayList.arrShows=arrSho;
            
            objPlayList.currentShowName=name;
            objPlayList.arrData=arrTr;
            objPlayList.objPlayList=objP;
            
            [self.navigationController pushViewController:objPlayList animated:true];
        }else{
            //            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
    
}


- (IBAction)showShareButton:(id)sender {
    if(isShown){
        [shareButton setImage:[UIImage imageNamed:@"shareIconInactive.png"] forState:UIControlStateNormal];
        //        [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
        
        
        // Set delegate and selector to remove from superview when animation completes
        // Move this view to bottom of superview
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        CGRect frame = shareView.frame;
        frame.origin = CGPointMake(0.0,-self.view.bounds.size.height);
        shareView.frame = frame;
        [UIView commitAnimations];
        isShown=false;
        
    }else{
        [shareButton setImage:[UIImage imageNamed:@"shareIconActive.png"] forState:UIControlStateNormal];
        [shareView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
        [view_mainShare setBackgroundColor:[UIColor colorWithRed:15.0/255 green:176.0/255 blue:212.0/255 alpha:0.30]];
        CGRect frame = shareView.frame;
        
        // set up an animation for the transition between the views
        //        CATransition *animation = [CATransition animation];
        //        [animation setDuration:0.5];
        //        [animation setType:kCATransitionPush];
        //        [animation setSubtype:kCATransitionFromTop];
        //        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        
        
        self.view.alpha=1.0f;
        frame.origin = CGPointMake(0.0, 0);
        shareView.frame = frame;
        //        [[shareView layer] addAnimation:animation forKey:@"Slidein"];
        [UIView commitAnimations];
        
        isShown=true;
        
    }
    
}
- (IBAction)shareOnTwitter:(id)sender {
    [self showShareButton:nil];
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//    {
    
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}

- (IBAction)shareOnFacebook:(id)sender {
    [self showShareButton:nil];
    [AppDelegate initAppdelegate].playerView.hidden=true;
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeFacebook];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate initAppdelegate].playerView.hidden=false;
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}

@end
