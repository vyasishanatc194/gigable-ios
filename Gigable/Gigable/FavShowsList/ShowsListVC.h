//
//  PlayListVC
//  Gigable
//
//  Created by Kalpit Gajera on 26/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "PlayListDelegate.h"


@protocol PLAYSELECTDELEGATE <NSObject>
-(void)changeSelection:(PlayList *)objPlayl;
@end;
@interface ShowsListVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    
     IBOutlet UIButton *backBtn;
     IBOutlet UIView *bgblurView;
     IBOutlet UILabel *lblTopTitle;
     IBOutlet UIPageControl *pageControl;
     IBOutlet UIView *topView;

     IBOutlet UIImageView *arrow;
     IBOutlet UIView *top_buttonView;
     IBOutlet UILabel *lblPlayTitle;
     IBOutlet UITableView *tblView;
     IBOutlet UIButton *menuButton;
    NSMutableArray *arrTracks,*arrShow;
    int Tag;
    IBOutlet UIView *viewNoti;
    IBOutlet UIImageView *notiImg;
    IBOutlet UILabel *lblMsg;
    
    
    
    
    IBOutlet UIView *view_mainShare;
    IBOutlet UIButton *shareButton;
    IBOutlet UIView *shareView;
    BOOL isShown;

    
}

@property (assign,nonatomic)id<PLAYSELECTDELEGATE> playdelegate;
@property (nonatomic, weak) id <PlayListDelegate> playListDelegate;

@property (assign,nonatomic)BOOL isTracks,isFav,isTick;
@property (strong,nonatomic)NSMutableArray *arrData,*arrShows;
@property (strong,nonatomic)NSString *currentShowName;
@end
