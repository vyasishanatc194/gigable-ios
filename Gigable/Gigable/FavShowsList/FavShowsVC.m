//
//  DashboardVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 26/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "FavShowsVC.h"
#import "Static.h"
#import "Utility.h"
#import "DOPDropDownMenu.h"
#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import "MFSideMenu.h"
#import "STKAudioPlayer.h"
#import "STKAutoRecoveringHTTPDataSource.h"
#import "SampleQueueId.h"
#import "TicketInfoVC.h"
#import "AppDelegate.h"
#import "PurchaseTicket.h"
#import "PaymentPageVC.h"
@interface FavShowsVC ()<DOPDropDownMenuDataSource, DOPDropDownMenuDelegate, UITableViewDataSource,SIMenuDelegate>
{
    UIView *playerView;
    AppDelegate *delegate;
    UILabel *lblHeader;
}
@property (nonatomic, strong) SIMenuTable *table;
@property (nonatomic, strong) UIView *menuContainer;
@property (strong,nonatomic)STKAudioPlayer* audioPlayer;


@end

@implementation FavShowsVC
@synthesize isTracks,arrData,isFav,isTick;
@synthesize arrShows;
@synthesize currentShowName;
@synthesize audioPlayer;
@synthesize playdelegate;
- (void)viewDidLoad {
    self.navigationController.navigationBarHidden=true;
    [super viewDidLoad];
    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
    delegate=[AppDelegate initAppdelegate];
    bgblurView.hidden=true;
    if(isTick==true){
        lblTopTitle.text=@"Purchase Ticket";
        arrShow=self.arrShows;

    }else{
        lblTopTitle.text=@"My Shows";
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(getMyFavouritesShows)
                                                     name:@"FavNotification"
                                                   object:nil];

        [self getMyFavouritesShows];
    }
        Tag=1;
    lblTopTitle.hidden=false;
    menuButton.hidden=true;
    arrow.hidden=true;
    lblPlayTitle.hidden=true;
    [backBtn setImage:[UIImage imageNamed:@"sidemenu.png"] forState:UIControlStateNormal];
    
    
    if(Tag==1){
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 10)];
        
               tblView.tableHeaderView=view;
    }


}
-(void)viewWillAppear:(BOOL)animated
{
    delegate.playerView.hidden=true;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- TABLEVIEW METHOD
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 1;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;   // custom view for header. will be adjusted to default or specified header height
{
    if(section==0){
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    
    lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(10,10, WIDTH-20, 30)];
    lblHeader.font=[UIFont fontWithName:FONTNAME size:20.0];
    lblHeader.textAlignment=NSTextAlignmentLeft;
    lblHeader.textColor=[UIColor whiteColor];
    if(isTick==true){
        lblHeader.text=[NSString stringWithFormat:@"Shows"];
        
    }else{
        lblHeader.text=[NSString stringWithFormat:@"My Shows"];
    }
    
    
    lblTick=[[UILabel alloc]initWithFrame:CGRectMake(10,40, WIDTH-20, 20)];
    lblTick.font=[UIFont fontWithName:FONTNAME size:13.0];
    lblTick.textAlignment=NSTextAlignmentLeft;
    lblTick.textColor=NAVNBAR_COLOR;
    lblTick.text=[NSString stringWithFormat:@"Tickets (%ld)",arrTic.count];
    
    [view addSubview:lblTick];
    [view addSubview:lblHeader];
    
    UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 59, WIDTH, 0.5)];
    [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
    [view addSubview:lblSep];
    [view setBackgroundColor:[UIColor clearColor]];
        
        return  view;
    }
    if(section==1){
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 20)];
        lblTick=[[UILabel alloc]initWithFrame:CGRectMake(10,0, WIDTH-20, 20)];
        lblTick.font=[UIFont fontWithName:FONTNAME size:13.0];
        lblTick.textAlignment=NSTextAlignmentLeft;
        lblTick.textColor=NAVNBAR_COLOR;
        lblTick.text=[NSString stringWithFormat:@"Watching... (%ld)",arrCurrent.count];
        
        [view addSubview:lblTick];
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 19, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [view addSubview:lblSep];
        [view setBackgroundColor:[UIColor clearColor]];
        return  view;
 
    }
    if(section==2){
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 20)];
        lblTick=[[UILabel alloc]initWithFrame:CGRectMake(10,0, WIDTH-20, 20)];
        lblTick.font=[UIFont fontWithName:FONTNAME size:13.0];
        lblTick.textAlignment=NSTextAlignmentLeft;
        lblTick.textColor=NAVNBAR_COLOR;
        lblTick.text=[NSString stringWithFormat:@"Past Shows (%ld)",arrPast.count];
        
        [view addSubview:lblTick];
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 19, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [view addSubview:lblSep];
        [view setBackgroundColor:[UIColor clearColor]];
        return  view;
        
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 3;    //count of section
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
           return 80;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(Tag==1){
        if(section==0){
            return arrTic.count;
        }
        if(section==1){
            return arrCurrent.count;
        }
        if(section==2){
            return arrPast.count;
        }
       
        return 0;
    }
    
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    if(section==0){
        return 60;
    }else{
        return 20;
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    cell.backgroundColor=[UIColor clearColor];

    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([tblView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tblView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tblView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tblView setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier] ;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
  
    if(indexPath.section==0){
    
        Show *objSh=[arrTic objectAtIndex:indexPath.row];
        //        cell.textLabel.text=objSh.show_title;
        //        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
        //        cell.textLabel.textColor=[UIColor whiteColor];
        
        NSString *dateString = objSh.show_Full_Date;
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *yourDate = [dateFormatter dateFromString:dateString];
        dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";
        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
        
        UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 15, WIDTH-100, 15)];
        lblDate.font=[UIFont fontWithName:FONTNAME size:13.0];
        [lblDate setText:[dateFormatter stringFromDate:yourDate]];
        lblDate.textColor=[UIColor whiteColor];
        
        [cell.contentView addSubview:lblDate];
        
        UILabel *lblShowName=[[UILabel alloc]initWithFrame:CGRectMake(5, 30, WIDTH-100, 20)];
        lblShowName.font=[UIFont fontWithName:FONTNAMEBOLD size:15.0];
        lblShowName.textColor=[UIColor whiteColor];
        [lblShowName setText:objSh.show_title];
        
        [cell.contentView addSubview:lblShowName];
        
        
        UILabel *lblDesc=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, WIDTH-100, 15)];
        lblDesc.font=[UIFont fontWithName:FONTNAME size:14.0];
        [lblDesc setText:objSh.show_desc];
        lblDesc.textColor=[UIColor whiteColor];
        
        [cell.contentView addSubview:lblDesc];

        UIButton *btnTic=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 33,33)];
        [btnTic setImage:[UIImage imageNamed:@"active-ticket-icon.png"] forState:UIControlStateNormal];
        [btnTic addTarget:self action:@selector(favShowClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnTic.accessibilityLabel=objSh.show_id;
        btnTic.tag=indexPath.row;
        cell.accessoryView =btnTic;

    }
    if(indexPath.section==1){
        Show *objSh=[arrCurrent objectAtIndex:indexPath.row];
        //        cell.textLabel.text=objSh.show_title;
        //        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
        //        cell.textLabel.textColor=[UIColor whiteColor];
        
        NSString *dateString = objSh.show_Full_Date;
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *yourDate = [dateFormatter dateFromString:dateString];
        dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";
        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
        
        UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 15, WIDTH-100, 15)];
        lblDate.font=[UIFont fontWithName:FONTNAME size:13.0];
        [lblDate setText:[dateFormatter stringFromDate:yourDate]];
        lblDate.textColor=[UIColor whiteColor];
        
        [cell.contentView addSubview:lblDate];
        
        UILabel *lblShowName=[[UILabel alloc]initWithFrame:CGRectMake(5, 30, WIDTH-100, 20)];
        lblShowName.font=[UIFont fontWithName:FONTNAMEBOLD size:15.0];
        lblShowName.textColor=[UIColor whiteColor];
        [lblShowName setText:objSh.show_title];
        
        [cell.contentView addSubview:lblShowName];
        
        
        UILabel *lblDesc=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, WIDTH-100, 15)];
        lblDesc.font=[UIFont fontWithName:FONTNAME size:14.0];
        [lblDesc setText:objSh.show_desc];
        lblDesc.textColor=[UIColor whiteColor];
        
        [cell.contentView addSubview:lblDesc];
        UIView *view_acc=[[UIView alloc]initWithFrame:CGRectMake(0, 5, 65, 80)];
        [view_acc setBackgroundColor:[UIColor clearColor]];
        
        //        UIButton *btnTic=[[UIButton alloc]initWithFrame:CGRectMake(0, 18, 27,42)];
        UIButton *btnTic=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 20,31)];
        
        [btnTic setImage:[UIImage imageNamed:@"purchaseTick.png"] forState:UIControlStateNormal];
        [btnTic addTarget:self action:@selector(showTicketInfo:) forControlEvents:UIControlEventTouchUpInside];
        btnTic.accessibilityLabel=objSh.show_id;
        btnTic.tag=indexPath.row;
        [view_acc addSubview:btnTic];
        
        
        UIButton *btnDel=[[UIButton alloc]initWithFrame:CGRectMake(35, 25, 30,30)];
        [btnDel addTarget:self action:@selector(deleteShowClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnDel.tag=indexPath.row;
        btnDel.accessibilityLabel=objSh.show_id;
        
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(44, 34, 12,12)];
        [img setImage:[UIImage imageNamed:@"close_over.png"]];
        [view_acc addSubview:img];
        
        
        [btnDel.layer setCornerRadius:15.0];
        [btnDel.layer setBorderColor:NAVNBAR_COLOR.CGColor];
        [btnDel.layer setBorderWidth:1.0];
        [view_acc addSubview:btnDel];
        cell.accessoryView=view_acc;

    }
    if(indexPath.section==2){
        Show *objSh=[arrPast objectAtIndex:indexPath.row];

        NSString *dateString = objSh.show_Full_Date;
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *yourDate = [dateFormatter dateFromString:dateString];
        dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";
        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
        
        UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 15, WIDTH-100, 15)];
        lblDate.font=[UIFont fontWithName:FONTNAME size:13.0];
        [lblDate setText:[dateFormatter stringFromDate:yourDate]];
        lblDate.textColor=[UIColor whiteColor];
        
        [cell.contentView addSubview:lblDate];
        
        UILabel *lblShowName=[[UILabel alloc]initWithFrame:CGRectMake(5, 30, WIDTH-100, 20)];
        lblShowName.font=[UIFont fontWithName:FONTNAMEBOLD size:15.0];
        lblShowName.textColor=[UIColor whiteColor];
        [lblShowName setText:objSh.show_title];
        
        [cell.contentView addSubview:lblShowName];
        
        
        UILabel *lblDesc=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, WIDTH-100, 15)];
        lblDesc.font=[UIFont fontWithName:FONTNAME size:14.0];
        [lblDesc setText:objSh.show_desc];
        lblDesc.textColor=[UIColor whiteColor];
        
        [cell.contentView addSubview:lblDesc];

    }

           UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0,79, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [cell.contentView addSubview:lblSep];
       UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor =CELL_SELECTTION_COLOR;
    [cell setSelectedBackgroundView:bgColorView];


    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    if(indexPath.section==1){
        Show *objSh=[arrCurrent objectAtIndex:indexPath.row];
        TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
        objT.objShow=objSh;
        [self.navigationController pushViewController:objT animated:true];

    }
    return;
    if(Tag==1){
        
        Show *objSh=[arrShow objectAtIndex:indexPath.row];
        TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
        objT.objShow=objSh;
        [self.navigationController pushViewController:objT animated:true];
//        if(indexPath.row==1){
//            Tag=1;
//            Show *objS=[arrShow objectAtIndex:indexPath.row];
//            bgblurView.hidden=false;
//            tblView.scrollEnabled=true;
//             [self getPlayLisTDetail:objS.show_id];
//            
//        }
    }
    if(Tag==2 || Tag==3){
            bgblurView.hidden=false;
            tblView.scrollEnabled=true;
        if(delegate.isMyFav==true){
            delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];

        }
        [delegate playAudioWithIndex:indexPath.row];
    }

    
}

#pragma mark- MARK-TOP MENU METHODS

- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu {
    return 1;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column {
    return 3;
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath {
    switch (indexPath.column) {
        case 0: return @"Hello 1";
            break;
        case 1: return @"Hello 2";
            break;
        case 2: return @"Hello 3";
            break;
        default:
            return nil;
            break;
    }
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath {
    //NSLog(@"column:%li row:%li", (long)indexPath.column, (long)indexPath.row);
}

- (IBAction)showSideMenu:(id)sender {

        [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
            //        [self setupMenuBarButtonItems];
        }];
        
    

}
#pragma mark- ANIMATION METHODS
-(IBAction)showMenu:(id)sender{
    
    [self onShowMenu];
}
- (void)onShowMenu
{
    if(menuButton.selected){
        menuButton.selected=false;

        [self onHideMenu];
    }else{
    if (!self.table) {
        UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect frame = mainWindow.frame;
        frame.size.height-=150;

//        frame.origin.y += self.view.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
        NSMutableArray *iem=[[NSMutableArray alloc]init];
               if(self.arrShows.count >0){
            
            for(int i=0;i<self.arrShows.count;i++){
                PlayList *objs=[self.arrShows objectAtIndex:i];
                [iem addObject:objs.playName];
            }
        }
        

        frame.origin.y=80;
        self.table = [[SIMenuTable alloc] initWithFrame:frame items:iem];
        self.table.menuDelegate = self;
        self.table.arrData=self.arrShows;

    }
    self.menuContainer=self.view;
    [self.menuContainer addSubview:self.table];
    [self rotateArrow:M_PI];
    [self.table show];
        menuButton.selected=true;
    }
}

- (void)onHideMenu
{
    [self rotateArrow:0];
    [self.table hide];
}

- (void)rotateArrow:(float)degrees
{
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
//        self.menuButton.arrow.layer.transform = CATransform3DMakeRotation(degrees, 0, 0, 1);
    } completion:NULL];
}

#pragma mark -
#pragma mark Delegate methods
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    PlayList *objs=[self.arrShows objectAtIndex:index];
    
    
    lblPlayTitle.text=objs.playName;
    [self onHandleMenuTap:nil];
    [self getPlayLisTDetail:objs.play_id];
    if(self.playdelegate){
        [self.playdelegate changeSelection:objs];
    }
   
}
-(void)getPlayLisTDetail:(NSString *)idPlayList
{
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
//    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@",idPlayList,[AppDelegate initAppdelegate].objUser.locId ];
    
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",idPlayList,[AppDelegate initAppdelegate].objUser.locId,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude, @"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];

    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            if(arrTracs.count >0){
                arrTracks=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrTracs)
                {
                    [arrTracks addObject:[Track initWithResponceDict:dict]];
                }
                delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
                delegate.index_current_song=0;
                [delegate playAudioWithIndex:delegate.index_current_song];
            }else{
                if(Tag==1){
                    lblHeader.text=[NSString stringWithFormat:@"%lu   Shows in this playlist",(unsigned long)arrShow.count];
                }else if(Tag==2){
                    lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in this playlist",(unsigned long)arrTracks.count];
                }else{
                    lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
                }

            }
            
                    [tblView reloadData];
            
            // [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictRes valueForKey:@"DATA"]];
            //[[AppDelegate initAppdelegate]showDashboardMenu];
        }else{
//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];

        }
    }];
    
    return;
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"show_id=%@",idPlayList];
    [Utility executePOSTRequestwithServicetype:DETAIL_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
            for(NSString * strCount in arr)
            {
                //NSLog(@"%@",strCount);
                NSMutableDictionary *dict=[arr valueForKey:strCount];
                //NSLog(@"%@",dict);
                
                
            }
            //            [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictRes valueForKey:@"DATA"]];
            //            [[AppDelegate initAppdelegate]showDashboardMenu];
        }else{
//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];

        }
    }];
    
}
- (void)didBackgroundTap
{

//    self.menuButton.isActive = !self.menuButton.isActive;
    [self onHandleMenuTap:nil];
}


- (void)onHandleMenuTap:(id)sender
{
    if (!menuButton.selected) {
        menuButton.selected=true;
        //NSLog(@"On show");
        [self onShowMenu];
    } else {
        //NSLog(@"On hide");
        [self onHideMenu];
                menuButton.selected=false;
    }
}



#pragma mark- WEBSERVICE METHODS

-(void)getAllShows
{
//    if(delegate.arrLoaction.count >0){
//        for(Location *objL in delegate.arrLoaction){
//            if([objL.loc_id isEqualToString:delegate.objUser.locId]){
//                delegate.objUser.objLocation=objL;
//                break;
//            }
//        }
//    }
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    
    NSString *postStr=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@",[AppDelegate initAppdelegate].objUser.locId,[AppDelegate initAppdelegate].objUser.locId ];
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            if(arrTracs.count >0){
                arrTracks=[[NSMutableArray alloc]init];
            for(NSMutableDictionary *dict in arrTracs)
            {

                [arrTracks addObject:[Track initWithResponceDict:dict]];
            }
                
            }
            
            
            
            [tblView reloadData];
// [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictRes valueForKey:@"DATA"]];
//[[AppDelegate initAppdelegate]showDashboardMenu];
        }else{
//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];

        }
    }];
    
}

-(void)favShowClicked:(UIButton *)btn{
    Show *objSh=[arrTic objectAtIndex:btn.tag];

    PaymentPageVC *objP=[[PaymentPageVC alloc]initWithNibName:@"PaymentPageVC" bundle:nil];
    objP.ticketId=objSh.tickId;
    [self.navigationController pushViewController:objP animated:true];
    
   //    PurchaseTicket *objP=[[PurchaseTicket alloc]initWithNibName:@"PurchaseTicket" bundle:nil];
//    objP.objShow=[arrShow objectAtIndex:btn.tag];
//    [self.navigationController pushViewController:objP animated:true];

    
}
-(void)deleteShowClicked:(UIButton *)btn
{
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"msf_show_id=%@&msf_lgn_id=%@",btn.accessibilityLabel,[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:DELETE_SHOW_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [arrCurrent removeObjectAtIndex:btn.tag];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNotification"
             object:self];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNoti"
             object:self];
            [tblView reloadData];
            //            lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
            
            
        }else{
            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
        }
    }];
   
}
-(void)getMyFavouritesShows
{
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"msf_lgn_id=%@",[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:GET_MY_FAV_SHOW withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
            if(arr.count >0){
                
                NSMutableDictionary *dictER=[dictresponce valueForKey:@"DATA"];
                
              NSMutableArray *arrtempt=[dictER valueForKey:@"currentArray"];
                NSMutableArray *arrtempt1=[dictER valueForKey:@"pastArray"];
               NSMutableArray *arrtem=[dictER valueForKey:@"ticketArray"];
                
                arrCurrent=[[NSMutableArray alloc]init];
                arrPast=[[NSMutableArray alloc]init];
                arrTic=[[NSMutableArray alloc]init];

                for(NSMutableDictionary *dict in arrtempt)
                {
                    Show *objs=[Show initWithResponceDict:dict];
                    [arrCurrent addObject:objs];
                }
                for(NSMutableDictionary *dict in arrtempt1)
                {
                    Show *objs=[Show initWithResponceDict:dict];
[arrCurrent addObject:objs];
                }

                for(NSMutableDictionary *dict in arrtem)
                {
                    Show *objs=[Show initWithResponceDict:dict];
                   
                    [arrTic addObject:objs];
                }

                
//                lblTick.text=[NSString stringWithFormat:@"Tickets (%d)",pur];
                }
            [tblView reloadData];
//            lblHeader.text=[NSString stringWithFormat:@"%lu   Shows in my favorites",(unsigned long)arrShow.count];


        }else{
//            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];

        }
    }];
}



-(IBAction)showTicketInfo:(UIButton *)sender
{
    Show *objSh=[arrCurrent objectAtIndex:sender.tag];
//    TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
//    objT.objShow=objSh;
//    [self.navigationController pushViewController:objT animated:true];
//    [AppDelegate initAppdelegate].playerView.hidden=true;

    
    
//    Show *objSh=[arrShow objectAtIndex:btn.tag];
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"show_id=%@",objSh.show_id];
    [Utility executePOSTRequestwithServicetype:DETAIL_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableDictionary *dictRes=[dictresponce valueForKey:@"DATA"];
            PurchaseTicket *objP=[[PurchaseTicket alloc]initWithNibName:@"PurchaseTicket" bundle:nil];
            objP.objShow=objSh;
            objP.arrTic=[dictRes valueForKey:@"Tickets"];
            [self.navigationController pushViewController:objP animated:true];
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    

}
-(void)showNotificationView:(NSString *)msg :(UIImage *)img
{
    notiImg.image=img;
    lblMsg.text=msg;
       [viewNoti setBackgroundColor:[UIColor colorWithRed:125.0/255.0 green:33.0/255.0 blue:210.0/255.0 alpha:0.8]];
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y += frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:3.0];
                     }];
    
}
-(void)hideNotiView
{
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y -= frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         //                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:0.5];
                     }];
    
}


@end
