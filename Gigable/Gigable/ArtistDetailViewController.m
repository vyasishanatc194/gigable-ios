//
//  ArtistDetailViewController.m
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import "ArtistDetailViewController.h"
#import "ConcertPlayListViewController.h"
#import "HomeDashboardViewController.h"
#import "HomeDashboardViewController.h"
#import "AppDelegate.h"
#import "Static.h"
#import "Utility.h"
#import "ArtistCellCollectionViewCell.h"
#import "FeaturedArtistCell.h"
#import "PlaylistCell.h"
#import "UIImageView+WebCache.h"
#import "ConcertListTableViewCell.h"
#import "FeaturedTrackTableViewCell.h"
#import "ShowDetailViewController.h"

@interface ArtistDetailViewController ()
{
    NSMutableArray *artistData;
    NSMutableArray *artistShow;
    NSMutableArray *artistTrack;
    NSMutableArray *arrTracks;
    AppDelegate *delegate;
    NSString *imageURl;
    NSString *ArtistName;
}
@end

@implementation ArtistDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    imageURl = @"";
    ArtistName = @"";
     delegate=[AppDelegate initAppdelegate];
    [self GetArtistDetail];
    artistData = [[NSMutableArray alloc]init];
    artistShow = [[NSMutableArray alloc]init];
    artistTrack = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    _lblFeaturedTrack.hidden = false;
    _lblelConcert.hidden = false;
    self.navigationController.navigationBarHidden = YES;
    _tblConcerts.estimatedRowHeight = 120;
    _tblConcerts.rowHeight = UITableViewAutomaticDimension;
    _tblConcerts.scrollEnabled = false;
    _tblfeaturedTrack.scrollEnabled = false;
    _imgArtist.layer.borderWidth = 1.0;
    _imgArtist.layer.borderColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:211.0/255.0 alpha:1.0].CGColor;
    
//    [[AppDelegate initAppdelegate] pausePlayer];
    [AppDelegate initAppdelegate].playerView.hidden = true;

    if(delegate.audioPlayer.isPlaying){
        
        [AppDelegate initAppdelegate].playerView.hidden = false;
        _bottom_To_playlist.constant = 200;
        
    }else{
        
        [AppDelegate initAppdelegate].playerView.hidden = true;
        _bottom_To_playlist.constant = 0;
       
    }
}
-(void)GetArtistDetail{
    
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    
    //    NSString *post = [NSString stringWithFormat: @"Lgn_id=%@&Latitude=%f&Longitude=%f&playlist_distance=%f&distance_type=%@&month_range=%ld",[AppDelegate initAppdelegate].objUser.user_id,self.locationManager.location.coordinate.latitude,self.locationManager.location.coordinate.longitude,[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],@"",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    NSString *post = [NSString stringWithFormat: @"art_id=%@",self.ArtistId];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,GET_ARTIST_DETAIL]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               STOPLOADING()
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   NSMutableDictionary * dictRes = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   NSLog(@"%@",dictRes);
                                   
                                   if([[dictRes valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       artistData = [dictRes valueForKey:@"DATA"];
                                       _lblArtistName.text = [artistData valueForKey:@"art_name"];
                                       _lblHeaderArtistName.text = [artistData valueForKey:@"art_name"];
                                       _lblPlayName.text = [artistData valueForKey:@"play_name"];
//                                       NSString *imageURl=[artistData valueForKey:@"art_image"];
                                       
                                       imageURl = [artistData valueForKey:@"art_image"];
                                       ArtistName = [artistData valueForKey:@"art_name"];
                                       
                                       
                                       [_imgArtist sd_setImageWithURL:[NSURL URLWithString:imageURl]
                                                     placeholderImage:[UIImage imageNamed:@""]];
                                       _lblArtistDescription.text = [artistData valueForKey:@"art_description"];
                                       NSLog(@"%ld",(long)_lblArtistDescription.numberOfLines);
                                       artistShow = [artistData valueForKey:@"Show"];
                                       artistTrack = [artistData valueForKey:@"Track"];
                                       
                                      
                                       delegate.arrTracks = [[NSMutableArray alloc]init];
                                       [delegate.arrTracks arrayByAddingObject:artistTrack];
//                                       delegate.arrTracks = artistTrack;
                                       NSLog(@"%@",delegate.arrTracks);
                                       _lblConcerts.text = [NSString stringWithFormat:@"%lu Concerts",(unsigned long)artistShow.count];
                                       _lblTracks.text = [NSString stringWithFormat:@"%lu Tracks",(unsigned long)artistTrack.count];
                                       
                                       
                                       if(artistTrack.count >0){
                                           
                                           for(NSMutableDictionary *dict in artistTrack)
                                           {
                                               [delegate.arrTracks addObject:[Track initWithResponceDict:dict]];
                                           }
                                           
//                                           delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
//                                           delegate.index_current_song=0;
                                           
                                       }

                                       
                                       
                                       
                                       if (artistShow.count == 0)
                                       {
                                           _tblConcertHeight.constant = 0;
                                           
                                           _lblelConcert.hidden = true;
                                       }
                                       if (artistTrack.count == 0)
                                       {
                                           _tblFeaturedTaskHeight.constant = 0;
                                           _lblFeaturedTrack.hidden = true;
                                       }

                                       [_tblfeaturedTrack reloadData];
                                       [_tblConcerts reloadData];
                                       _mainViewHeight.constant = _tblConcerts.contentSize.height + _tblfeaturedTrack.contentSize.height + 130 + 54 + 41;
                                       
                                       //                                       PlayListTracks = [PlayalistAllData valueForKey:@"Track"];
                                       //                                       [_tblconcertPlaylist reloadData];
                                   }else{
                                       
                                       
                                       
                                   }
                                   
                               }
                           }];
    
}
- (IBAction)btnTwitterPressed:(id)sender {
}
- (IBAction)btnFacebookPressed:(id)sender {
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblConcerts)
    {
        [tableView registerNib:[UINib nibWithNibName:@"ConcertListTableViewCell" bundle:nil] forCellReuseIdentifier:@"ConcertListTableViewCell"];
        ConcertListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConcertListTableViewCell"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [formatter dateFromString:[[artistShow objectAtIndex:indexPath.row]valueForKey:@"show_date"]];
        
        [formatter setDateFormat:@"dd MMM yyyy"];
        NSString *output = [formatter stringFromDate:date];
        
        cell.lblConcertDate.text = output;

        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.lblConcertDate.text = [[artistShow objectAtIndex:indexPath.row]valueForKey:@"show_date"];
        cell.lblConcertPlace.text = [[artistShow objectAtIndex:indexPath.row]valueForKey:@"show_venue"];
        cell.lblConcertPrice.text = [NSString stringWithFormat:@"$ %@",[[artistShow objectAtIndex:indexPath.row]valueForKey:@"show_price"]];
        
        _tblConcertHeight.constant = _tblConcerts.contentSize.height;
        _mainViewHeight.constant = _tblConcerts.contentSize.height + _tblfeaturedTrack.contentSize.height + 130 + 54 + 41;
        return cell;
    }
    else
    {
        [tableView registerNib:[UINib nibWithNibName:@"FeaturedTrackTableViewCell" bundle:nil] forCellReuseIdentifier:@"FeaturedTrackTableViewCell"];
        
        FeaturedTrackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeaturedTrackTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lbltrackTitle.text = [[artistTrack objectAtIndex:indexPath.row]valueForKey:@"trk_title"];
        cell.lblplaylistname.text = [NSString stringWithFormat:@"from %@ playlist",[artistData valueForKey:@"play_name"]] ;
        cell.viewBorder.layer.borderWidth = 1.0;
        cell.viewBorder.layer.borderColor = [UIColor colorWithRed:10.0/255.0 green:216.0/255.0 blue:248.0/255.0 alpha:1.0].CGColor;
        _tblFeaturedTaskHeight.constant = _tblfeaturedTrack.contentSize.height;
        _mainViewHeight.constant = _tblConcerts.contentSize.height + _tblfeaturedTrack.contentSize.height + 130 + 54 + 41;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblConcerts)
    {
        ShowDetailViewController *controller = [[ShowDetailViewController alloc]initWithNibName:@"ShowDetailViewController" bundle:nil];
        controller.showId = [[artistShow objectAtIndex:indexPath.row]valueForKey:@"show_id"];
        controller.artistImage = imageURl;
        controller.artistName = ArtistName;
//        controller.modalPresentationStyle = UIModalPresentationCustom;
//        [self presentViewController:controller animated:true completion:nil];
        [self.navigationController pushViewController:controller animated:false];
    }
    else
    {
        NSLog(@"%@",delegate.arrTracks);
        [delegate playAudioWithIndex:indexPath.row];
        _bottom_To_playlist.constant = 200;
    }

}
- (IBAction)btnBackPressed:(id)sender {
    [[self navigationController]popViewControllerAnimated:NO];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tblConcerts)
    {
        return artistShow.count;
    }
    else
    {
        return artistTrack.count;
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
//    [[AppDelegate initAppdelegate] pausePlayer];
    [AppDelegate initAppdelegate].playerView.hidden = true;

}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
