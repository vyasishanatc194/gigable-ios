//
//  UpcomingConcertViewController.h
//  Gigable
//
//  Created by AppDev03 on 2/3/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingConcertViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrTracks;
}
@property (strong, nonatomic) IBOutlet UITableView *tblView;

@property (strong, nonatomic)  NSMutableArray *arrShowForUpcoming;
@property (nonatomic)  BOOL isGetTicket;
@end
