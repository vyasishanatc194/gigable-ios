//
//  DashboardVC.m
//  Gigable
//
//  Created by Kalpit Gajera on 26/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import "PlayListVC.h"
#import "Static.h"
#import "Utility.h"
#import "DOPDropDownMenu.h"
#import "SINavigationMenuView.h"
#import "SIMenuConfiguration.h"
#import "MFSideMenu.h"
#import "STKAudioPlayer.h"
#import "STKAutoRecoveringHTTPDataSource.h"
#import "SampleQueueId.h"
#import "TicketInfoVC.h"
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import "PurchaseTicket.h"
#import <Social/Social.h>
#import "DashboardVC.h"

@interface PlayListVC ()<DOPDropDownMenuDataSource, DOPDropDownMenuDelegate, UITableViewDataSource,SIMenuDelegate>
{
    UIView *playerView;
    AppDelegate *delegate;
    UILabel *lblHeader;
    BOOL playerHidden;
}
@property (nonatomic, strong) SIMenuTable *table;
@property (nonatomic, strong) UIView *menuContainer;
@property (strong,nonatomic)STKAudioPlayer* audioPlayer;


@end

@implementation PlayListVC
@synthesize objPlayList;
@synthesize playListDelegate;
@synthesize isTracks,arrData,isFav;
@synthesize arrShows;
@synthesize currentShowName;
@synthesize audioPlayer;
@synthesize playdelegate;
@synthesize isOverLay;
@synthesize isFromTic;
- (void)viewDidLoad {
    self.navigationController.navigationBarHidden=true;
    [super viewDidLoad];
    playerHidden=[AppDelegate initAppdelegate].playerView.hidden;

    //[topView setBackgroundColor:NAVNBAR_COLOR_LIGHT];
    delegate=[AppDelegate initAppdelegate];
    if( self.isOverLay==true){
        delegate.playerView.hidden=true;
    }
 bgblurView.hidden=true;
    
    if(self.isTracks==true){
        Tag=2;
        arrTracks=self.arrData;
        bgblurView.hidden=false;
        tblView.scrollEnabled=true;
        lblTopTitle.hidden=true;
        lblPlayTitle.text=self.currentShowName;
        [backBtn setImage:[UIImage imageNamed:@"back_nav.png"] forState:UIControlStateNormal];

    }else if(self.isFav==true) {
        Tag=3;
        lblTopTitle.hidden=false;
        lblTopTitle.text=@"favorites";
        menuButton.hidden=true;
        arrow.hidden=true;
        lblPlayTitle.hidden=true;
        arrShow=self.arrData;
        [backBtn setImage:[UIImage imageNamed:@"sidemenu.png"] forState:UIControlStateNormal];
        [self getMyFavourites];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(getMyFavourites)
                                                     name:@"FavClick"
                                                   object:nil];
        
        
          }else{
        Tag=1;
        lblTopTitle.hidden=true;
//        lblTopTitle.text=@"Shows List";
//        menuButton.hidden=true;
//        arrow.hidden=true;
//        lblPlayTitle.hidden=true;
        arrShow=self.arrData;
        [backBtn setImage:[UIImage imageNamed:@"back_nav.png"] forState:UIControlStateNormal];
              [[NSNotificationCenter defaultCenter] addObserver:self
                                                       selector:@selector(getCurrDetail)
                                                           name:@"FavNotifi"
                                                         object:nil];

    }
    
     if(self.isOverLay==false){
     
    if(Tag==1  || Tag==2 || Tag==3){
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 50)];
        UIButton *btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 0, 20, 50)];
        
        [btn setImage:[UIImage imageNamed:@"Back_track.png"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        
        
        lblHeader=[[UILabel alloc]initWithFrame:CGRectMake(25,0, WIDTH-50, 50)];
        lblHeader.font=[UIFont fontWithName:FONTNAME size:20.0];
        lblHeader.textAlignment=NSTextAlignmentCenter;
        lblHeader.textColor=[UIColor whiteColor];
        
        if(Tag==1){
            if(self.isFromTic==true){
                lblHeader.text=[NSString stringWithFormat:@"%lu shows for this artist",(unsigned long)arrShow.count];
                
            }else{
            if([self.objPlayList.play_id isEqualToString:@"local_playlist"]){
                lblHeader.text=[NSString stringWithFormat:@"%lu shows for this playlist",(unsigned long)arrShow.count];

            }else{
                lblHeader.text=[NSString stringWithFormat:@"%lu shows for this playlist",(unsigned long)arrShow.count];

            }
            }
            
        }else if(Tag==2){
            lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in this playlist",(unsigned long)arrTracks.count];
        }else{
            lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
        }
        [view addSubview:lblHeader];
        
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0, 49, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [view addSubview:lblSep];
        [view setBackgroundColor:[UIColor clearColor]];
        tblView.tableHeaderView=view;
    }
     }else{
         
         lblTrackCount.text=[NSString stringWithFormat:@"%d Tracks",arrTracks.count];
         [btnPlay.layer setCornerRadius:btnPlay.frame.size.width/2];
         btnPlay.layer.masksToBounds=true;
         [btnPlay.layer setBorderWidth:1.0];
         [btnPlay.layer setBorderColor:NAVNBAR_COLOR.CGColor];
         [btnClose.layer setCornerRadius:btnPlay.frame.size.width/2];
         btnClose.layer.masksToBounds=true;
         [btnClose.layer setBorderWidth:1.0];
         [btnClose.layer setBorderColor:NAVNBAR_COLOR.CGColor];
            [btnPlay setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];

     }

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setPlayIndicator) name:@"PLAYCHANGE" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    lblPlayTitle.text= [AppDelegate initAppdelegate].objPlaylist.playName;
    

}
- (void)viewWillDisappear:(BOOL)animated; {
    [AppDelegate initAppdelegate].playerView.hidden=playerHidden;
}
-(void)setPlayIndicator
{
    [tblView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- TABLEVIEW METHOD
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;    //count of section
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (Tag==1) {
        return 80;
    }
           return 50;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if(Tag==1){
        return arrShow.count;
    }
    if(Tag==2 || Tag==3){
        return arrTracks.count;
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    cell.backgroundColor=[UIColor clearColor];

    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([tblView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tblView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tblView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tblView setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:MyIdentifier] ;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    
  
    if(Tag==1){
       
      /*  Show *objSh=[arrShow objectAtIndex:indexPath.row];
        cell.textLabel.text=objSh.show_title;
        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
        cell.textLabel.textColor=[UIColor whiteColor];
        
        
        UIView *view_acc=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 70, 50)];
        [view_acc setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *imgB=[[UIImageView alloc]initWithFrame:CGRectMake(50,17, 26,15)];
        imgB.image=[UIImage imageNamed:@"play_indicator.png"];

        [view_acc addSubview:imgB];

        UIButton *btnStar=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        
        [btnStar setImage:[UIImage imageNamed:objSh.isFav?@"Star-Icon-Active.png":@"Star-Icon.png"] forState:UIControlStateNormal];
        [btnStar addTarget:self action:@selector(favShowClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnStar.tag=indexPath.row;
        
        [view_acc addSubview:btnStar];
        cell.accessoryView=view_acc;

        
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0,49, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [cell.contentView addSubview:lblSep];*/
        static NSString *MyIdentifier = @"MyIdentifier";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier] ;
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
        
        
        Show *objSh=[arrShow objectAtIndex:indexPath.row];
        //        cell.textLabel.text=objSh.show_title;
        //        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
        //        cell.textLabel.textColor=[UIColor whiteColor];
        
        NSString *dateString = objSh.show_Full_Date;
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *yourDate = [dateFormatter dateFromString:dateString];
        dateFormatter.dateFormat = @"MMM dd, yyyy hh:mm a";
        
        //NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
        
        UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 15, WIDTH-100, 15)];
        lblDate.font=[UIFont fontWithName:FONTNAME size:13.0];
        [lblDate setText:[dateFormatter stringFromDate:yourDate]];
        lblDate.textColor=[UIColor whiteColor];
        
        [cell.contentView addSubview:lblDate];
        
        UILabel *lblShowName=[[UILabel alloc]initWithFrame:CGRectMake(5, 30, WIDTH-100, 20)];
        lblShowName.font=[UIFont fontWithName:FONTNAMEBOLD size:15.0];
        lblShowName.textColor=[UIColor whiteColor];
        [lblShowName setText:objSh.show_title];
        
        [cell.contentView addSubview:lblShowName];
        
        
        UILabel *lblDesc=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, WIDTH-100, 15)];
        lblDesc.font=[UIFont fontWithName:FONTNAME size:14.0];
        [lblDesc setText:objSh.show_desc];
        lblDesc.textColor=[UIColor whiteColor];
        
        [cell.contentView addSubview:lblDesc];
        
        
        UIView *view_acc=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 65, 80)];
        [view_acc setBackgroundColor:[UIColor clearColor]];
        
        
        
        UIButton *btnTic=[[UIButton alloc]initWithFrame:CGRectMake(0, 18, 27,42)];
        [btnTic setImage:[UIImage imageNamed:@"purchaseTick.png"] forState:UIControlStateNormal];
        [btnTic addTarget:self action:@selector(purchaseTic:) forControlEvents:UIControlEventTouchUpInside];
        btnTic.accessibilityLabel=objSh.show_id;
        btnTic.tag=indexPath.row;
        [view_acc addSubview:btnTic];
        
        
        UIButton *btnDel=[[UIButton alloc]initWithFrame:CGRectMake(35, 20, 40, 40)];
        [btnDel setImage:[UIImage imageNamed:objSh.isFav?@"Star-Icon-Active.png":@"fav_mark.png"] forState:UIControlStateNormal];
        [btnDel addTarget:self action:@selector(favShowClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnDel.tag=indexPath.row;
        btnDel.accessibilityLabel=objSh.show_id;
        
        
        //    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(47,25, 30,30)];
        //    [img setImage:[UIImage imageNamed:objSh.isFav?@"Star-Icon-Active.png":@"fav_mark.png"] ];
        //
        //    [view_acc addSubview:img];
        
        
        //    [btnDel.layer setCornerRadius:20.0];
        //    [btnDel.layer setBorderColor:NAVNBAR_COLOR.CGColor];
        //    [btnDel.layer setBorderWidth:1.0];
        [view_acc addSubview:btnDel];
        
        
        cell.accessoryView=view_acc;
        
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0,79, WIDTH, 0.5)];
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        [cell.contentView addSubview:lblSep];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor =CELL_SELECTTION_COLOR;
        [cell setSelectedBackgroundView:bgColorView];
        
        
        return cell;

    }
    if(Tag==2 || Tag==3){
        if(self.isOverLay==false){
            if(self.isFav==false){
            UIImageView *imgB=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"play_indicator.png"]];
            
            cell.accessoryView=imgB;
            }
            

        }
        
        
            Track *objSh=[arrTracks objectAtIndex:indexPath.row];
        cell.textLabel.text=objSh.trk_title;
        cell.textLabel.font=[UIFont fontWithName:FONTNAME size:14.0];
        cell.textLabel.textColor=[UIColor whiteColor];
        UILabel *lblSep=[[UILabel alloc]initWithFrame:CGRectMake(0,49, WIDTH, 0.5)];
        
        
        if(self.isFav==true){
            
            
            UIView *accessView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
            UIImageView *imgB=[[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 30, 30)];
            
            if(delegate.index_current_song==indexPath.row){
                imgB.image=[UIImage imageNamed:@"CurrentPlaying.png"];
            }
            [accessView addSubview:imgB];

            UIButton *btnStar=[[UIButton alloc]initWithFrame:CGRectMake(35,10, 30,30)];
            [btnStar setImage:[UIImage imageNamed:@"close_over.png"] forState:UIControlStateNormal];
            [btnStar addTarget:self action:@selector(favTrackClicked:) forControlEvents:UIControlEventTouchUpInside];
            btnStar.tag=indexPath.row;
            [btnStar.layer setCornerRadius:15.0];
            [btnStar.layer setBorderColor:NAVNBAR_COLOR.CGColor];
             [btnStar.layer setBorderWidth:1.0];
            [accessView addSubview:btnStar];
            
            cell.accessoryView=accessView;

            

        }
        
        if(self.isOverLay==false){
        
        [lblSep setBackgroundColor:CELL_SELECTTION_COLOR];
        }else{
            
            
            cell.detailTextLabel.text=objSh.art_name;
            cell.detailTextLabel.font=[UIFont fontWithName:FONTNAME size:13.0];
            cell.detailTextLabel.textColor=[UIColor colorWithWhite:1.0 alpha:0.6];

        [lblSep setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.1]];
        }
        [cell.contentView addSubview:lblSep];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor =CELL_SELECTTION_COLOR;
    [cell setSelectedBackgroundView:bgColorView];


    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];

    
    if(Tag==1){
        Show *objSh=[arrShow objectAtIndex:indexPath.row];
        TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
        objT.objShow=objSh;
        objT.playListDelegate=self.playListDelegate;
        [self.navigationController pushViewController:objT animated:true];
//        if(indexPath.row==1){
//            Tag=1;
//            Show *objS=[arrShow objectAtIndex:indexPath.row];
//            bgblurView.hidden=false;
//            tblView.scrollEnabled=true;
//             [self getPlayLisTDetail:objS.show_id];
//            
//        }
    }
    if(Tag==2 || Tag==3){
        
        if(isOverLay==true){
            delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
            [delegate playAudioWithIndex:indexPath.row];
            delegate.playerView.hidden=true;
            [self.playdelegate changePlyTitle:self.objPlayList withArrTr:arrTracks withShow:arrShows];
            [AppDelegate initAppdelegate].isPushed=false;
            [[AppDelegate initAppdelegate].info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];
            [AppDelegate initAppdelegate].isPushed=false;

            [self.navigationController popToRootViewControllerAnimated:true];
            return;

        }
        
            bgblurView.hidden=false;
            tblView.scrollEnabled=true;
        if(delegate.isMyFav==true){
            delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];

        }
        [delegate playAudioWithIndex:indexPath.row];
        if(self.isOverLay==true){
        }
    }

    
}
-(void)purchaseTic:(UIButton *)btn{
    
    
    
    
    Show *objSh=[arrShow objectAtIndex:btn.tag];
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
     [AppDelegate initAppdelegate].playerView.hidden=true;

    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"show_id=%@",objSh.show_id];
    [Utility executePOSTRequestwithServicetype:DETAIL_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
          NSMutableDictionary *dictRes=[dictresponce valueForKey:@"DATA"];
            PurchaseTicket *objP=[[PurchaseTicket alloc]initWithNibName:@"PurchaseTicket" bundle:nil];
            objP.objShow=objSh;
            objP.arrTic=[dictRes valueForKey:@"Tickets"];
            objP.playListDelegate=self.playListDelegate;
            [self.navigationController pushViewController:objP animated:true];
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];

//    TicketInfoVC *objT=[[TicketInfoVC alloc]initWithNibName:@"TicketInfoVC_New" bundle:nil];
//    objT.objShow=objSh;
//    [self.navigationController pushViewController:objT animated:true];
       
    
}

- (IBAction)showSideMenu:(id)sender {

//    if(Tag==3){
        [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        }];
//    }else{
//        [self.navigationController popViewControllerAnimated:true];
//  
//    }
    

}

- (IBAction)backButtonPressed:(id)sender {
    [AppDelegate initAppdelegate].isPushedShow=false;
    [self.navigationController popViewControllerAnimated:true];
}
#pragma mark- ANIMATION METHODS
-(IBAction)showMenu:(id)sender{
    
//    [self onShowMenu];
    if([AppDelegate initAppdelegate].arrPlayList.count==0){
        return;
    }
    [self onShowMenu];

}
- (void)onShowMenu
{
    if(menuButton.selected){
        menuButton.selected=false;

        [self onHideMenu];
    }else{
        //    if (!self.table) {
        UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect frame = mainWindow.frame;
        //        frame.origin.y += self.view.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
        
        frame.origin.y=80;
        frame.size.height-=150;
        NSMutableArray *iem=[[NSMutableArray alloc]init];
        
        
        if([AppDelegate initAppdelegate].arrPlayList.count >0){
            
            for(int i=0;i<[AppDelegate initAppdelegate].arrPlayList.count;i++){
                PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:i];
                [iem addObject:objs.playName];
            }
        }
        
        self.table = [[SIMenuTable alloc] initWithFrame:frame items:iem];
        self.table.menuDelegate = self;
        self.table.arrData=[AppDelegate initAppdelegate].arrPlayList;
        self.menuContainer=self.view;
        [self.menuContainer addSubview:self.table];
        [self rotateArrow:M_PI];
        [self.table show];
        menuButton.selected=true;
    }
}

- (void)onHideMenu
{
    [self rotateArrow:0];
    [self.table hide];
}

- (void)rotateArrow:(float)degrees
{
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
//        self.menuButton.arrow.layer.transform = CATransform3DMakeRotation(degrees, 0, 0, 1);
    } completion:NULL];
}

#pragma mark -
#pragma mark Delegate methods
- (void)didSelectPlayAtIndex:(NSUInteger)index
{
    
    [self backButtonPressed:nil];
    [self.playListDelegate didSelectPlayAtIndex:index];
//    [Utility showAlrtWithMessage:@"Play select"];
    return;
    
    
}
- (void)didSelectPlayItemListItemAtIndex:(NSUInteger)index;
{
    [self backButtonPressed:nil];
    [self.playListDelegate didSelectPlayItemListItemAtIndex:index];
    return;
}
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    
    [self.table hide];
    PlayList *objs=[[AppDelegate initAppdelegate].arrPlayList objectAtIndex:index];
    
    if([objs.totaltrack integerValue]==0){
        //        [self showNotificationView:@"No Tracks Found" :[UIImage imageNamed:@"info.png"]];
        return;
    }
    //       lblPlayTitle.text=objs.playName;
    
    //    [self onHandleMenuTap:nil];
    
    [AppDelegate initAppdelegate].objPlaylist=objs;
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    [self showOverLayDetail:objs.play_id withName:objs.playName withObj:objs];
    
}


-(void)showOverLayDetail:(NSString *)idPlayList withName:(NSString *)name withObj:(PlayList *)objP
{
    //    [arrTracks removeAllObjects];
    //    [arrShow removeAllObjects];
    //    [tblView reloadData];
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",idPlayList,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTr,*arrSho;
            
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            if(arrTracs.count >0){
                arrTr=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrTracs)
                {
                    [arrTr addObject:[Track initWithResponceDict:dict]];
                }
                
            }
            //                arrTracks=arrTr;
            //            delegate.index_current_song=0;
            //            [delegate playAudioWithIndex:delegate.index_current_song];
            //            [delegate pausePlayer];
            if(arrShows.count >0){
                arrSho=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows)
                {
                    [arrSho addObject:[Show initWithResponceDict:dict]];
                }
                
            }else{
                
            }
            
            DashboardVC *objDash;
            
            for(UIViewController *vc in [self.navigationController viewControllers]){
                if([vc isKindOfClass:[DashboardVC class]]){
                    
                    objDash=(DashboardVC *)vc;
                    break;
                }
            }
            
            
            PlayListVC *objPlayList=[[PlayListVC alloc]initWithNibName:@"PlayListVC_New" bundle:nil];
            objPlayList.isTracks=true;
            objPlayList.playdelegate=objDash;
            objPlayList.isOverLay=true;
            objPlayList.arrShows=arrSho;
            
            objPlayList.currentShowName=name;
            objPlayList.arrData=arrTr;
            objPlayList.objPlayList=objP;
            [AppDelegate initAppdelegate].playerView.hidden=true;
            
            [self.navigationController pushViewController:objPlayList animated:true];
        }else{
            //            [self showNotificationView:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0] :[UIImage imageNamed:@"info.png"]];
            
        }
    }];
    
}

-(void)getPlayLisTDetail:(NSString *)idPlayList
{
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
//    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@",idPlayList,[AppDelegate initAppdelegate].objUser.locId ];
    
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",idPlayList,[AppDelegate initAppdelegate].objUser.locId,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];

    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            NSMutableArray *arrShows=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            if(arrTracs.count >0){
                arrTracks=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrTracs)
                {
                    [arrTracks addObject:[Track initWithResponceDict:dict]];
                }
                delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
                delegate.index_current_song=0;
                [delegate playAudioWithIndex:delegate.index_current_song];
            }else{
                if(Tag==1){
                    if(self.isFromTic==true){
                        lblHeader.text=[NSString stringWithFormat:@"%lu shows for this artist",(unsigned long)arrShow.count];
                        
                    }else{
                    
                    lblHeader.text=[NSString stringWithFormat:@"%lu shows for this playlist",(unsigned long)arrShow.count];
                    }
                }else if(Tag==2){
                    lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in this playlist",(unsigned long)arrTracks.count];
                }else{
                    lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
                }

            }
            
                    [tblView reloadData];
            
            // [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictRes valueForKey:@"DATA"]];
            //[[AppDelegate initAppdelegate]showDashboardMenu];
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    
    return;
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"show_id=%@",idPlayList];
    [Utility executePOSTRequestwithServicetype:DETAIL_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
            for(NSString * strCount in arr)
            {
                //NSLog(@"%@",strCount);
                NSMutableDictionary *dict=[arr valueForKey:strCount];
                //NSLog(@"%@",dict);
                
                
            }
            //            [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictRes valueForKey:@"DATA"]];
            //            [[AppDelegate initAppdelegate]showDashboardMenu];
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    
}
- (void)didBackgroundTap
{

//    self.menuButton.isActive = !self.menuButton.isActive;
    [self onHandleMenuTap:nil];
}


- (void)onHandleMenuTap:(id)sender
{
    if (!menuButton.selected) {
        menuButton.selected=true;
        //NSLog(@"On show");
        [self onShowMenu];
    } else {
        //NSLog(@"On hide");
        [self onHideMenu];
                menuButton.selected=false;
    }
}



#pragma mark- WEBSERVICE METHODS

-(void)getAllShows
{
    if(delegate.arrLoaction.count >0){
        for(Location *objL in delegate.arrLoaction){
            if([objL.loc_id isEqualToString:delegate.objUser.locId]){
                delegate.objUser.objLocation=objL;
                break;
            }
        }
    }
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    
    NSString *postStr=[NSString stringWithFormat:@"utp_play_id=%@&utp_loc_id=%@&distance_type=%@&playlist_distance=%f&month_range=%ld",[AppDelegate initAppdelegate].objUser.locId,[AppDelegate initAppdelegate].objUser.locId ,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrTracs=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Track"];
            if(arrTracs.count >0){
                arrTracks=[[NSMutableArray alloc]init];
            for(NSMutableDictionary *dict in arrTracs)
            {

                [arrTracks addObject:[Track initWithResponceDict:dict]];
            }
                
            }
            
            
            
            [tblView reloadData];
// [AppDelegate initAppdelegate].objUser=[User initWithResponceDict:[dictRes valueForKey:@"DATA"]];
//[[AppDelegate initAppdelegate]showDashboardMenu];
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    
}
-(void)favTrackClicked:(UIButton *)btn{
    
    Track *objTr=[arrTracks objectAtIndex:btn.tag];
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"usrplay_lgn_id=%@&utp_trk_id=%@",[AppDelegate initAppdelegate].objUser.login_id,objTr.id_track];
    
    [Utility executePOSTRequestwithServicetype:MAKE_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [Utility showAlrtWithMessage:[dictresponce valueForKey:@"MESSAGES"]];
            if(objTr.isFav==true){
                objTr.isFav=false;
//                [arrTracks removeObject:objTr];
//                [tblView reloadData];
//                lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"FavClick"
                 object:self];
                [[AppDelegate initAppdelegate]checkFavTrack:objTr];
            }else{
                objTr.isFav=true;
            }
        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
    

    
    
    
    
    


}
-(void)favShowClicked:(UIButton *)btn{
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    Show *objSh=[arrShow objectAtIndex:btn.tag];
    NSString *postStr=[NSString stringWithFormat:@"msf_show_id=%@&msf_lgn_id=%@",objSh.show_id,[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:MARK_SHOW_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            
            objSh.isFav=!objSh.isFav;
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FavNotification"
             object:self];
//            NSMutableDictionary *arr=[dictresponce valueForKey:@"DATA"];
                        [tblView reloadData];
            
                [self showNotificationView:@"Show saved to My Shows" :[UIImage imageNamed:@"star-icon-status-bar-white.png"]];
                
            
            
        }else{
            [self showNotificationView:[dictresponce valueForKey:@"MESSAGES"] :[UIImage imageNamed:@"star-icon-status-bar-white.png"]];

        }
    }];

}

-(void)showNotificationView:(NSString *)msg :(UIImage *)img
{
    notiImg.image=img;
    lblMsg.text=msg;
       [viewNoti setBackgroundColor:[UIColor colorWithRed:125.0/255.0 green:33.0/255.0 blue:210.0/255.0 alpha:0.8]];
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y += frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:0.5];
                     }];
    
}
-(void)hideNotiView
{
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                                                  CGRect frame = topView.frame;

                         frame.origin.y -= frame.size.height;
                         viewNoti.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                         //                         [self performSelector:@selector(hideNotiView) withObject:nil afterDelay:0.5];
                     }];
    
}
-(void)getCurrDetail
{
    
    
    
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    
    
    NSString *postStr1=[NSString stringWithFormat:@"utp_play_id=%@&lgn_id=%@&latitude=%f&longitude=%f&distance_type=%@&playlist_distance=%f&month_range=%ld",self.objPlayList.play_id,[AppDelegate initAppdelegate].objUser.user_id ,[AppDelegate initAppdelegate].locationManager.location.coordinate.latitude,[AppDelegate initAppdelegate].locationManager.location.coordinate.longitude,@"2",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
    [Utility executePOSTRequestwithServicetype:GET_PLAY_LIST withPostString: postStr1 withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrShows1=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            
            if(arrShows1.count >0){
                arrShow=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows1)
                {
                    [arrShow addObject:[Show initWithResponceDict:dict]];
                }
                
                [tblView reloadData];
            }
        }
    }];
    
    return;
    /*
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,GET_PLAY_LIST]]];
    [request setRequestMethod:@"POST"];
    [request setPostValue:self.objPlayList.play_id forKey:@"utp_play_id"];
    //    [request setPostValue:[AppDelegate initAppdelegate].objUser.locId forKey:@"utp_loc_id"];
    [request setPostValue:[AppDelegate initAppdelegate].objUser.user_id forKey:@"lgn_id"];
    [request setPostValue:[NSString stringWithFormat:@"%f", [AppDelegate initAppdelegate].locationManager.location.coordinate.latitude] forKey:@"latitude"];
    [request setPostValue:[NSString stringWithFormat:@"%f", [AppDelegate initAppdelegate].locationManager.location.coordinate.longitude] forKey:@"longitude"];
    [request setPostValue:@"2" forKey:@"distance_type"];
    [request setPostValue:[NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"]] forKey:@"playlist_distance"];
    [request setPostValue:[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]] forKey:@"month_range"];
    

    
    [request setDelegate:self];
    [request startAsynchronous];
    [request setCompletionBlock:^{
        
        
        NSError *error1;
        
        NSMutableDictionary * dictresponce = [NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:&error1];
        
        
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arrShows1=[[dictresponce valueForKey:@"DATA"] valueForKey:@"Show"];
            
            if(arrShows1.count >0){
                arrShow=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arrShows1)
                {
                    [arrShow addObject:[Show initWithResponceDict:dict]];
                }
                
                [tblView reloadData];
            }
        }
        STOPLOADING()
    }];
    
    [request setFailedBlock:^{
        
    }];
    
    [request start];
    */
    
}


-(void)getMyFavourites
{
    [arrTracks removeAllObjects];
 if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }

    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"usrplay_lgn_id=%@",[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:GET_MY_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableArray *arr=[dictresponce valueForKey:@"DATA"];
            if(arr.count >0){
                arrTracks=[[NSMutableArray alloc]init];
                for(NSMutableDictionary *dict in arr)
                {
                    [arrTracks addObject:[Track initWithResponceDict:dict]];
                }
                delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
                delegate.index_current_song=0;
                [delegate playAudioWithIndex:delegate.index_current_song];
                [delegate pausePlayer];
                delegate.isMyFav=true;
            }
            [tblView reloadData];
            lblHeader.text=[NSString stringWithFormat:@"%lu Tracks in my favorites",(unsigned long)arrTracks.count];


        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];
}


- (IBAction)dismissView:(id)sender {
    [AppDelegate initAppdelegate].isPushed=false;
    [[AppDelegate initAppdelegate].info setImage:[UIImage imageNamed:@"infor.png"] forState:UIControlStateNormal];
    [AppDelegate initAppdelegate].isPushed=false;

    [self.navigationController popViewControllerAnimated:true];
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)playCurrentTracks:(id)sender {
    
    delegate.arrTracks=[[NSMutableArray alloc]initWithArray:arrTracks];
    [delegate playAudioWithIndex:0];
    delegate.playerView.hidden=true;
    [self.playdelegate changePlyTitle:self.objPlayList withArrTr:arrTracks withShow:arrShows];
    
    [self dismissView:nil];
    [self.navigationController popToRootViewControllerAnimated:true];

    return;

}

#pragma mark- Share Methods
- (IBAction)showShareButton:(id)sender {
    if(isShown){
        [shareButton setImage:[UIImage imageNamed:@"shareIconInactive.png"] forState:UIControlStateNormal];
        //        [UIView beginAnimations:@"removeFromSuperviewWithAnimation" context:nil];
        
        
        // Set delegate and selector to remove from superview when animation completes
        // Move this view to bottom of superview
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        CGRect frame = shareView.frame;
        frame.origin = CGPointMake(0.0,-self.view.bounds.size.height);
        shareView.frame = frame;
        [UIView commitAnimations];
        isShown=false;
        
    }else{
        [shareButton setImage:[UIImage imageNamed:@"shareIconActive.png"] forState:UIControlStateNormal];
        [shareView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
        [view_mainShare setBackgroundColor:[UIColor colorWithRed:15.0/255 green:176.0/255 blue:212.0/255 alpha:0.30]];
        CGRect frame = shareView.frame;
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:shareView cache:YES];
        
        
        self.view.alpha=1.0f;
        frame.origin = CGPointMake(0.0, 0);
        shareView.frame = frame;
        //        [[shareView layer] addAnimation:animation forKey:@"Slidein"];
        [UIView commitAnimations];
        
        isShown=true;
        
    }
    
}
- (IBAction)showAppLink:(id)sender;
{
    
}

- (IBAction)shareOnTwitter:(id)sender {
    [self showShareButton:nil];
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//    {
    
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}

- (IBAction)shareOnFacebook:(id)sender {
    [self showShareButton:nil];
    [AppDelegate initAppdelegate].playerView.hidden=true;
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeFacebook];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]];
        
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [AppDelegate initAppdelegate].playerView.hidden=false;
                [self dismissViewControllerAnimated:NO completion:^{
                    //NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //  Set the initial body of the Tweet
        [tweetSheet setInitialText:@"Download This Great App At https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"];
        
        //  Adds an image to the Tweet.  For demo purposes, assume we have an
        //  image named 'larry.png' that we wish to attach
        if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
            //NSLog(@"Unable to add the image!");
        }
        
        //  Add an URL to the Tweet.  You can add multiple URLs.
        if (![tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/gigable-music-discovery/id1118761510?ls=1&mt=8"]]){
            //NSLog(@"Unable to add the URL!");
        }
        
        //  Presents the Tweet Sheet to the user
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
    
}


@end
