//
//  PlayListVC
//  Gigable
//
//  Created by Kalpit Gajera on 26/01/16.
//  Copyright © 2016 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "PlayListDelegate.h"


@protocol PLAYSELECTDELEGATE <NSObject>
-(void)changeSelection:(PlayList *)objPlayl;
-(void)changePlyTitle:(PlayList *)objPlayl withArrTr:(NSMutableArray *)arrTrac withShow:(NSMutableArray *)arrsh;

@end;
@interface PlayListVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,PlayListDelegate>
{
    
    IBOutlet UILabel *lblTrackCount;
    IBOutlet UIButton *btnClose;
    IBOutlet UIButton *btnPlay;
     IBOutlet UIButton *backBtn;
     IBOutlet UIView *bgblurView;
     IBOutlet UILabel *lblTopTitle;
     IBOutlet UIPageControl *pageControl;
     IBOutlet UIView *topView;

     IBOutlet UIImageView *arrow;
     IBOutlet UIView *top_buttonView;
     IBOutlet UILabel *lblPlayTitle;
     IBOutlet UITableView *tblView;
     IBOutlet UIButton *menuButton;
    IBOutlet UIView *viewNoti;
    IBOutlet UIImageView *notiImg;
    IBOutlet UILabel *lblMsg;

    NSMutableArray *arrTracks,*arrShow;
    int Tag;
    
    
    IBOutlet UIView *view_mainShare;
    IBOutlet UIButton *shareButton;
    BOOL isFav;
    BOOL isShown;
    IBOutlet UIView *shareView;
}

- (IBAction)dismissView:(id)sender;

- (IBAction)playCurrentTracks:(id)sender;

@property (nonatomic, weak) id <PlayListDelegate> playListDelegate;

@property (assign,nonatomic) PlayList *objPlayList;
@property (assign,nonatomic)BOOL isOverLay,isFromTic;
@property (assign,nonatomic)id<PLAYSELECTDELEGATE> playdelegate;
@property (assign,nonatomic)BOOL isTracks,isFav;
@property (strong,nonatomic)NSMutableArray *arrData,*arrShows;
@property (strong,nonatomic)NSString *currentShowName;
@end
