//
//  HomeDashboardViewController.h
//  Gigable
//
//  Created by AppDev03 on 1/16/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ChangeLocationViewController.h"

@interface HomeDashboardViewController : UIViewController <UIApplicationDelegate,CLLocationManagerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ChangeLocationViewControllerDelegate>
{
    IBOutlet UICollectionView *CollectionArtist;
    IBOutlet UICollectionView *CollectionFeaturedArtist;
    IBOutlet UILabel *lblArtistName;
    IBOutlet UILabel *lblConcertCount;
    IBOutlet UILabel *lblTrackCount;
    IBOutlet UILabel *lblLocation;
    IBOutlet NSLayoutConstraint *FeaturedCollectionViewHeight;
    IBOutlet UICollectionView *collectionPlaylist;
    IBOutlet NSLayoutConstraint *PlaylistCollectionViewHeight;
    
    IBOutlet NSLayoutConstraint *mainViewScrollHeight;
   
    IBOutlet UIImageView *imgUpcomingConcert;
    IBOutlet UILabel *lblUpcomingArtistName;
    IBOutlet UILabel *lblUpcomingConcertCount;
    IBOutlet UILabel *lblUpcomingTracksCount;
    
}
@property (strong,nonatomic)CLLocationManager *locationManager;

@end
