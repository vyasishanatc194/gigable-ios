//
//  ArtistDetailViewController.h
//  Gigable
//
//  Created by AppDev03 on 1/18/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tblfeaturedTrack;
@property (strong, nonatomic) IBOutlet UITableView *tblConcerts;
@property (strong, nonatomic) IBOutlet UIImageView *imgArtist;
@property (strong, nonatomic) IBOutlet UILabel *lblArtistName;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tblFeaturedTaskHeight;
@property (strong, nonatomic) IBOutlet UILabel *lblArtistDescription;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tblConcertHeight;

@property (strong, nonatomic) IBOutlet UILabel *lblHeaderArtistName;

@property (strong, nonatomic) IBOutlet UILabel *lblConcerts;
@property (strong, nonatomic) IBOutlet UILabel *lblTracks;
@property (strong, nonatomic) IBOutlet UILabel *lblPlayName;
@property (strong, nonatomic) IBOutlet UILabel *lblFeaturedTrack;
@property (strong, nonatomic) IBOutlet UILabel *lblelConcert;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottom_To_playlist;

@property(nonatomic,strong)NSString *ArtistId;
@end
