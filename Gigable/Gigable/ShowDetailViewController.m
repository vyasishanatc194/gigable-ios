//
//  ShowDetailViewController.m
//  Gigable
//
//  Created by AppDev03 on 1/19/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import "ShowDetailViewController.h"

#import "HomeDashboardViewController.h"
#import "HomeDashboardViewController.h"
#import "AppDelegate.h"
#import "Static.h"
#import "Utility.h"
#import "ArtistCellCollectionViewCell.h"
#import "FeaturedArtistCell.h"
#import "PlaylistCell.h"
#import "UIImageView+WebCache.h"
#import "PurchaseTicket.h"

@interface ShowDetailViewController ()
{
    NSMutableArray *ConcertAllData;
    
}
@end

@implementation ShowDetailViewController
@synthesize playListDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
   
    [self FetchConcertDetail];
}


-(void)FetchConcertDetail{
    //    if([Utility isStringEmpty:email.text]){
    //        //        [Utility showAlrtWithMessage:@"Please enter email"];
    //        CGRect frLb=sepLogin.frame;
    //        frLb.size.height=3.0;
    //        sepLogin.frame=frLb;
    //        errro1.text=@"Please enter email";
    //
    //        return;
    //    }
    //
    //    if(![Utility NSStringIsValidEmail:email.text]){
    //        CGRect frLb=sepLogin.frame;
    //        frLb.size.height=3.0;
    //        sepLogin.frame=frLb;
    //        errro1.text=@"Please enter valid email";
    //        return;
    //    }
    //
    //    if([Utility isStringEmpty:password.text]){
    //        CGRect frLb1=sepPwd.frame;
    //        frLb1.size.height=3.0;
    //        sepPwd.frame=frLb1;
    //        errro2.text=@"Please enter password";
    //
    //
    //        return;
    //    }
    
    
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    
    //    NSString *post = [NSString stringWithFormat: @"Lgn_id=%@&Latitude=%f&Longitude=%f&playlist_distance=%f&distance_type=%@&month_range=%ld",[AppDelegate initAppdelegate].objUser.user_id,self.locationManager.location.coordinate.latitude,self.locationManager.location.coordinate.longitude,[[NSUserDefaults standardUserDefaults] floatForKey:@"Distance"],@"",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"Month"]];
    
   
    
    NSString *post = [NSString stringWithFormat: @"show_id=%@&lgn_id=%@",_showId,[AppDelegate initAppdelegate].objUser.user_id];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MAIN_URL,DETAIL_PLAYLIST]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:20.0];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               STOPLOADING()
                               NSError *error1;
                               if(data==nil){
                               }else{
                                   
                                   
                                   NSMutableDictionary * dictRes = [NSJSONSerialization                                                JSONObjectWithData:data options:kNilOptions error:&error1];
                                   NSLog(@"%@",dictRes);
                                   
                                   if([[dictRes valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
                                       
//                                        [self GetAllFavouriteShowList];
                                       
                                       ConcertAllData = [dictRes valueForKey:@"DATA"];
                                       NSMutableArray *arrTickets = [[NSMutableArray alloc]init];
                                       arrTickets = [ConcertAllData valueForKey:@"Tickets"];
                                       NSString *Price;
                                       Price = [[arrTickets objectAtIndex:0] valueForKey:@"ticket_price"];
                                       [_btnGetTicket setTitle:[NSString stringWithFormat:@"GET TICKET $%@",Price] forState:UIControlStateNormal];
                                       _lblAddress.text = [ConcertAllData valueForKey:@"ven_location"];
                                       _lblConcertname.text = [ConcertAllData valueForKey:@"show_title"];
                                       _lblDate.text = [NSString stringWithFormat:@"%@ %@",[ConcertAllData valueForKey:@"show_date"],[ConcertAllData valueForKey:@"show_time"]];
                                       NSMutableArray *artistData = [ConcertAllData valueForKey:@"artist"];
//                                       _lblSingerName.text = [[artistData objectAtIndex:0]valueForKey:@"art_name"];
                                        _lblSingerName.text =_artistName;
                                       NSString *isFav = [ConcertAllData valueForKey:@"msf_show_fav_status"];
                                       NSInteger fav = [isFav integerValue];
                                       if (fav == 0)
                                       {
                                           _imgFav.image = [UIImage imageNamed:@"Star 8"];
                                       }
                                       else
                                       {
                                           _imgFav.image = [UIImage imageNamed:@"Star-Icon-Active.png"];
                                       }
//                                       NSString *imageURl = [[artistData objectAtIndex:0]valueForKey:@"art_image"];
                                       [_imgSingerName sd_setImageWithURL:[NSURL URLWithString:_artistImage]
                                                         placeholderImage:[UIImage imageNamed:@""]];
                                   }else{
                                       
//                                        [self GetAllFavouriteShowList];
                                       
                                   }
                                   
                               }
                           }];
    
}

- (IBAction)btnClosePressed:(id)sender {
//    [self dismissViewControllerAnimated:false completion:nil];
    [self.navigationController popViewControllerAnimated:false];
}

- (IBAction)btnFavouritePressed:(id)sender {
    [self isFavourite];
    
}


-(void)isFavourite
{
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    
    SHOWLOADING(@"Loading")
    
    NSString *postStr=[NSString stringWithFormat:@"msf_show_id=%@&msf_lgn_id=%@",_showId,[AppDelegate initAppdelegate].objUser.user_id];
    [Utility executePOSTRequestwithServicetype:MARK_SHOW_FAV withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            [self FetchConcertDetail];
            
        }else{
            
            [self FetchConcertDetail];
        }
    }];

}
//-(void)GetAllFavouriteShowList
//{
//    
//    if(![Utility connectedToNetwork]){
//        [Utility showNetworkError];
//        return;
//    }
//    
//    
//    SHOWLOADING(@"Loading")
//    
////    NSString *postStr=[NSString stringWithFormat:@"msf_lgn_id=%@",[AppDelegate initAppdelegate].objUser.user_id];
//     NSString *postStr=[NSString stringWithFormat:@"msf_lgn_id=%d",3];
//    [Utility executePOSTRequestwithServicetype:GET_MY_FAV_SHOW withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
//        STOPLOADING()
//        NSLog(@"%@",dictresponce);
//        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
//            
//            
////            for (type *object in <#collection#>) {
////                <#statements#>
////            }
//            
//        }else{
//            
//            
//        }
//    }];
//
//    
//}
- (IBAction)btnGetTicketPressed:(id)sender {
    
    Show *objSh=ConcertAllData;
    if(![Utility connectedToNetwork]){
        [Utility showNetworkError];
        return;
    }
    
    [AppDelegate initAppdelegate].playerView.hidden=true;
    
    SHOWLOADING(@"Loading")
    NSString *postStr=[NSString stringWithFormat:@"show_id=%@",[objSh valueForKey:@"show_id"]];
    [Utility executePOSTRequestwithServicetype:DETAIL_PLAYLIST withPostString: postStr withBlock:^(NSMutableDictionary *dictresponce, NSError *error) {
        STOPLOADING()
        //NSLog(@"%@",dictresponce);
        if([[dictresponce valueForKey:@"STATUS"]isEqualToString:@"SUCCESS"]){
            NSMutableDictionary *dictRes=[dictresponce valueForKey:@"DATA"];
            PurchaseTicket *objP=[[PurchaseTicket alloc]initWithNibName:@"PurchaseTicket" bundle:nil];
            objP.objShow=objSh;
            objP.arrTic=[dictRes valueForKey:@"Tickets"];
            
            objP.playListDelegate=self.playListDelegate;

            [self.navigationController pushViewController:objP animated:false];

        }else{
            [Utility showAlrtWithMessage:[[dictresponce valueForKey:@"MESSAGES"]objectAtIndex:0]];
        }
    }];

    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
