//
//  UpcomingConcertCell.h
//  Gigable
//
//  Created by AppDev03 on 1/17/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingConcertCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgUpcomingConcert;

@end
