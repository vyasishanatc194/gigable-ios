//
//  LocationListCell.h
//  Gigable
//
//  Created by AppDev03 on 1/19/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *LocationName;

@end
