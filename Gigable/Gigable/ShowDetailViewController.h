//
//  ShowDetailViewController.h
//  Gigable
//
//  Created by AppDev03 on 1/19/17.
//  Copyright © 2017 Kalpit Gajera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "PlayListDelegate.h"

@interface ShowDetailViewController : UIViewController<PlayListDelegate>
@property (nonatomic,strong)NSString *showId;

@property (nonatomic,strong)NSString *artistImage;
@property (nonatomic,strong)NSString *artistName;

@property (strong, nonatomic) IBOutlet UIImageView *imgSingerName;
@property (strong, nonatomic) IBOutlet UILabel *lblSingerName;
@property (strong, nonatomic) IBOutlet UILabel *lblConcertname;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIImageView *imgFav;
@property (strong, nonatomic) IBOutlet UIButton *btnGetTicket;

@property (nonatomic, weak) id <PlayListDelegate> playListDelegate;

@end
